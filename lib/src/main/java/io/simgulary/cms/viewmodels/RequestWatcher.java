/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>
 * Last Modified 21/08/18 02:55 PM
 */

package io.simgulary.cms.viewmodels;

import io.simgulary.cms.http.ApiRequest;

/**
 * Listen the request creation to register/unregister it on Activities
 */
public interface RequestWatcher {
    /**
     * Triggered when a Request is created
     *
     * @param request the new Request
     */
    void onRequestCreated(ApiRequest<?> request);
}
