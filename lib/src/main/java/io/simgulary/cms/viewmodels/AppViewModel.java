/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 09:39 AM
 */

package io.simgulary.cms.viewmodels;

import android.app.Application;

import java.util.List;
import java.util.Vector;

import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import io.simgulary.cms.http.ApiRequest;
import io.simgulary.cms.threads.Threads;

@SuppressWarnings("unused")
public abstract class AppViewModel extends AndroidViewModel {
    private final List<ApiRequest<?>> requestList = new Vector<>();
    @Nullable private RequestWatcher mWatcher;

    protected AppViewModel(@NonNull Application application) {
        super(application);
    }

    public final void setWatcher(@Nullable RequestWatcher watcher) {
        mWatcher = watcher;
    }

    protected final void requestCreated(ApiRequest<?> request) {
        Threads.executeOnMainThread(() -> {
            requestList.add(request);

            if (mWatcher != null)
                mWatcher.onRequestCreated(request);
        });
    }

    @Override @CallSuper
    protected void onCleared() {
        mWatcher = null;

        for (ApiRequest<?> request : requestList) {
            request.close();
        }

        requestList.clear();
    }
}
