/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/10/20 05:01 PM.
 */

package io.simgulary.cms.utils;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import androidx.core.content.FileProvider;
import io.simgulary.cms.BuildConfig;
import io.simgulary.cms.R;
import io.simgulary.cms.app.AppApplication;

/**
 * Logger
 */

@SuppressWarnings({"unused", "WeakerAccess"})
public class Logger {
    private static final String UID = BuildConfig.LIBRARY_PACKAGE_NAME;
    private static final File logFile;
    private static final FileLogger fileLogger;
    private static final String DEBUG = UID + " D/";
    private static final String INFO = UID + " I/";
    private static final String WARNING = UID + " W/";
    private static final String ERROR = UID + " E/";

    static {
        File storageDirectory = AppApplication.get().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        String name = AppApplication.get().getString(R.string.log_name);
        logFile = new File(storageDirectory, name + "-log.txt");

        fileLogger = new FileLogger();
        if (!logFile.exists()) {
            boolean result = false;
            File parent = logFile.getParentFile();

            if (!parent.exists()) {
                //noinspection ResultOfMethodCallIgnored
                parent.mkdirs();
            }

            try {
                result = logFile.createNewFile();
            }
            catch (Throwable error) {
                Log.e("Logger", "Error creating log file: " + logFile, error);
            }

            Log.i("Logger", "Log file created: " + result);
        }

        Log.i("Logger", String.format("Log file: %s [%s]", logFile, logFile.exists()));
    }

    private final String TAG;

    private Logger(Class<?> clazz) {
        TAG = clazz.getSimpleName();
    }

    /**
     * Creates and retrieves a new Logger Instance
     *
     * @param clazz the Class to log
     * @return the Logger Instance
     */
    public static Logger getLogger(Class<?> clazz) {
        return new Logger(clazz);
    }

    public void debug(String format, Object... parameters) {
        try {
            final String txt = formatText(format, parameters);

            Log.i(TAG, txt);

            if (BuildConfig.DEBUG) {
                fileLogger.log(this, DEBUG, txt);
            }
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void info(String format, Object... parameters) {
        try {
            final String txt = formatText(format, parameters);

            Log.i(TAG, txt);
            fileLogger.log(this, INFO, txt);
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void warn(String format, Object... parameters) {
        try {
            final String txt = formatText(format, parameters);

            Log.w(TAG, txt);
            fileLogger.log(this, WARNING, txt);
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void warn(Throwable t, String format, Object... parameters) {
        t.printStackTrace();

        try {
            final String txt = formatText(format, parameters);

            Log.e(TAG, txt);
            fileLogger.log(this, WARNING, txt, getMessage(t));
        }
        catch (Throwable u) {
            u.printStackTrace();
        }
    }

    public void error(String format, Object... parameters) {
        try {
            final String txt = formatText(format, parameters);

            Log.e(TAG, txt);
            fileLogger.log(this, ERROR, txt);
        }
        catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public void error(Throwable t, String format, Object... parameters) {
        t.printStackTrace();

        try {
            final String txt = formatText(format, parameters);

            Log.e(TAG, txt);
            fileLogger.log(this, ERROR, txt, getMessage(t));
        }
        catch (Throwable u) {
            u.printStackTrace();
        }
    }

    public static void send(Activity activity, String errorMessage, int textRes, Integer code) {
        Uri path = FileProvider.getUriForFile(activity, activity.getPackageName() + ".provider", logFile);

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Error Report");
        emailIntent.putExtra(Intent.EXTRA_TEXT, errorMessage + AppApplication.from(activity).getDeviceInfo());
        String to[] = {"pedrop@manzanares.com.ve"};

        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        emailIntent.putExtra(Intent.EXTRA_STREAM, path);
        emailIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

        Intent chooser = Intent.createChooser(emailIntent, activity.getString(textRes));

        if (code == null) {
            activity.startActivity(chooser);
        } else {
            activity.startActivityForResult(chooser, code);
        }
    }

    public void logDeviceInfo() {
        fileLogger.log(this, INFO, AppApplication.get().getDeviceInfo());
    }

    private String formatText(String format, Object[] parameters) {
        if (parameters == null || parameters.length == 0) {
            return format;
        } else {
            return String.format(Locale.ROOT, format, parameters);
        }
    }

    public static String getMessage(Throwable t) {
        final StringBuilder sb = new StringBuilder();
        sb.append(t.getMessage());

        appendCause(sb, t);

        Throwable cause = t.getCause();

        while (cause != null) {
            sb.append(" cause: ");
            sb.append(cause.getMessage());
            appendCause(sb, cause);
            cause = cause.getCause();
        }

        return sb.toString();
    }

    private static void appendCause(StringBuilder sb, Throwable t) {
        StackTraceElement[] trace = t.getStackTrace();
        if (trace != null) {
            for (StackTraceElement traceElement : trace)
                sb.append("\tat ").append(traceElement).append('\n');
        }
    }

    private static class FileLogger implements Runnable {
        private final Deque<String> lines = new LinkedList<>();
        private final Deque<String> queue = new LinkedList<>();
        private volatile boolean running = false;
        private volatile boolean loaded = false;

        FileLogger() {
            Log.i("Logger", "Logger started in " + logFile.getAbsolutePath());
        }

        void log(Logger logger, String prefix, String content) {
            content = String.format("%s %s%s %s", new Date(), prefix, logger.TAG, content);

            synchronized (queue) {
                queue.addLast(content);

                if (!running) {
                    try {
                        AsyncTask.THREAD_POOL_EXECUTOR.execute(this);
                        running = true;
                    }
                    catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }
        }

        void log(Logger logger, String prefix, String content, String error) {
            log(logger, prefix, String.format("%s %s", content, error));
        }

        @Override
        public void run() {
            if (!loaded) {
                try {
                    final List<String> file = FileUtils.readLines(logFile, Charset.defaultCharset());
                    lines.addAll(file);
                }
                catch (Throwable ignored) {
                }

                loaded = true;
            }

            boolean updated;

            synchronized (queue) {
                updated = false;
            }

            while (running) {
                while (true) {
                    final String line;

                    synchronized (queue) {
                        if (queue.isEmpty()) {
                            break;
                        }

                        line = queue.removeFirst();
                    }

                    lines.addLast(line);
                    updated = true;

                    while (lines.size() > 1024) {
                        lines.removeFirst();
                    }
                }

                if (updated) {
                    try {
                        FileUtils.writeLines(logFile, lines, IOUtils.LINE_SEPARATOR_WINDOWS, false);
                    }
                    catch (Throwable ignored) {
                    }
                }

                synchronized (queue) {
                    running = !queue.isEmpty();

                    if (!running)
                        break;
                }
            }
        }
    }
}
