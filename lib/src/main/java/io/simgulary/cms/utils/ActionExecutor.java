/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 25/10/18 10:55 PM
 */

package io.simgulary.cms.utils;

public interface ActionExecutor<T> {
    void execute(T input);
}
