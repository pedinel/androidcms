/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 15/03/19 11:33 AM
 */

package io.simgulary.cms.utils;

import androidx.annotation.NonNull;

public final class InstanceHelper {
    public static <T> T initialize(T object, @NonNull InitializerFunction<T> initializer) {
        if (object != null) {
            initializer.onInitialize(object);
        }

        return object;
    }

    public interface InitializerFunction<T> {
        void onInitialize(@NonNull T t);
    }
}
