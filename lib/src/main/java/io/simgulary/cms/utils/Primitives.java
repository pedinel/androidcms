/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 06/11/18 11:42 AM
 */

package io.simgulary.cms.utils;

public final class Primitives {
    public static int unwrap(Integer value) {
        return unwrap(value, 0);
    }

    public static int unwrap(Integer value, int valueIfNull) {
        return (value != null) ? value : valueIfNull;
    }

    public static long unwrap(Long value) {
        return unwrap(value, 0);
    }

    public static long unwrap(Long value, long valueIfNull) {
        return (value != null) ? value : valueIfNull;
    }

    public static boolean unwrap(Boolean value) {
        return unwrap(value, false);
    }

    public static boolean unwrap(Boolean value, boolean valueIfNull) {
        return (value != null) ? value : valueIfNull;
    }
}
