/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified by Pedro Parra 09/04/19 03:14 PM
 */

package io.simgulary.cms.utils;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import androidx.annotation.CheckResult;
import androidx.annotation.Nullable;
import androidx.arch.core.util.Function;
import io.simgulary.cms.math.MathUtils;

@SuppressWarnings("unused")
public final class StringUtils {
    private StringUtils() {
        //no instance
    }

    @CheckResult
    public static String stripAccents(String in) {
        if (in == null)
            return null;

        String normalized = Normalizer.normalize(in, Normalizer.Form.NFD);
        return normalized.replaceAll("\\p{InCOMBINING_DIACRITICAL_MARKS}+", "");
    }

    public static String getFormattedNumber(int value, int decimates) {
        if (decimates == 0) {
            return String.valueOf(value);
        } else {
            String f = "%." + decimates + "f";
            return String.format(f, MathUtils.fromE(value, decimates));
        }
    }

    public static String getFormattedNumber(double value, int decimates) {
        if (decimates == 0) {
            return String.valueOf((long) value);
        } else {
            String f = "%." + decimates + "f";
            return String.format(f, value);
        }
    }

    /**
     * Convenience method to return only the digits in the string.
     *
     * @param string       The String from which digits and plus will
     *                     be extracted
     *
     * @return             A String comprising all of the digits
     */
    public static String digitsOnly(String string) {
        StringBuilder buffer = new StringBuilder();

        for (int i = 0, size = string.length(); i < size; i++) {
            char character = string.charAt(i);

            if (Character.isDigit(character)) {
                buffer.append(character);
            }
        }
        return buffer.toString();
    }

    public static String valueOf(Object source, String valueIfNull) {
        return source == null ? valueIfNull : source.toString();
    }

    @Nullable
    public static String valueOf(Object source) {
        return valueOf(source, null);
    }

    public static String join(String delimiter, Iterable<?> tokens) {
        return join(delimiter, tokens, Object::toString);
    }

    public static <T> String join(String delimiter, Iterable<T> tokens, Function<T, String> function) {
        StringJoiner s = new StringJoiner(delimiter);

        for (T e : tokens) {
            s.add(function.apply(e));
        }

        return s.toString();
    }

    public static Comparator<String> naturalComparator() {
        return (o1, o2) -> o1.length() == o2.length()
                ? o1.compareTo(o2)
                : Integer.compare(o1.length(), o2.length());
    }

    public static List<String> sort(Collection<String> in) {
        List<String> out = new ArrayList<>(in);

        Collections.sort(out, naturalComparator());

        return out;
    }
}
