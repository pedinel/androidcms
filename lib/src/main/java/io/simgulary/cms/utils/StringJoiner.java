/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 28/02/19 03:03 PM
 */

package io.simgulary.cms.utils;

import androidx.annotation.NonNull;

public class StringJoiner {
    private String delimiter;
    private String emptyValue = "";

    private StringBuilder value;

    public StringJoiner(@NonNull CharSequence delimiter) {
        this.delimiter = delimiter.toString();
    }

    public StringJoiner setEmptyValue(@NonNull CharSequence emptyValue) {
        this.emptyValue = emptyValue.toString();
        return this;
    }

    private StringBuilder prepareBuilder() {
        if (value == null)
            value = new StringBuilder();
        else
            value.append(delimiter);

        return value;
    }

    public StringJoiner add(CharSequence newElement) {
        prepareBuilder().append(newElement);
        return this;
    }

    public <T> StringJoiner add(Iterable<T> newElements) {
        for (T newElement : newElements) {
            add(newElement.toString());
        }
        return this;
    }
    public void add(char[] str, int offset, int len) {
        prepareBuilder().append(str, offset, len);
    }


    public int length() {
        return (value != null) ? value.length() : emptyValue.length();
    }

    @Override @NonNull
    public String toString() {
        return (value != null) ? value.toString() : emptyValue;
    }
}
