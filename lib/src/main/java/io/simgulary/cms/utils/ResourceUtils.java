/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 02/08/19 09:30 AM
 */

package io.simgulary.cms.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import androidx.annotation.CheckResult;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;

public final class ResourceUtils {
    private ResourceUtils() {
        //no instance
    }

    @CheckResult
    @Nullable
    public static Drawable getDrawable(Context context, @DrawableRes int resource) {
        return AppCompatResources.getDrawable(context, resource);
    }

    @CheckResult
    public static DecodedResource decodeResource(Context context, int resource) {
        Drawable drawable = getDrawable(context, resource);
        return (drawable != null) ? new DecodedResource(drawable) : null;
    }

    public static final class DecodedResource {
        private final Drawable mDrawable;

        DecodedResource(Drawable drawable) {
            mDrawable = drawable;
        }

        @CheckResult
        public Drawable asDrawable() {
            return mDrawable;
        }

        @CheckResult
        public Bitmap asBitmap() {
            int width = mDrawable.getIntrinsicWidth();
            int height = mDrawable.getIntrinsicHeight();

            Bitmap result = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            Canvas resultCanvas = new Canvas(result);
            mDrawable.setBounds(0, 0, resultCanvas.getWidth(), resultCanvas.getHeight());
            mDrawable.draw(resultCanvas);

            return result;
        }
    }
}
