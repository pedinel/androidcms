/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 04/06/19 08:55 AM
 */

package io.simgulary.cms.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.arch.core.util.Function;
import io.simgulary.cms.lifecycle.Predicate;
import io.simgulary.cms.threads.Threads;

public final class Iterables {
    @CheckResult
    public static <K, V> Map<K, V> toMap(@NonNull Collection<V> values, @NonNull Function<V, K> keyMapper) {
        Map<K, V> out = new HashMap<>(values.size());

        for (V value : values) {
            @NonNull K apply = keyMapper.apply(value);
            out.put(apply, value);
        }

        return out;
    }

    @CheckResult
    public static <K, V> Map<K, V> toMap(@NonNull V[] values, @NonNull Function<V, K> keyMapper) {
        Map<K, V> out = new HashMap<>(values.length);

        for (V value : values) {
            @NonNull K apply = keyMapper.apply(value);
            out.put(apply, value);
        }

        return out;
    }

    public static <T> List<T> replace(@NonNull Collection<T> in,
                                      @NonNull T newValue,
                                      @NonNull Predicate<T> match,
                                      @NonNull Predicate<T> condition) {
        List<T> out = new ArrayList<>(in);

        for (int i = 0; i < out.size(); i++) {
            T t = out.get(i);

            if (match.test(t)) {
                if (condition.test(t)) {
                    out.set(i, newValue);
                }
                break;
            }
        }

        return out;
    }

    public static <T> List<T> replace(@NonNull Collection<T> in, @NonNull T newValue, @NonNull Predicate<T> function) {
        return replace(in, newValue, function, t -> true);
    }

    public static <T> void replace(@NonNull Collection<T> in, @NonNull T newValue, @NonNull Predicate<T> function, @NonNull ResultCallback<List<T>> callback) {
        Threads.runAsync(() -> replace(in, newValue, function))
                .onCompleted(callback::onResult);
    }

    public static <T> void replaceOrAdd(@NonNull Collection<T> in, @NonNull T newValue, @NonNull Predicate<T> function, @NonNull ResultCallback<List<T>> callback) {
        Threads.executeAsync(() -> {
            List<T> out = new ArrayList<>(in);

            for (int i = 0; i < out.size(); i++) {
                T t = out.get(i);

                if (function.test(t)) {
                    out.set(i, newValue);
                    Threads.executeOnMainThread(() -> {
                        callback.onResult(out);
                    });
                    break;
                }
            }

            out.add(newValue);
            Threads.executeOnMainThread(() -> {
                callback.onResult(out);
            });
        });
    }

    public static <T> List<T> add(@NonNull Collection<T> in, @NonNull T newValue) {
        List<T> out = new ArrayList<>(in.size() + 1);
        out.addAll(in);
        out.add(newValue);
        return out;
    }

    public static <T> List<T> add(@NonNull T newValue, @NonNull Collection<T> in) {
        List<T> out = new ArrayList<>(in.size() + 1);
        out.add(newValue);
        out.addAll(in);
        return out;
    }

    public static <T> void add(@NonNull Collection<T> in, @NonNull T newValue, @NonNull ResultCallback<List<T>> callback) {
        Threads.runAsync(() -> {
            return add(in, newValue);
        }).onCompleted(callback::onResult);
    }

    public static <T> List<T> delete(@NonNull Collection<T> in, @NonNull Predicate<T> function) {
        List<T> out = new ArrayList<>(in.size());
        Iterator<T> it = in.iterator();

        while (it.hasNext()) {
            T value = it.next();

            if (function.test(value)) {
                break;
            }

            out.add(value);
        }

        while (it.hasNext()) {
            out.add(it.next());
        }

        return out;
    }

    public static <T> void delete(@NonNull Collection<T> in, @NonNull Predicate<T> function, @NonNull ResultCallback<List<T>> callback) {
        Threads.runAsync(() -> delete(in, function))
                .onCompleted(callback::onResult);
    }

    @CheckResult
    public static <T extends Comparable<? super T>> List<T> sortUnique(List<T> in) {
        List<T> out = new ArrayList<>(in);
        Collections.sort(out);

        if (!out.isEmpty()) {
            ListIterator<T> it = out.listIterator();
            T e = it.next();

            while (it.hasNext()) {
                T c = it.next();

                if (e.compareTo(c) == 0) {
                    it.remove();
                } else {
                    e = c;
                }
            }
        }

        return out;
    }

    @CheckResult
    public static <T> int count(@NonNull Collection<T> in, @NonNull Predicate<T> function) {
        int count = 0;
        for (T e : in) {
            if (function.test(e))
                count++;
        }

        return count;
    }

    @CheckResult
    public static <I, O> List<O> map(@NonNull Collection<I> in, @NonNull Function<I, O> function) {
        List<O> out = new ArrayList<>(in.size());

        for (I o : in) {
            out.add(function.apply(o));
        }

        return out;
    }

    @CheckResult
    public static <I, O> List<O> map(@NonNull I[] in, @NonNull Function<I, O> function) {
        List<O> out = new ArrayList<>(in.length);

        for (I o : in) {
            out.add(function.apply(o));
        }

        return out;
    }

    @CheckResult
    public static <T> List<T> filter(@NonNull Collection<T> in, @NonNull Predicate<T> function) {
        List<T> out = new ArrayList<>(in.size());

        for (T e : in) {
            if (function.test(e))
                out.add(e);
        }

        return out;
    }

    @CheckResult
    public static <T> T find(@Nullable Collection<T> in, @NonNull Predicate<T> function) {
        if (in != null) {
            for (T e : in) {
                if (function.test(e)) {
                    return e;
                }
            }
        }

        return null;
    }

    @CheckResult
    public static <T> int indexOf(@Nullable Collection<T> in, @NonNull Predicate<T> function) {
        int index = -1;

        if (in != null) {
            for (T e : in) {
                index++;
                if (function.test(e)) {
                    return index;
                }
            }
        }

        return index;
    }

    public static <T> void each(Iterable<T> source, @NonNull ResultCallback<T> callback) {
        if (source != null) {
            for (T e : source) {
                callback.onResult(e);
            }
        }
    }

    public static <T> void each(T[] source, @NonNull ResultCallback<T> callback) {
        if (source != null) {
            for (T e : source) {
                callback.onResult(e);
            }
        }
    }

    public static <T> void eachNonNull(Iterable<T> source, @NonNull ResultCallback<T> callback) {
        if (source != null) {
            for (T e : source) {
                if (e != null) {
                    callback.onResult(e);
                }
            }
        }
    }

    public static <T> void eachNonNull(T[] source, @NonNull ResultCallback<T> callback) {
        if (source != null) {
            for (T e : source) {
                if (e != null) {
                    callback.onResult(e);
                }
            }
        }
    }

    public interface ResultCallback<T> {
        void onResult(T result);
    }
}
