/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 17/06/19 11:30 AM
 */

package io.simgulary.cms.utils

import java.util.*

interface Identifiable {
    val id: Long

    val updatedAt: Date?
        get() = null

    val deletedAt: Date?
        get() = null

    fun getUpdatedAtMillis(): Long {
        return updatedAt?.time ?: 0L
    }

    fun getDeletedAtMillis(): Long {
        return deletedAt?.time ?: 0L
    }
}
