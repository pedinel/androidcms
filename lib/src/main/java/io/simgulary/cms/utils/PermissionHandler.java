/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified by Pedro Parra 10/12/18 04:56 PM
 */

package io.simgulary.cms.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.SystemClock;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


@SuppressWarnings({"WeakerAccess", "unused"})
public final class PermissionHandler {
	private static final Logger log = Logger.getLogger(PermissionHandler.class);

	/**
	 * Singleton instance
	 */
	private static volatile PermissionHandler instance;
	@SuppressLint("UseSparseArrays")
	private final HashMap<Integer, PendingPermission> pendingRequest = new HashMap<>();

	private PermissionHandler() {
	}

	/**
	 * Checks for permissions
	 *
	 * @param context the Activity Context
	 * @param l the Callback listener
	 * @param permissions the Permissions to ask
	 */
	public void checkPermission(Activity context, PermissionListener l, String... permissions) {
		final List<String> notGrantedPermissions = new ArrayList<>(permissions.length);

		log.info("Checking permissions: %s", TextUtils.join(", ", permissions));

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			for (String permission : permissions) {
				if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
					notGrantedPermissions.add(permission);
				}
			}
		}

		if (!notGrantedPermissions.isEmpty()) {
			log.info("Missing permissions: %s", TextUtils.join(", ", notGrantedPermissions));

			l.onPermissionChecked(notGrantedPermissions, () -> {
				add(notGrantedPermissions, context, l);
			});
		}
		else {
			l.onPermissionGranted();
		}
	}

	public static synchronized PermissionHandler getInstance() {
		if (instance == null) {
			instance = new PermissionHandler();
		}

		return instance;
	}

	private void add(List<String> notGrantedPermissions, Activity context, PermissionListener l) {
		PendingPermission p = new PendingPermission(notGrantedPermissions, l);
		pendingRequest.put(p.requestCode, p);
		p.request(context);
	}

	/**
	 * Handles the result given on {@link Activity#onActivityResult(int, int, Intent)}.
	 *
	 * @param requestCode the Request code
	 * @param grantResults the Grant Results
	 * @return {@code true} if the result is consumed
	 */
	public boolean handleResult(int requestCode, int[] grantResults) {
		PendingPermission p = pendingRequest.remove(requestCode);

		if (p != null) {
			p.onResult(grantResults);
			return true;
		}
		else {
			return false;
		}
	}

	public interface PermissionListener {
		/**
		 * The permissions were checked. This allows to display a dialog or information before continuing.
		 *
		 * @param missingPermissions the missing permissions list. If empty, this method is not triggered.
		 * @param askPermissionsTask the task to ask for permissions. Just call {@link Runnable#run()}.
		 */
		default void onPermissionChecked(@NonNull List<String> missingPermissions, @NonNull Runnable askPermissionsTask) {
			askPermissionsTask.run();
		}

		void onPermissionGranted();

		default void onPermissionDenied() {}
	}

	static final class PendingPermission {
		private final String[] notGrantedPermissions;
		private final int requestCode;
		private final PermissionListener listener;

		private PendingPermission(List<String> notGrantedPermissions, PermissionListener l) {
			this.listener = l;
			this.notGrantedPermissions = notGrantedPermissions.toArray(new String[notGrantedPermissions.size()]);
			this.requestCode = (int) (SystemClock.elapsedRealtime() & 0x0000FFFF);
		}

		private void request(Activity context) {
			ActivityCompat.requestPermissions(context, notGrantedPermissions, requestCode);
		}

		private void onResult(int[] grantResults) {
			for (int grantResult : grantResults) {
				if (grantResult != PackageManager.PERMISSION_GRANTED) {
					listener.onPermissionDenied();
					return;
				}
			}

			if (grantResults.length > 0) {
				listener.onPermissionGranted();
			}
			else {
				listener.onPermissionDenied();
			}
		}
	}
}
