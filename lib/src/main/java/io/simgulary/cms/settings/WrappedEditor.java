/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */

package io.simgulary.cms.settings;

import org.json.JSONArray;

import java.util.List;
import java.util.Set;

import androidx.annotation.Nullable;

/**
 * Wrapper Implementation
 */

final class WrappedEditor implements Editor {
    private final WrappedPreferences pref;
    private volatile android.content.SharedPreferences.Editor w;

    WrappedEditor(WrappedPreferences preferences) {
        this.pref = preferences;
    }

    private android.content.SharedPreferences.Editor edit() {
        if (w == null) {
            w = pref.w.edit();
        }

        return w;
    }

    @Override
    public Editor putString(String key, @Nullable String value) {
        pref.stringCache.set(key, value);
        edit().putString(key, value);
        return this;
    }

    @Override
    public Editor putStringSet(String key, @Nullable Set<String> values) {
        pref.stringSetCache.set(key, values);
        edit().putStringSet(key, values);
        return this;
    }

    @Override
    public Editor putStringList(String key, @Nullable List<String> values) {
        putString(key, new JSONArray(values).toString());
        return this;
    }

    @Override
    public Editor putInt(String key, int value) {
        pref.integerCache.set(key, value);
        edit().putInt(key, value);
        return this;
    }

    @Override
    public Editor putLong(String key, long value) {
        pref.longCache.set(key, value);
        edit().putLong(key, value);
        return this;
    }

    @Override
    public Editor putFloat(String key, float value) {
        pref.floatCache.set(key, value);
        edit().putFloat(key, value);
        return this;
    }

    @Override
    public Editor putBoolean(String key, boolean value) {
        pref.booleanCache.set(key, value);
        edit().putBoolean(key, value);
        return this;
    }

    @Override
    public Editor remove(String key) {
        Cache cache = pref.mapper.remove(key);

        if (cache != null)
            cache.remove(key);

        edit().remove(key);

        return this;
    }

    @Override
    public Editor clear() {
        for (Cache cache: pref.mapper.values()) {
            cache.clear();
        }

        pref.mapper.clear();

        edit().clear();

        return this;
    }

    @Override
    public boolean hasChanges() {
        return w != null;
    }

    @Override
    public synchronized boolean commit() {
        if (w != null) {
            if (w.commit()) {
                w = null;
                return true;
            }
        }

        return false;
    }

    @Override
    public synchronized void apply() {
        if (w != null) {
            w.apply();
            w = null;
        }
    }
}
