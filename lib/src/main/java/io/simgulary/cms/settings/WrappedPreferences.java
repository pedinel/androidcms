/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */

package io.simgulary.cms.settings;

import android.content.Context;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Wrapper Implementation
 */

final class WrappedPreferences implements SharedPreferences {
    private static final HashMap<String, SharedPreferences> loadedPreferences = new HashMap<>();
    final Cache<String> stringCache = new Cache<>(this);
    final Cache<Integer> integerCache = new Cache<>(this);
    final Cache<Float> floatCache = new Cache<>(this);
    final Cache<Set<String>> stringSetCache = new Cache<>(this);
    final Cache<Long> longCache = new Cache<>(this);
    final Cache<Boolean> booleanCache = new Cache<>(this);
    final ConcurrentHashMap<String, Cache> mapper = new ConcurrentHashMap<>();
    final android.content.SharedPreferences w;
    private io.simgulary.cms.settings.Editor editor;

    private WrappedPreferences(@NonNull android.content.SharedPreferences sharedPreferences) {
        this.w = sharedPreferences;
    }

    static SharedPreferences load(Context context, String prefName) {
        SharedPreferences loaded = loadedPreferences.get(prefName);

        if (loaded == null) {
            loaded = new WrappedPreferences(context.getSharedPreferences(prefName, Context.MODE_PRIVATE));
            loadedPreferences.put(prefName, loaded);
        }

        return loaded;
    }

    @Override
    public Map<String, ?> getAll() {
        return w.getAll();
    }

    @Nullable
    @Override
    public String getString(String key, @Nullable String defValue) {
        return stringCache.has(key)
                ? stringCache.get(key)
                : stringCache.set(key, w.getString(key, defValue));
    }

    @Nullable
    @Override
    public Set<String> getStringSet(String key, @Nullable Set<String> defValues) {
        return stringSetCache.has(key)
                ? stringSetCache.get(key)
                : stringSetCache.set(key, w.getStringSet(key, defValues));
    }

    @Override
    public List<String> getStringList(String key, @Nullable List<String> defValues) {
        String v = stringCache.has(key)
                ? stringCache.get(key)
                : stringCache.set(key, w.getString(key, (defValues != null) ? new JSONArray(defValues).toString() : "[]"));

        try {
            JSONArray jsonArray = new JSONArray(v);
            ArrayList<String> o = new ArrayList<>(jsonArray.length());

            for (int i = 0; i < jsonArray.length(); ++i) {
                o.add(jsonArray.getString(i));
            }

            return o;
        }
        catch (Throwable t) {
            throw new RuntimeException("Invalid Json " + v, t);
        }
    }

    @Override
    public int getInt(String key, int defValue) {
        //noinspection ConstantConditions
        return integerCache.has(key)
                ? integerCache.get(key)
                : integerCache.set(key, w.getInt(key, defValue));
    }

    @Override
    public long getLong(String key, long defValue) {
        //noinspection ConstantConditions
        return longCache.has(key)
                ? longCache.get(key)
                : longCache.set(key, w.getLong(key, defValue));
    }

    @Override
    public float getFloat(String key, float defValue) {
        //noinspection ConstantConditions
        return floatCache.has(key)
                ? floatCache.get(key)
                : floatCache.set(key, w.getFloat(key, defValue));
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        //noinspection ConstantConditions
        return booleanCache.has(key)
                ? booleanCache.get(key)
                : booleanCache.set(key, w.getBoolean(key, defValue));
    }

    @Override
    public boolean contains(String key) {
        return w.contains(key);
    }

    @Override
    public io.simgulary.cms.settings.Editor edit() {
        return getEditor();
    }

    @Override
    public boolean changed() {
        return editor != null && editor.hasChanges();
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
        w.registerOnSharedPreferenceChangeListener(listener);
    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
        w.unregisterOnSharedPreferenceChangeListener(listener);
    }

    private synchronized io.simgulary.cms.settings.Editor getEditor() {
        if (editor == null)
            editor = new WrappedEditor(this);

        return editor;
    }
}
