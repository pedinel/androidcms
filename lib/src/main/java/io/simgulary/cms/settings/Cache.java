/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>
 * Last Modified 07/09/18 06:10 PM
 */

package io.simgulary.cms.settings;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Settings Cache (to prevent reading the value from local file every-time)
 */

final class Cache<T> {
    /**
     * Cached values
     */

    private static final Object nullValue = new Object();
    private final ConcurrentHashMap<String, Object> values = new ConcurrentHashMap<>();
    private final WrappedPreferences w;

    Cache(WrappedPreferences wrappedPreferences) {
        w = wrappedPreferences;
    }

    /**
     * Reads cache
     *
     * @param key Key
     * @return value
     */
    T get(String key) {
        Object v = values.get(key);

        //noinspection unchecked
        return (v != nullValue) ? (T) v : null;
    }

    /**
     * Writes cache
     *
     * @param key Key
     * @param value Value
     * @return the Value
     */
    T set(String key, T value) {
        values.put(key, (value != null) ? value : nullValue);

        if (!w.mapper.containsKey(key))
            w.mapper.put(key, this);

        return value;
    }

    boolean has(String key) {
        return values.containsKey(key);
    }

    /**
     * Deletes cache
     *
     * @param key Key
     * @return the Value
     */
    @SuppressWarnings("UnusedReturnValue")
    T remove(String key) {
        Object v = values.remove(key);

        //noinspection unchecked
        return (v != nullValue) ? (T) v : null;
    }

    /**
     * Clears cache
     */
    void clear() {
        values.clear();
    }
}
