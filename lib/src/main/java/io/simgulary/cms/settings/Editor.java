/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */

package io.simgulary.cms.settings;

import android.content.SharedPreferences;

import java.util.List;
import java.util.Set;

import androidx.annotation.Nullable;

/**
 * Editor Wrapper
 */

public interface Editor extends SharedPreferences.Editor {
    @Override
    Editor putString(String key, @Nullable String value);

    @Override
    Editor putStringSet(String key, @Nullable Set<String> values);

    /**
     * Set a ordered list of String values in the preferences editor, to be written
     * back once {@link #commit} or {@link #apply} is called.
     *
     * @param key The name of the preference to modify.
     * @param values The set of new values for the preference.  Passing {@code null}
     *    for this argument is equivalent to calling {@link #remove(String)} with
     *    this key.
     * @return Returns a reference to the same Editor object, so you can
     * chain put calls together.
     */
    Editor putStringList(String key, @Nullable List<String> values);

    @Override
    Editor putInt(String key, int value);

    @Override
    Editor putLong(String key, long value);

    @Override
    Editor putFloat(String key, float value);

    @Override
    Editor putBoolean(String key, boolean value);

    @Override
    Editor remove(String key);

    @Override
    Editor clear();

    /**
     * Checks if the content changed
     *
     * @return {@code true} if editor has changes
     */
    boolean hasChanges();
}
