/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/06/19 03:38 PM
 */

package io.simgulary.cms.settings;

import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import io.simgulary.cms.threads.Threads;

class ObservableConfig<T> extends LiveData<T> {
    private final String prefKey;
    private final ConfigX configX;
    private final android.content.SharedPreferences.OnSharedPreferenceChangeListener listener;
    private final Class<T> type;
    private final T defaultValue;
    private boolean validate = false;
    @Nullable private Delegated<T> delegated;

    ObservableConfig(String prefKey, ConfigX configX, Class<T> type, T defaultValue) {
        this.prefKey = prefKey;
        this.configX = configX;

        listener = (sharedPreferences, key) -> {
            if (prefKey.equals(key)) {
                T value = getDelegated().execute();
                setValue(value);
            }
        };
        this.type = type;
        this.defaultValue = defaultValue;
    }

    @Override
    protected void onActive() {
        super.onActive();

        update();

        configX.registerOnSharedPreferenceChangeListener(listener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();

        configX.unregisterOnSharedPreferenceChangeListener(listener);
    }

    private void update() {
        Threads.executeLastAsync(this, () -> {
            T value = getDelegated().execute();
            postValue(value);
        });
    }

    @Override
    protected void setValue(T value) {
        if (validate) {
            T old = getValue();

            if ((value == old) || (value != null && value.equals(old))) {
                return;
            }
        } else {
            validate = true;
        }

        super.setValue(value);
    }

    private synchronized Delegated<T> getDelegated() {
        if (delegated == null) {
            if (type.equals(Integer.class)) {
                delegated = this::intValue;
            } else if (type.equals(Long.class)) {
                delegated = this::longValue;
            } else if (type.equals(Float.class)) {
                delegated = this::floatValue;
            } else if (type.equals(String.class)) {
                delegated = this::stringValue;
            } else if (type.equals(Boolean.class)) {
                delegated = this::booleanValue;
            } else if (type.equals(Set.class)) {
                delegated = this::stringSetValue;
            } else {
                throw new IllegalArgumentException("type " + type + " not allowed");
            }
        }

        return delegated;
    }

    private T intValue() {
        return type.cast(configX.pref().getInt(prefKey, (Integer) defaultValue));
    }

    private T longValue() {
        return type.cast(configX.pref().getLong(prefKey, (Long) defaultValue));
    }

    private T floatValue() {
        return type.cast(configX.pref().getFloat(prefKey, (Float) defaultValue));
    }

    private T stringValue() {
        return type.cast(configX.pref().getString(prefKey, (String) defaultValue));
    }

    private T booleanValue() {
        return type.cast(configX.pref().getBoolean(prefKey, (Boolean) defaultValue));
    }

    private T stringSetValue() {
        //noinspection unchecked
        return type.cast(configX.pref().getStringSet(prefKey, (Set<String>) defaultValue));
    }

    private interface Delegated<T> {
        T execute();
    }

    @NonNull
    @Override
    public String toString() {
        return prefKey + " = " + getValue();
    }
}
