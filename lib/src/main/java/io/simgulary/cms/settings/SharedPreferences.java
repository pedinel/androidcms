/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */

package io.simgulary.cms.settings;

import java.util.List;

import androidx.annotation.Nullable;

/**
 * Shared Preferences Wrapper
 */

@SuppressWarnings("unused")
public interface SharedPreferences extends android.content.SharedPreferences {
    @Nullable
    List<String> getStringList(String key, @Nullable List<String> defValues);

    @Override
    io.simgulary.cms.settings.Editor edit();

    /**
     * Checks if the preferences has changed
     *
     * @return {@code true} if the preferences has changed
     */
    boolean changed();
}
