/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/06/19 03:35 PM
 */

package io.simgulary.cms.settings;

import android.annotation.SuppressLint;
import android.content.Context;

import java.util.HashSet;

import androidx.annotation.CallSuper;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import io.simgulary.cms.threads.Threads;


/**
 * Settings Handler
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class ConfigX {
	protected static final String DEFAULT_PREF_NAME = "settings";
	private static final String VERSION = "config_x_version";
	private final SharedPreferences preferences;
	private final HashSet<SharedPreferences.OnSharedPreferenceChangeListener> listeners = new HashSet<>();

	protected ConfigX(@Nullable String prefName, @NonNull Context context) {
		if (prefName == null || prefName.length() == 0) {
			prefName = DEFAULT_PREF_NAME;
		}

		preferences = WrappedPreferences.load(context, prefName);

		Threads.executeAsync(this::onBoot);
	}

	@CallSuper
	public void save() {
		synchronized (this) {
			if (hasUnsavedChanges()) {
				edit().apply();
			}
		}
	}

	@CheckResult
	public final boolean hasUnsavedChanges() {
		synchronized (this) {
			return preferences.changed();
		}
	}

	public final void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener l) {
		synchronized (listeners) {
			if (!listeners.contains(l)) {
				listeners.add(l);
				preferences.registerOnSharedPreferenceChangeListener(l);
			}
		}
	}

	public final void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener l) {
		synchronized (listeners) {
			if (listeners.contains(l)) {
				listeners.remove(l);
				preferences.unregisterOnSharedPreferenceChangeListener(l);
			}
		}
	}

	@CheckResult
	public final <T> LiveData<T> observable(String key, T defaultValue, Class<T> type) {
		return new ObservableConfig<>(key, this, type, defaultValue);
	}

	protected int getCurrentVersion() {
		return 1;
	}

	@SuppressLint("CommitPrefEdits")
	@CheckResult
	protected synchronized Editor edit() {
		return preferences.edit();
	}

	@CheckResult
	protected SharedPreferences pref() {
		return preferences;
	}

	@WorkerThread
	private synchronized void onBoot() {
		android.content.SharedPreferences w = ((WrappedPreferences) pref()).w;
		int oldVersion = w.getInt(VERSION, 1);
		int newVersion = getCurrentVersion();

		if (oldVersion != newVersion) {
			onVersionUpdated(oldVersion, newVersion);
			w.edit().putInt(VERSION, newVersion).apply();
		}
	}

	@WorkerThread
	protected void onVersionUpdated(int oldVersion, int newVersion) {
	}
}
