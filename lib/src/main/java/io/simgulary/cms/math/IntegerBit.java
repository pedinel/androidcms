/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>
 * Last Modified 21/08/18 02:19 PM
 */

package io.simgulary.cms.math;

/**
 * BitCode Helper
 */

@SuppressWarnings("unused")
public abstract class IntegerBit {
    public abstract int bitSet(int value);

    public abstract int value(int bitSet);

    public final <T> T value(int bitSet, T[] values) {
        final int v = value(bitSet);

        if (v < values.length) {
            return values[v];
        }
        else {
            return null;
        }
    }

    abstract int offset();

    abstract int length();

    abstract int mask();

    public static IntegerBit create(int length) {
        return new FirstBit(length);
    }

    public static <T> IntegerBit create(T[] values) {
        return create(MathUtils.bits(values.length));
    }

    public static IntegerBit create(int length, int offset) {
        if (offset == 0) {
            return create(length);
        }
        else {
            return new NonFirstBit(length, offset);
        }
    }

    public static IntegerBit create(int length, IntegerBit prev) {
        return new NonFirstBit(length, prev);
    }

    public static <T> IntegerBit create(T[] values, IntegerBit prev) {
        return create(MathUtils.bits(values.length), prev);
    }

    @Override
    public String toString() {
        return String.format("Bit{%s}", Integer.toBinaryString(mask()));
    }

    private static final class FirstBit extends IntegerBit {
        private final int length;
        private final int mask;

        FirstBit(int length) {
            this.length = length;
            this.mask = (1 << length) - 1;
        }

        @Override
        public int bitSet(int value) {
            return value & mask;
        }

        @Override
        public int value(int bitSet) {
            return bitSet & mask;
        }

        @Override
        int offset() {
            return 0;
        }

        @Override
        int length() {
            return length;
        }

        @Override
        int mask() {
            return mask;
        }
    }

    private static final class NonFirstBit extends IntegerBit {
        private final int offset;
        private final int length;
        private final int mask;

        NonFirstBit(int length, int offset) {
            this.length = length;
            this.offset = offset;
            this.mask = ((1 << length) - 1) << offset;
        }

        NonFirstBit(int length, IntegerBit prev) {
            this(length, prev.offset() + prev.length());
        }

        @Override
        public int bitSet(int value) {
            return (value << offset) & mask;
        }

        @Override
        public int value(int bitSet) {
            return (bitSet & mask) >>> offset;
        }

        @Override
        int offset() {
            return offset;
        }

        @Override
        int length() {
            return length;
        }

        @Override
        int mask() {
            return mask;
        }
    }
}
