/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 09:03 AM
 */

package io.simgulary.cms.math;

import androidx.annotation.Nullable;

public final class NumberUtils {
    private NumberUtils() {
        //no instance
    }

    public static boolean equals(@Nullable Number v1, @Nullable Number v2) {
        return (v1 == null) ? (v2 == null) : v1.equals(v2);
    }

    public static <T extends Comparable<T>> int compare(@Nullable T v1, @Nullable T v2) {
        if (v1 == null) {
            if (v2 == null)
                return 0;
            else
                return -1;
        } else if (v2 == null) {
            return 1;
        } else {
            return v1.compareTo(v2);
        }
    }
}
