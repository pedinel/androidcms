/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 23/01/20 11:49 AM
 */

package io.simgulary.cms.math;

import androidx.annotation.CheckResult;
import androidx.annotation.IntRange;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * Math Utilities
 */

@SuppressWarnings({"unused", "WeakerAccess"})
public final class MathUtils {
    private static final double E6 = 1000000.0;
    private static final double E2 = 100.0;
    private MathUtils() {
        //no instance
    }

    /**
     * Forces a value withing a range
     *
     * @param value the value
     * @param min the min range
     * @param max the max range
     * @return the clamped value
     */
    @CheckResult
    public static int clamp(int value, int min, int max) {
        return (value <= min) ? min : (value >= max) ? max : value;
    }

    /**
     * Forces a value withing a range
     *
     * @param value the value
     * @param min the min range
     * @param max the max range
     * @return the clamped value
     */
    @CheckResult
    public static float clamp(float value, float min, float max) {
        return (value <= min) ? min : (value >= max) ? max : value;
    }

    /**
     * Forces a value withing a range
     *
     * @param value the value
     * @param min the min range
     * @param max the max range
     * @return the clamped value
     */
    @CheckResult
    public static double clamp(double value, double min, double max) {
        return (value <= min) ? min : (value >= max) ? max : value;
    }

    public static int bits(int i) {
        if (i == 0)
            return 0;

        int n = 0;
        if ((i & 0xffff0000) != 0) { n = 0b10000; i >>>= 0b10000; }
        if ((i & 0xff00) != 0) { n |= 0b1000; i >>= 0b1000; }
        if ((i & 0xf0) != 0) { n |= 0b100; i >>= 0b100; }
        if ((i & 0xc) != 0) { n |= 0b10; i >>= 0b10; }
        if ((i & 0x2) != 0) { n |= 0b1; i >>= 0b1; }
        if (i != 0) { n += 1; }

        return n;
    }

    public static int log2(int i) {
        if (i == 0)
            return 0;

        int n = 0;
        if ((i & 0xffff0000) != 0) { n = 0b10000; i >>>= 0b10000; }
        if ((i & 0xff00) != 0) { n |= 0b1000; i >>= 0b1000; }
        if ((i & 0xf0) != 0) { n |= 0b100; i >>= 0b100; }
        if ((i & 0xc) != 0) { n |= 0b10; i >>= 0b10; }
        if ((i & 0x2) != 0) { n |= 0b1; }

        return n;
    }

    /**
     * Integer pow 2^x (x >= 0; x < 32)
     *
     * @param x the exponent
     * @return the result of 2^x
     */
    public static int powOf2(int x) {
        return (1 << x);
    }

    /**
     * Fast Pow
     * @param v the value
     * @param e the exponent (positive non zero)
     * @return the value ^ exponent
     */
    public static int pow(int v, @IntRange(from = 0) int e) {
        if (e < 0) {
            throw new IllegalArgumentException();
        }

        if (e == 0) {
            return 1;
        }
        else {
            return fastPow(v, e);
        }
    }

    public static long pow(long v, @IntRange(from = 0) long e) {
        if (e < 0) {
            throw new IllegalArgumentException();
        }

        if (e == 0) {
            return 1;
        }
        else {
            return fastPow(v, e);
        }
    }

    /**
     * Fast Pow
     * @param v the value
     * @param e the exponent (positive non zero)
     * @return the value ^ exponent
     */
    public static int fastPow(int v, int e) {
        if ((e & 1) == 0) {
            return fastPow(v * v, e >> 1);
        } else if (e == 1) {
            return v;
        } else {
            return v * fastPow(v * v, e >> 1);
        }
    }

    public static long fastPow(long v, long e) {
        if ((e & 1) == 0) {
            return fastPow(v * v, e >> 1);
        } else if (e == 1) {
            return v;
        } else {
            return v * fastPow(v * v, e >> 1);
        }
    }

    public static double fromE6(int v) {
        return v / E6;
    }

    public static double fromE6(long v) {
        return v / E6;
    }

    public static int toE6(double v) {
        return (int) (v * E6);
    }

    public static double fromE2(int v) {
        return v / E2;
    }

    public static double fromE2(long v) {
        return v / E2;
    }

    public static long toE2(double v) {
        return (long) (v * E2);
    }

    public static boolean equals(Number v1, Number v2) {
        if (v1 == null) {
            return v2 == null;
        } else {
            return v1.equals(v2);
        }
    }

    public static boolean equals(Date v1, Date v2) {
        if (v1 == null) {
            return v2 == null;
        } else {
            return v1.equals(v2);
        }
    }

    public static double fromE(int value, int decimates) {
        return (double) value / pow(10, decimates);
    }

    public static double fromE(long value, int decimates) {
        return (double) value / pow(10, decimates);
    }

    public static long toE(double value, int decimates) {
        return (long) (value * pow(10, decimates));
    }

    public static long parseLong(CharSequence text, int decimates) throws NumberFormatException, ParseException {
        if (text == null)
            return 0;

        Number number = NumberFormat.getInstance().parse(text.toString());
        double v = number.doubleValue();
        return Math.round(v * fastPow(10L, decimates));
    }

    public static long ceil(double value) {
        return Math.round(Math.ceil(value));
    }
}
