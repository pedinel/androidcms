/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 14/06/19 09:12 AM
 */

package io.simgulary.cms.app;

public interface OnBackPressedCallback {
    boolean onBackPressed();
}
