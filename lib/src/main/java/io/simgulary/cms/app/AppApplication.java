/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 25/03/20 02:40 PM
 */

package io.simgulary.cms.app;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;

@SuppressWarnings("unused")
public class AppApplication extends Application {
    protected static volatile AppApplication instance;
    private int animDuration = -1;
    protected boolean useLeakCanary = true;

    /**
     * Retrieves the Application from any Context
     *
     * @param context the Context
     * @return Application
     */
    @NonNull
    public static AppApplication from(@NonNull Context context) {
        if (context instanceof AppApplication) {
            return (AppApplication) context;
        } else {
            return (AppApplication) context.getApplicationContext();
        }
    }

    /**
     * Retrieves the Application instance
     *
     * @return Application
     */
    public static AppApplication get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

//        if (useLeakCanary && LeakCanary.isInAnalyzerProcess(this)) {
//            // This process is dedicated to LeakCanary for heap analysis.
//            // You should not init your app in this process.
//            return;
//        }

        //Stores the App instance
        instance = this;

//        if (useLeakCanary && BuildConfig.DEBUG) {
//            LeakCanary.install(this);
//        }
    }

    public final String getDeviceInfo() {
        StringBuilder sb = new StringBuilder();

        sb.append("\n*** Device Info ***")
                .append("\nBrand: ").append(Build.BRAND)
                .append("\nManufacturer: ").append(Build.MANUFACTURER)
                .append("\nDevice: ").append(Build.DEVICE)
                .append("\nProduct: ").append(Build.PRODUCT)
                .append("\nModel: ").append(Build.MODEL)
                .append("\nBoard: ").append(Build.BOARD)
                .append("\nBootloader: ").append(Build.BOOTLOADER)
                .append("\nId: ").append(Build.ID)
                .append("\nHardware: ").append(Build.HARDWARE)
                .append("\nTags: ").append(Build.TAGS);

        sb.append("\n*** Supported ABIs ***");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            for (String abi : Build.SUPPORTED_ABIS) {
                sb.append("\nABI: ").append(abi);
            }
        }

        sb.append("\n*** Firmware ***")
                .append("\nSDK: ").append(Build.VERSION.SDK_INT)
                .append("\nRelease: ").append(Build.VERSION.RELEASE)
                .append("\nIncremental: ").append(Build.VERSION.INCREMENTAL);

        sb.append("\n");

        return sb.toString();
    }

    public int getAnimDuration() {
        if (animDuration == -1) {
            animDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);
        }

        return animDuration;
    }
}
