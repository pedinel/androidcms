/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 23/11/18 10:15 AM
 */

package io.simgulary.cms.app;

import android.content.Intent;

public interface ActivityResultListener {
    void onResult(int resultCode, Intent data);
}
