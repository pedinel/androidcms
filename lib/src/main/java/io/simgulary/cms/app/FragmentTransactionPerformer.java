/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 04/12/18 03:18 PM
 */

package io.simgulary.cms.app;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

interface FragmentTransactionPerformer<A extends BaseActivity<A, F>, F extends BaseFragment<A, F>> {
    @NonNull
    FragmentManager getFragmentManager();

    @NonNull
    Context getContext();

    void onFragmentTransactionReady(@NonNull FragmentTransaction transaction);

    void onFragmentReady(F fragment);

    int getFragmentContainer();
}
