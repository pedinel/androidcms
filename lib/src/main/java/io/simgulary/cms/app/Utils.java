/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 10/05/19 05:07 PM
 */

package io.simgulary.cms.app;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.Transition;
import androidx.transition.TransitionInflater;
import io.simgulary.cms.utils.Iterables;

import static io.simgulary.cms.app.BaseActivity.FRAGMENT_REMOVE_ALL;
import static io.simgulary.cms.app.BaseActivity.FRAGMENT_REMOVE_CURRENT;

final class Utils {
    private Utils() {
        //no instance
    }

    public static <A extends BaseActivity<A, F>, F extends BaseFragment<A, F>>
    void applyFragmentTransaction(FragmentTransactionPerformer<A, F> ftp, F fragment, int mode, Pair[] sharedElements) {
        FragmentManager fm = ftp.getFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        if (mode == FRAGMENT_REMOVE_ALL) {
            int count = fm.getBackStackEntryCount();
            if (count > 0) {
                FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(0);
                fm.popBackStack(entry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fm.executePendingTransactions();
            }
        } else if (mode == FRAGMENT_REMOVE_CURRENT) {
            int count = fm.getBackStackEntryCount();
            if (count > 0) {
                FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(count - 1);
                fm.popBackStack(entry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fm.executePendingTransactions();
            }
            transaction.addToBackStack(fragment.getClass().getName());
        } else {
            transaction.addToBackStack(fragment.getClass().getName());
        }

        if (sharedElements != null && sharedElements.length > 0) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                Transition enterTransition = TransitionInflater.from(ftp.getContext())
                        .inflateTransition(android.R.transition.move);
                Transition exitTransition = TransitionInflater.from(ftp.getContext())
                        .inflateTransition(android.R.transition.explode);
                fragment.setSharedElementEnterTransition(enterTransition);
                fragment.setSharedElementReturnTransition(exitTransition);
            }

            for (Pair pair : sharedElements) {
                if (pair != null) {
                    View view = (View) pair.first;
                    String name = (String) pair.second;
                    transaction.addSharedElement(view, name);
                }
            }
        }

        ftp.onFragmentTransactionReady(transaction);

        ftp.onFragmentReady(fragment);

        transaction.replace(ftp.getFragmentContainer(), fragment, fragment.getClass().getName())
                .commit();
    }

    @CheckResult
    static Bundle getTransitionOptions(@NonNull BaseActivity activity, @Nullable Pair<View, String>[] sharedElements) {
        if (sharedElements == null || sharedElements.length == 0)
            return null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            List<android.util.Pair<View, String>> transitions = new ArrayList<>(sharedElements.length);
            for (Pair<View, String> sharedElement : sharedElements) {
                transitions.add(android.util.Pair.create(sharedElement.first, sharedElement.second));
            }

            //noinspection unchecked
            android.util.Pair<View, String> pairs[] = (android.util.Pair<View, String>[]) new android.util.Pair[sharedElements.length];
            transitions.toArray(pairs);
            return ActivityOptions.makeSceneTransitionAnimation(activity, pairs).toBundle();
        } else {
            List<Pair<View, String>> transitions = new ArrayList<>(sharedElements.length);
            for (Pair<View, String> sharedElement : sharedElements) {
                transitions.add(Pair.create(sharedElement.first, sharedElement.second));
            }

            //noinspection unchecked
            Pair<View, String> pairs[] = (Pair<View, String>[]) new Pair[sharedElements.length];
            transitions.toArray(pairs);
            return ActivityOptionsCompat.makeSceneTransitionAnimation(activity, pairs).toBundle();
        }
    }

    static void setIntentParameters(@NonNull Intent intent, @Nullable Serializable[] params) {
        Iterables.eachNonNull(params, (param) -> {
            intent.putExtra(param.getClass().getName(), param);
        });
    }

    @CheckResult
    static <T extends BaseFragment<?, ?>> boolean popFragmentUntil(@NonNull BaseActivity activity, @NonNull Class<T> fragmentClass, boolean inclusive) {
        FragmentManager fm = activity.getSupportFragmentManager();
        int i = fm.getBackStackEntryCount();

        while (i-- > 0) {
            FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(i);

            if (fragmentClass.getName().equals(entry.getName())) {
                fm.popBackStack(entry.getId(), inclusive ? FragmentManager.POP_BACK_STACK_INCLUSIVE : 0);
                fm.executePendingTransactions();
                return true;
            }
        }

        Log.w("Utils", "popFragmentUntil: " + fragmentClass + " not found in stack " + fm.getFragments());
        return false;
    }

    @CheckResult
    public static <T extends BaseFragment> T getLastFragmentOnStack(FragmentManager fm) {
        int size = fm.getBackStackEntryCount();

        for (int i = size-1; i >= 0; i--) {
            FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(i);
            Fragment fragment = fm.findFragmentByTag(entry.getName());

            if (fragment instanceof BaseFragment) {
                try {
                    //noinspection unchecked
                    return (T) fragment;
                }
                catch (Throwable ignored) {
                }
            }
        }

        return null;
    }
}
