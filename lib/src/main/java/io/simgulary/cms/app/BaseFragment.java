/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/07/20 11:49 AM.
 */

package io.simgulary.cms.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import androidx.annotation.CallSuper;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Pair;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.simgulary.cms.ui.UiUtils;
import io.simgulary.cms.utils.Logger;

@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class BaseFragment<A extends BaseActivity<A, F>, F extends BaseFragment<A, F>> extends Fragment {
    private static final Logger log = Logger.getLogger(BaseFragment.class);
    protected final Observer<Boolean> mLoadingObserver;
    private final AtomicBoolean loadingState = new AtomicBoolean(false);
    private final Handler loadingHandler = new Handler();
    private final Runnable switchLoadingTask = () -> setContentLoading(loadingState.get());
    private final Map<String, Object> paramsCache = new HashMap<>();
    protected View mLoadingView;
    private boolean isVisible = false;
    private FragmentTransactionPerformer<A, F> ftp;

    private ViewModelProvider viewModelProvider;

    @NonNull
    public final ViewModelProvider getViewModelProvider() {
        if (viewModelProvider == null) {
            viewModelProvider = new ViewModelProvider(this);
        }

        return viewModelProvider;
    }

    protected BaseFragment() {
        mLoadingObserver = isLoading -> {
            if (isLoading == null || loadingState.get() == isLoading)
                return;

            loadingState.set(isLoading);

            if (isLoading) {
                loadingHandler.postDelayed(switchLoadingTask, AppApplication.get().getAnimDuration());
            } else {
                loadingHandler.removeCallbacks(switchLoadingTask);
                switchLoadingTask.run();
            }
        };
    }

    @NonNull
    public static <T extends BaseFragment> T newInstance(Class<T> fragmentClass, Serializable... args) {
        final T out;
        try {
            out = fragmentClass.newInstance();
        }
        catch (java.lang.InstantiationException e) {
            log.error(e, "Unable to instantiate fragment %s", fragmentClass);
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e) {
            log.error(e, "Unable to access fragment %s", fragmentClass);
            throw new RuntimeException(e);
        }

        if (args != null && args.length > 0) {
            Bundle bundle = new Bundle();
            for (Serializable arg : args) {
                bundle.putSerializable(arg.getClass().getName(), arg);
            }

            out.setArguments(bundle);
            log.debug("Fragment instantiated: %s %s", out.getClass().getSimpleName(), bundle);
        }

        return out;
    }

    @NonNull
    public static <T extends BaseFragment> T newInstance(Class<T> fragmentClass, FragmentParameter param) {
        final T out;
        try {
            out = fragmentClass.newInstance();
        }
        catch (java.lang.InstantiationException e) {
            log.error(e, "Unable to instantiate fragment %s", fragmentClass);
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e) {
            log.error(e, "Unable to access fragment %s", fragmentClass);
            throw new RuntimeException(e);
        }

        if (param != null) {
            Bundle bundle = new Bundle();
            param.onBindParameters(bundle);
            out.setArguments(bundle);
            log.debug("Fragment instantiated: %s %s", out.getClass().getSimpleName(), bundle);
        }

        return out;
    }

    @NonNull
    public static <T extends BaseFragment> T newInstance(Class<T> fragmentClass, Bundle bundle) {
        final T out;
        try {
            out = fragmentClass.newInstance();
        }
        catch (java.lang.InstantiationException e) {
            log.error(e, "Unable to instantiate fragment %s", fragmentClass);
            throw new RuntimeException(e);
        }
        catch (IllegalAccessException e) {
            log.error(e, "Unable to access fragment %s", fragmentClass);
            throw new RuntimeException(e);
        }

        if (bundle != null) {
            out.setArguments(bundle);
        }

        return out;
    }

    @CheckResult
    public final <T extends Serializable> T getParameter(Class<T> clazz) {
        String key = clazz.getName();

        if (paramsCache.containsKey(key)) {
            return clazz.cast(paramsCache.get(key));
        }

        Bundle args = getArguments();

        if (args == null)
            return null;

        Serializable extra = args.getSerializable(key);

        if (extra == null)
            return null;

        paramsCache.put(key, extra);
        return clazz.cast(extra);
    }

    public final void showNestedFragment(F fragment, @BaseActivity.FragmentMode int mode, Pair... sharedElements) {
        int fragmentContainer = getFragmentContainer();

        if (fragmentContainer == 0) {
            log.error("Fragment container not defined. Override #getFragmentContainer on Activity");
            return;
        }

        Utils.applyFragmentTransaction(getFragmentTransactionPerformer(), fragment, mode, sharedElements);
    }

    /**
     * Called when the user clicks back. This is only triggered when the fragment is Visible.
     * @return {@code true} if the event is consumed. Otherwise, the fragment will be removed.
     */
    public boolean onBackPressed() {
        return false;
    }

    @NonNull
    public final A appActivity() {
        //noinspection unchecked
        return (A) requireActivity();
    }

    protected int getFragmentContainer() {
        return 0;
    }

    protected void onFragmentTransactionReady(FragmentTransaction transaction) {
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                android.R.anim.fade_in, android.R.anim.fade_out);
    }

    protected void onFragmentReady(F fragment) {

    }

    protected final void setContentLoading(boolean isLoading) {
        Activity activity = getActivity();

        if (activity == null)
            return;

        View view = getView();

        if (view == null)
            return;

        if (mLoadingView == null)
            mLoadingView = findLoadingView(view);

        View loadingView = mLoadingView;
        setViewLoading(loadingView, isLoading);
    }

    protected View getLoadingView(@NonNull View root) {
        return null;
    }

    protected final void setViewLoading(@Nullable View view, boolean isLoading) {
        if (view == null)
            return;

        if (view instanceof SwipeRefreshLayout) {
            ((SwipeRefreshLayout) view).setRefreshing(isLoading);
        } else {
            view.setVisibility((isLoading) ? View.VISIBLE : View.GONE);
        }
    }

    protected void onVisible() {

    }

    protected void onHidden() {

    }

    protected final int startActionOnResult(Intent intent, ActivityResultListener callback) {
        return appActivity().startActionOnResult(intent, callback);
    }

    protected void setToolbarTitle(@StringRes int resId) {
        setToolbarTitle(resId, 0);
    }

    @SuppressWarnings("SameParameterValue")
    protected void setToolbarTitle(@StringRes int titleResId, @StringRes int subtitleResId) {
        Toolbar toolbar = appActivity().getToolbar();

        if (toolbar != null) {
            if (titleResId != 0)
                toolbar.setTitle(titleResId);
            else
                toolbar.setTitle(null);

            if (subtitleResId != 0)
                toolbar.setSubtitle(subtitleResId);
            else
                toolbar.setSubtitle(null);
        }
    }

    protected void setToolbarTitle(CharSequence title) {
        setToolbarTitle(title, null);
    }

    @SuppressWarnings("SameParameterValue")
    protected void setToolbarTitle(CharSequence title, CharSequence subtitle) {
        Toolbar toolbar = appActivity().getToolbar();

        if (toolbar != null) {
            toolbar.setTitle(title);
            toolbar.setSubtitle(subtitle);
        }
    }

    private FragmentTransactionPerformer<A, F> getFragmentTransactionPerformer() {
        if (ftp == null) {
            ftp = new FragmentTransactionPerformer<A, F>() {
                @Override
                @NonNull
                public FragmentManager getFragmentManager() {
                    return getChildFragmentManager();
                }

                @NonNull
                @Override
                public Context getContext() {
                    return requireContext();
                }

                @Override
                public void onFragmentTransactionReady(@NonNull FragmentTransaction transaction) {
                    BaseFragment.this.onFragmentTransactionReady(transaction);
                }

                @Override
                public void onFragmentReady(F fragment) {
                    BaseFragment.this.onFragmentReady(fragment);
                }

                @Override
                public int getFragmentContainer() {
                    return BaseFragment.this.getFragmentContainer();
                }
            };
        }

        return ftp;
    }

    private View findLoadingView(@NonNull View root) {
        View view = getLoadingView(root);

        if (view == null) {
            view = appActivity().getLoadingView();
        }

        return view;
    }

    private void onVisibilityChanged(boolean visible)  {
        if (isVisible == visible)
            return;

        isVisible = visible;

        if (visible)
            onVisible();
        else
            onHidden();
    }

    @Override @CallSuper
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        onVisibilityChanged(!hidden);
    }

    @Override @CallSuper
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UiUtils.hideKeyboard(this);
    }

    @Override @CallSuper
    public void onResume() {
        super.onResume();

        onVisibilityChanged(true);
    }

    @Override @CallSuper
    public void onPause() {
        super.onPause();

        onVisibilityChanged(false);
    }

    public interface FragmentParameter {
        void onBindParameters(Bundle bundle);
    }
}
