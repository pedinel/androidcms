/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/07/20 11:49 AM.
 */

package io.simgulary.cms.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.navigation.NavigationView;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import androidx.annotation.CallSuper;
import androidx.annotation.CheckResult;
import androidx.annotation.IdRes;
import androidx.annotation.IntDef;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Pair;
import androidx.core.view.GravityCompat;
import androidx.databinding.Observable;
import androidx.databinding.ObservableBoolean;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.simgulary.cms.threads.Threads;
import io.simgulary.cms.utils.InstanceHelper;
import io.simgulary.cms.utils.Logger;
import io.simgulary.cms.utils.PermissionHandler;

@SuppressWarnings("unused")
public abstract class BaseActivity<A extends BaseActivity<A, F>, F extends BaseFragment<A, F>> extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    protected final Logger log = Logger.getLogger(getClass());
    public static final int FRAGMENT_REMOVE_ALL = 0;
    public static final int FRAGMENT_KEEP_ALL = 1;
    public static final int FRAGMENT_REMOVE_CURRENT = 2;

    protected static final String HAS_PARENT_ACTIVITY = "hasParentActivity";
    protected boolean mHandleBackButton = true;

    @SuppressWarnings("WeakerAccess")
    @IntDef({FRAGMENT_REMOVE_ALL, FRAGMENT_KEEP_ALL, FRAGMENT_REMOVE_CURRENT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FragmentMode {
    }

    private final AtomicInteger mLoadingCount = new AtomicInteger();
    protected final ObservableBoolean mLoadingState = new ObservableBoolean();
    private int mLoadingSwitchDelay = 10;
    private final Handler loadingHandler = new Handler();
    private final Runnable switchLoadingTask = () -> setContentLoading(mLoadingState.get());
    public final Observer<Boolean> mLoadingObserver = isLoading -> {
        if (isLoading == null)
            return;

        int count = (isLoading) ? mLoadingCount.incrementAndGet() : mLoadingCount.decrementAndGet();
        mLoadingState.set(count > 0);
    };

    private final PermissionHandler permissionHandler = PermissionHandler.getInstance();
    private final Map<String, Object> paramsCache = new HashMap<>();
    private FragmentTransactionPerformer<A, F> ftp;
    private boolean mLockDrawer = false;
    private static final ActivityStarter activityStarter = new ActivityStarter();
    private final Set<OnBackPressedCallback> mBackPressedListeners = new LinkedHashSet<>();

    private ViewModelProvider viewModelProvider;

    @NonNull
    public final ViewModelProvider getViewModelProvider() {
        if (viewModelProvider == null) {
            viewModelProvider = new ViewModelProvider(this);
        }

        return viewModelProvider;
    }

    private final FragmentManager.OnBackStackChangedListener backStackListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            boolean unlocked = !mLockDrawer && getSupportFragmentManager().getBackStackEntryCount() == 0;

            if (unlocked) {
                if (mDrawerLayout != null) {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                }

                if (mDrawerToggle != null) {
                    mDrawerToggle.setDrawerIndicatorEnabled(true);
                }
            } else {
                if (mDrawerLayout != null) {
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                }
                if (mDrawerToggle != null) {
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
                }
            }
        }
    };

    @Nullable protected View mLoadingView;
    @Nullable protected AppBarLayout mAppBar;
    @Nullable protected Toolbar mToolbar;
    @Nullable protected ActionBar mActionBar;
    @Nullable protected ActionBarDrawerToggle mDrawerToggle;
    @Nullable protected DrawerLayout mDrawerLayout;
    @Nullable protected NavigationView mNavigationView;

    private final AtomicInteger pendingIntentCode = new AtomicInteger(1000);
    private final Map<Integer, ActivityResultListener> pendingIntents = new HashMap<>();

    public BaseActivity() {
        mLoadingState.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                loadingHandler.removeCallbacks(switchLoadingTask);
                loadingHandler.postDelayed(switchLoadingTask, mLoadingSwitchDelay);
            }
        });
    }

    public final int startActionOnResult(Intent intent, ActivityResultListener callback) {
        int code = pendingIntentCode.incrementAndGet();
        pendingIntents.put(code, callback);
        startActivityForResult(intent, code);
        return code;
    }

    public boolean isFinished() {
        return isFinishing() || getLifecycle().getCurrentState() == Lifecycle.State.DESTROYED;
    }

    /**
     * Checks the permission for any given permission array
     *
     * @param l           the permission result listener
     * @param permissions the permissions to check
     */
    @SuppressWarnings("unused")
    public final void checkPermission(PermissionHandler.PermissionListener l, String... permissions) {
        permissionHandler.checkPermission(this, l, permissions);
    }

    public final <T extends Activity> void startActivity(Class<T> clazz) {
        startActivity(clazz, intent -> {
        });
    }

    /**
     * Starts a new Activity instance using shared elements and serializable parameters.
     * <p>
     * The Serializable parameters are stored using the class name as Key
     *
     * @param clazz the activity class
     * @param <T>   the activity type
     */
    public final <T extends Activity> void startActivity(Class<T> clazz, @NonNull InstanceHelper.InitializerFunction<Intent> init) {
        Intent intent = InstanceHelper.initialize(new Intent(this, clazz), init);
        appendIntentParentInfo(intent);

        activityStarter.launch(this, clazz, intent, null);
    }

    public final <T extends Activity> void startActivity(Class<T> clazz, @Nullable Pair<View, String>[] sharedElements, @NonNull InstanceHelper.InitializerFunction<Intent> init) {
        Bundle options = Utils.getTransitionOptions(this, sharedElements);

        Intent intent = InstanceHelper.initialize(new Intent(this, clazz), init);
        appendIntentParentInfo(intent);

        activityStarter.launch(this, clazz, intent, options);
    }

    protected final void goBackToActivity(@NonNull Class<? extends Activity> clazz, @Nullable Bundle params) {
        Intent i = new Intent(this, clazz);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        if (params != null) {
            i.putExtras(params);
        }

        startActivity(i);
        finish();
    }

    protected final void goBack(@NonNull Class<? extends Activity> clazz, @Nullable Bundle params) {
        //The intent that called this activity instance
        Intent intent = getIntent();

        //The parent class for this activity
        boolean hasParent = intent.getBooleanExtra(HAS_PARENT_ACTIVITY, false);

        if (hasParent) {
            finish();
            return;
        }

        if (getClass() != clazz) {
            goBackToActivity(clazz, params);
        } else {
            super.onBackPressed();
        }

    }

    protected final void goBack(@NonNull Class<? extends Activity> clazz) {
        goBack(clazz, null);
    }

    public static void appendIntentParentInfo(Intent intent) {
        intent.putExtra(HAS_PARENT_ACTIVITY, true);
    }

    /**
     * Retrieves a serializable parameter. This uses a Cache to keep the same object instance for
     * the same object type.
     *
     * @param clazz the class type (used as Key
     * @param <T>   the Object
     * @return the Object instance
     */
    @CheckResult
    public final <T extends Serializable> T getParameter(Class<T> clazz) {
        String key = clazz.getName();

        if (paramsCache.containsKey(key)) {
            return clazz.cast(paramsCache.get(key));
        }

        Intent intent = getIntent();

        if (intent == null)
            return null;

        Serializable extra = intent.getSerializableExtra(key);

        if (extra == null)
            return null;

        paramsCache.put(key, extra);
        return clazz.cast(extra);
    }

    @Override
    public final boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.isChecked()) // avoid re-entering
            return false;

        DrawerLayout drawerLayout = mDrawerLayout;
        ActionBar actionBar = mActionBar;

        assert drawerLayout != null;
        assert actionBar != null;

        if (onNavigationItemSelected(item, item.getItemId())) {
            drawerLayout.closeDrawers();
            return true;
        }

        return false;
    }

    /**
     * Triggered when a navigation item is selected
     *
     * @param item   the Menu Item
     * @param itemId the Menu Item ID
     * @return {@code true} to consume
     */
    protected boolean onNavigationItemSelected(MenuItem item, int itemId) {
        return false;
    }

    /**
     * Retrieves the Fragment View to replace when adding fragments
     *
     * @return the Resource Id
     */
    @IdRes
    protected int getFragmentContainer() {
        return 0;
    }

    public final void showFragment(F fragment, @FragmentMode int mode, Pair... sharedElements) {
        int fragmentContainer = getFragmentContainer();

        if (fragmentContainer == 0) {
            log.error("Fragment container not defined. Override #getFragmentContainer on Activity");
            return;
        }

        Utils.applyFragmentTransaction(getFragmentTransactionPerformer(), fragment, mode, sharedElements);
    }

    public final <T extends F> boolean popFragmentsUntil(Class<T> fragmentClass) {
        return Utils.popFragmentUntil(this, fragmentClass, false);
    }

    public final <T extends F> boolean popFragmentsInclusiveUntil(Class<T> fragmentClass) {
        return Utils.popFragmentUntil(this, fragmentClass, true);
    }

    private FragmentTransactionPerformer<A, F> getFragmentTransactionPerformer() {
        if (ftp == null) {
            ftp = new FragmentTransactionPerformer<A, F>() {
                @Override
                @NonNull
                public FragmentManager getFragmentManager() {
                    return getSupportFragmentManager();
                }

                @NonNull
                @Override
                public Context getContext() {
                    return BaseActivity.this;
                }

                @Override
                public void onFragmentTransactionReady(@NonNull FragmentTransaction transaction) {
                    BaseActivity.this.onFragmentTransactionReady(transaction);
                }

                @Override
                public void onFragmentReady(F fragment) {
                    BaseActivity.this.onFragmentReady(fragment);
                }

                @Override
                public int getFragmentContainer() {
                    return BaseActivity.this.getFragmentContainer();
                }
            };
        }

        return ftp;
    }

    protected void onFragmentReady(F fragment) {

    }

    protected void onFragmentTransactionReady(FragmentTransaction transaction) {
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public final void showFragment(F fragment, Pair... sharedElements) {
        showFragment(fragment, FRAGMENT_REMOVE_ALL, sharedElements);
    }

    public final void addFragment(F fragment, Pair... sharedElements) {
        showFragment(fragment, FRAGMENT_KEEP_ALL, sharedElements);
    }

    public final void replaceFragment(F fragment, Pair... sharedElements) {
        showFragment(fragment, FRAGMENT_REMOVE_CURRENT, sharedElements);
    }

    public final void clearFragmentStack() {
        FragmentManager fm = getSupportFragmentManager();
        int count = fm.getBackStackEntryCount();
        if (count > 0) {
            FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(0);
            fm.popBackStack(entry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fm.executePendingTransactions();
        }
    }

    public final <T extends BaseFragment> T getFragment(Class<T> clazz) {
        Deque<BaseFragment> fragmentList = getFragmentList(getSupportFragmentManager());

        for (BaseFragment baseFragment : fragmentList) {
            if (clazz.equals(baseFragment.getClass()))
                //noinspection unchecked
                return (T) baseFragment;
        }

        return null;
    }

    protected <T extends BaseFragment> T getLastFragmentOnStack() {
        return Utils.getLastFragmentOnStack(getSupportFragmentManager());
    }

    @Nullable
    public final Toolbar getToolbar() {
        return mToolbar;
    }

    @Nullable
    public final AppBarLayout getAppBar() {
        return mAppBar;
    }

    protected final void setContentLoading(boolean isLoading) {
        if (mLoadingView == null) {
            mLoadingView = getLoadingView();
        }

        View loadingView = mLoadingView;
        setViewLoading(loadingView, isLoading);
    }

    protected View getLoadingView() {
        return null;
    }

    protected final void setViewLoading(@Nullable View view, boolean isLoading) {
        if (view == null)
            return;

        if (view instanceof SwipeRefreshLayout) {
            ((SwipeRefreshLayout) view).setRefreshing(isLoading);
        } else {
            view.setVisibility((isLoading) ? View.VISIBLE : View.GONE);
        }
    }

    protected final <T extends BaseFragment> Deque<T> getFragmentList(FragmentManager fm) {
        LinkedList<T> result = new LinkedList<>();

        for (Fragment fragment : fm.getFragments()) {
            if (fragment instanceof BaseFragment) {
                //noinspection unchecked
                result.add((T) fragment);
            }
        }

        return result;
    }

    @Override
    @CallSuper
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (!permissionHandler.handleResult(requestCode, grantResults)) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    @CallSuper
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        ActivityResultListener action = pendingIntents.remove(requestCode);

        if (action != null) {
            Threads.executeOnMainThread(() -> action.onResult(resultCode, data));
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    @CallSuper
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FragmentManager fm = getSupportFragmentManager();
        fm.addOnBackStackChangedListener(backStackListener);
    }


    @Override
    public void onBackPressed() {
        for (OnBackPressedCallback l : mBackPressedListeners) {
            if (l.onBackPressed()) {
                return;
            }
        }

        Intent intent = getIntent();

        //The parent class for this activity
        boolean hasParent = intent.getBooleanExtra(HAS_PARENT_ACTIVITY, false);

        if (hasParent || !mHandleBackButton || !onBackPressedHandled()) {
            super.onBackPressed();
        }
    }

    /**
     * This method is triggered if the back button is pressed and it will close the app. This works
     * only if {@link #mHandleBackButton} is {@code true}.
     *
     * @return {@code true} to avoid closing the app.
     */
    protected boolean onBackPressedHandled() {
        return false;
    }

    @Override
    @CallSuper
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);

            if (mDrawerLayout != null) {
                mActionBar = getSupportActionBar();

                if (mActionBar != null) {
                    mActionBar.setHomeButtonEnabled(true);
                    mActionBar.setDisplayHomeAsUpEnabled(true);
                }

                mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, 0, 0);
                mDrawerToggle.syncState();
                mDrawerToggle.setToolbarNavigationClickListener(v -> onBackPressed());

                mDrawerLayout.addDrawerListener(mDrawerToggle);

                if (mNavigationView != null) {
                    mNavigationView.setItemIconTintList(null);
                    mNavigationView.setNavigationItemSelectedListener(this);
                }
            }
        }

        if (mDrawerLayout != null) {
            backStackListener.onBackStackChanged();
        }

        addOnBackPressedCallback(() -> {
            if (mDrawerLayout != null) {
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawers();
                    return true;
                }
            }

            if (mToolbar != null) {
                Menu menu = mToolbar.getMenu();

                if (menu != null) {
                    int size = menu.size();
                    for (int i = 0; i < size; i++) {
                        View item = menu.getItem(i).getActionView();

                        if (item instanceof SearchView) {
                            SearchView searchView = (SearchView) item;
                            if (!searchView.isIconified()) {
                                searchView.setIconified(true);
                                return true;
                            }
                        }
                    }
                }
            }

            FragmentManager fm = getSupportFragmentManager();
            Deque<BaseFragment> fragmentList = getFragmentList(fm);
            final int fragmentsCount = fragmentList.size();

            if (fragmentsCount > 0) {
                while (!fragmentList.isEmpty()) {
                    BaseFragment last = fragmentList.removeLast();

                    if (!last.isVisible()) {
                        continue;
                    }

                    if (last.onBackPressed()) {
                        return true;
                    }
                }
            }

            return false;
        });

        mLoadingSwitchDelay = AppApplication.get().getAnimDuration();
    }

    protected void setDrawerLocked(boolean locked) {
        if (mLockDrawer == locked) {
            return;
        }
        mLockDrawer = locked;

        if (mDrawerLayout != null) {
            backStackListener.onBackStackChanged();
        }
    }

    @Override
    @CallSuper
    protected void onDestroy() {
        if (mDrawerToggle != null) {
            mDrawerToggle.setToolbarNavigationClickListener(null);
        }

        super.onDestroy();
    }

    public final void addOnBackPressedCallback(@NonNull OnBackPressedCallback callback) {
        mBackPressedListeners.add(callback);
    }

    public final void removeOnBackPressedCallback(@NonNull OnBackPressedCallback callback) {
        mBackPressedListeners.remove(callback);
    }

    static final class ActivityStarter {
        private final long delay = AppApplication.get().getAnimDuration();
        private Class<? extends Activity> lastLaunch;
        private long when;

        void launch(Context context, Class<? extends Activity> activity, Intent intent, @Nullable Bundle options) {
            if (activity.equals(getLastLaunch())) {
                return;
            }

            lastLaunch = activity;
            when = SystemClock.elapsedRealtime();

            Log.i("BaseActivity", "start activity " + activity.getName() + "@" + intent.getExtras());
            if (options == null) {
                context.startActivity(intent);
            } else {
                context.startActivity(intent, options);
            }
        }

        Class<? extends Activity> getLastLaunch() {
            if (lastLaunch != null && SystemClock.elapsedRealtime() - when >= delay) {
                lastLaunch = null;
            }

            return lastLaunch;
        }
    }


}
