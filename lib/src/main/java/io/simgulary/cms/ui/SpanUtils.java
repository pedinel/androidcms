/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */

package io.simgulary.cms.ui;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.StyleSpan;
import android.widget.TextView;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.IllegalFormatPrecisionException;
import java.util.List;
import java.util.UnknownFormatConversionException;

import androidx.annotation.Nullable;

import static android.graphics.Typeface.BOLD;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class SpanUtils {
    private SpanUtils() {
        //no instance
    }

    /**
     * Helper to create an Spannable Text
     *
     * @param v the TextView
     * @param textFormat the Text
     * @param value the Value to span
     * @param l the Span creator
     */
    public static void createSpan(TextView v, String textFormat, @Nullable String value, SpanListener l) {
        final List<Object> spanList = new ArrayList<>();

        List<FormatParam> params = null;

        if (value != null) {
            params = params(textFormat);
        }

        final SpannableString span;
        if (params == null || params.isEmpty()) {
            span = new SpannableString(textFormat);
            l.onCreateSpan(span, spanList);

            for (Object what : spanList) {
                span.setSpan(what, 0, textFormat.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        } else {
            final String formattedText = String.format(textFormat, value);
            span = new SpannableString(formattedText);

            FormatParam param = params.get(0);
            int endIndex = param.begIndex + (formattedText.length() - textFormat.length()) + param.length();

            l.onCreateSpan(span, spanList);

            for (Object what : spanList) {
                span.setSpan(what, param.begIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            }
        }

        spanList.clear();//avoid leaks

        v.setMovementMethod(new LinkMovementMethod());
        v.setText(span);
    }

    /**
     * Helper to create an Spannable Text
     *
     * @param textFormat the Text
     * @param value the Value to span
     */
    public static CharSequence createBold(String textFormat, @Nullable Object value) {
        List<FormatParam> params = null;

        if (value != null) {
            params = params(textFormat);
        }

        final SpannableString span;
        if (params == null || params.isEmpty()) {
            span = new SpannableString(textFormat);
            span.setSpan(new StyleSpan(BOLD), 0, textFormat.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        } else {
            final String formattedText = String.format(textFormat, value);
            span = new SpannableString(formattedText);

            FormatParam param = params.get(0);
            int endIndex = param.begIndex + (formattedText.length() - textFormat.length()) + param.length();
            span.setSpan(new StyleSpan(BOLD), param.begIndex, endIndex, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        return span;
    }

    /**
     * Helper to create an Spannable Text
     *
     * @param v the TextView
     * @param text the Text to span
     * @param l the Span creator
     */
    public static void createSpan(TextView v, String text, SpanListener l) {
        createSpan(v, text, null, l);
    }

    /**
     * Helper to create an Spannable Text
     *
     * @param v the TextView
     * @param text the Text to span
     * @param onClick the Action;
     */
    public static void createSpan(TextView v, String text, ClickableSpan onClick) {
        createSpan(v, text, null, onClick);
    }

    /**
     * Helper to create an Spannable Text
     *
     * @param v the TextView
     * @param textFormat the Text
     * @param value the Value to span
     * @param onClick the Action
     */
    public static void createSpan(TextView v, String textFormat, @Nullable String value, ClickableSpan onClick) {
        createSpan(v, textFormat, value, new SpanListenerImpl(onClick));
    }

    private static List<FormatParam> params(String format) {
        List<FormatParam> result = new ArrayList<>();
        FormatParam last = null;

        for (int i = 0, len = format.length(); i < len; ) {
            int nextPercent = format.indexOf('%', i);
            if (format.charAt(i) != '%') {
                // This is plain-text part, find the maximal plain-text
                // sequence and store it.
                i = (nextPercent == -1) ? len: nextPercent;
            } else {
                // We have a format specifier
                FormatParam fsp = new FormatParam(format, i);

                if (last != null && last.endIndex == fsp.begIndex) {
                    last.endIndex = fsp.endIndex;
                } else {
                    result.add(fsp);
                }

                i = fsp.endIndex;
                last = fsp;
            }
        }

        return result;
    }

    private static class SpanListenerImpl implements SpanListener {
        private final Reference<ClickableSpan> span;

        private SpanListenerImpl(ClickableSpan span) {
            this.span = new WeakReference<>(span);
        }

        @Override
        public void onCreateSpan(SpannableString s, List<Object> spans) {
            ClickableSpan clickableSpan = span.get();

            if (clickableSpan != null) {
                spans.add(clickableSpan);
                spans.add(new StyleSpan(BOLD));
            }
        }
    }

    private static final class FormatParam {
        private final String format;
        int begIndex;
        int endIndex;

        private static final String FLAGS = ",-(+# 0<";

        FormatParam(String format, int begIndex) {
            this.format = format;
            this.begIndex = begIndex;
            endIndex = begIndex + 1;
            String width = null;
            // Index
            if (nextIsInt()) {
                String nint = nextInt();
                if (peek() == '$') {
                    advance();
                } else if (nint.charAt(0) == '0') {
                    // This is a flag, skip to parsing flags.
                    back(nint.length());
                } else {
                    // This is the width, skip to parsing precision.
                    width = nint;
                }
            }
            // Flags
            while (width == null && FLAGS.indexOf(peek()) >= 0) {
                advance();
            }
            // Width
            if (width == null && nextIsInt()) {
                //noinspection UnusedAssignment
                width = nextInt();
            }
            // Precision
            if (peek() == '.') {
                advance();
                if (!nextIsInt()) {
                    throw new IllegalFormatPrecisionException(peek());
                }
                nextInt();
            }
            // tT
            if (peek() == 't' || peek() == 'T') {
                advance();
            }

            advance();
        }

        private int diff(Object arg) {
            if (arg == null) {
                return 0;
            }

            int formatSize = endIndex - begIndex;
            int targetSize = String.format(format.substring(begIndex, endIndex), arg).length();
            return targetSize - formatSize;
        }

        private int length() {
            return endIndex - begIndex;
        }

        private String nextInt() {
            int strBegin = endIndex;
            while (nextIsInt()) {
                advance();
            }
            return format.substring(strBegin, endIndex);
        }

        private boolean nextIsInt() {
            return !isEnd() && Character.isDigit(peek());
        }

        private char peek() {
            if (isEnd()) {
                throw new UnknownFormatConversionException("End of String");
            }
            return format.charAt(endIndex);
        }

        private void advance() {
            if (isEnd()) {
                throw new UnknownFormatConversionException("End of String");
            }
            endIndex++;
        }

        private void back(int len) {
            endIndex -= len;
        }

        private boolean isEnd() {
            return endIndex == format.length();
        }

        private void slice(int count) {
            if (count == 0)
                return;

            begIndex += count;
            endIndex += count;
        }
    }
}
