/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 14/08/20 06:48 PM.
 */

package io.simgulary.cms.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import com.google.android.material.appbar.AppBarLayout;

import androidx.annotation.AnyThread;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.databinding.ObservableBoolean;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import io.simgulary.cms.R;
import io.simgulary.cms.app.AppApplication;
import io.simgulary.cms.app.BaseActivity;
import io.simgulary.cms.threads.Threads;

import static android.view.MotionEvent.ACTION_DOWN;

@SuppressWarnings({"WeakerAccess", "unused"})
public final class UiUtils {
    private UiUtils() {
        //no instance
    }

    /**
     * Use this method to colorize toolbar icons to the desired target color
     * @param toolbarView toolbar view being colored
     * @param toolbarIconsColor the target color of toolbar icons
     * @param activity reference to activity needed to register observers
     */
    public static void colorizeToolbar(Toolbar toolbarView, int toolbarIconsColor, Activity activity) {
        for(int i = 0; i < toolbarView.getChildCount(); i++) {
            final View v = toolbarView.getChildAt(i);

            //Step 1 : Changing the color of back button (or open drawer button).
            if(v instanceof ImageButton) {
                //Action Bar back button
                Drawable drawable = ((ImageButton) v).getDrawable();
                DrawableCompat.setTint(drawable, toolbarIconsColor);
            }

            if(v instanceof ActionMenuView) {
                for(int j = 0; j < ((ActionMenuView)v).getChildCount(); j++) {

                    //Step 2: Changing the color of any ActionMenuViews - icons that are not back button, nor text, nor overflow menu icon.
                    //Colorize the ActionViews -> all icons that are NOT: back button | overflow menu
                    final View innerView = ((ActionMenuView)v).getChildAt(j);

                    if(innerView instanceof ActionMenuItemView) {
                        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) innerView;

                        for(int k = 0; k < actionMenuItemView.getCompoundDrawables().length; k++) {
                            Drawable drawable = actionMenuItemView.getCompoundDrawables()[k];
                            if(drawable != null) {
                                //Important to set the color filter in seperate thread, by adding it to the message queue
                                //Won't work otherwise.
                                innerView.post(() -> DrawableCompat.setTint(drawable, toolbarIconsColor));
                            }
                        }
                    }
                }
            }
        }

        //Step 3: Changing the color of title and subtitle.
        toolbarView.setTitleTextColor(toolbarIconsColor);
        toolbarView.setSubtitleTextColor(toolbarIconsColor);

        //Step 4: Changing the color of the Overflow Menu icon.
        setOverflowButtonColor(activity, toolbarView, toolbarIconsColor);
    }

    /**
     * It's important to set overflowDescription atribute in styles, so we can grab the reference
     * to the overflow icon. Check: res/values/styles.xml
     */
    private static void setOverflowButtonColor(final Activity activity, final Toolbar toolbar, final int toolbarIconsColor) {
        final ViewGroup decorView = (ViewGroup) activity.getWindow().getDecorView();
        final ViewTreeObserver viewTreeObserver = decorView.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                if(toolbar != null && toolbar.getOverflowIcon() != null){
                    Drawable bg = DrawableCompat.wrap(toolbar.getOverflowIcon());
                    DrawableCompat.setTint(bg, toolbarIconsColor);
                }
                removeOnGlobalLayoutListener(decorView,this);
            }
        });
    }

    private static void removeOnGlobalLayoutListener(View v, ViewTreeObserver.OnGlobalLayoutListener listener) {
        v.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
    }

    @AnyThread @CheckResult
    public static ObservableBoolean applyCollapsingToolbarBehaviour(LifecycleOwner owner, BaseActivity activity, AppBarLayout appBar, Toolbar toolbar) {
        final ObservableBoolean stateCollapsed = new ObservableBoolean();

        if (appBar == null || toolbar == null) {
            throw new RuntimeException("applyCollapsingToolbarBehaviour with appBar: "
                    + (appBar != null) + ", toolbar: " + (toolbar != null));
        }

        Threads.executeAsync(() -> {
            //resolve Color Accent
            final TypedValue value = new TypedValue();
            activity.getTheme().resolveAttribute(R.attr.colorAccent, value, true);
            final int colorAccent = getAttribute(activity, R.attr.colorAccent);

            Threads.executeOnMainThread(() -> {
                AppBarLayout.OnOffsetChangedListener listener = (appBarLayout, verticalOffset) -> {
                    if (appBarLayout.getTotalScrollRange() - Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange() / 2) {
                        stateCollapsed.set(false);
                        colorizeToolbar(toolbar, Color.WHITE, activity);
                    } else {
                        stateCollapsed.set(true);
                        colorizeToolbar(toolbar, colorAccent, activity);
                    }
                };

                appBar.addOnOffsetChangedListener(listener);

                owner.getLifecycle().addObserver(new DefaultLifecycleObserver() {
                    @Override
                    public void onDestroy(@NonNull LifecycleOwner owner) {
                        appBar.removeOnOffsetChangedListener(listener);
                        owner.getLifecycle().removeObserver(this);
                    }
                });
            });
        });

        return stateCollapsed;
    }

    @Nullable
    public static ViewGroup findRoot(View view) {
        ViewGroup fallback = view instanceof ViewGroup ? (ViewGroup) view : null;

        do {
            if (view instanceof CoordinatorLayout) {
                // We've found a CoordinatorLayout, use it
                return (ViewGroup) view;
            } else if (view instanceof FrameLayout) {
                if (view.getId() == android.R.id.content) {
                    // If we've hit the decor content view, then we didn't find a CoL in the
                    // hierarchy, so use it.
                    return (ViewGroup) view;
                } else {
                    // It's not the content view but we'll use it as our fallback
                    fallback = (ViewGroup) view;
                }
            } else if (view instanceof ViewGroup) {
                fallback = (ViewGroup) view;
            }

            if (view != null) {
                // Else, we will loop and crawl up the view hierarchy and try to find a parent
                final ViewParent parent = view.getParent();
                view = parent instanceof View ? (View) parent : null;
            }
        } while (view != null);

        // If we reach here then we didn't find a CoL or a suitable content view so we'll fallback
        return fallback;
    }

    public static ViewGroup findRoot(LifecycleOwner owner) {
        View v;
        if (owner instanceof Activity) {
            v = ((Activity) owner).getWindow().getDecorView();
        } else if (owner instanceof Fragment) {
            v = ((Fragment) owner).getView();
        } else if (owner instanceof android.app.Fragment) {
            v = ((android.app.Fragment) owner).getView();
        } else {
            throw new IllegalArgumentException("Subclass of LifecycleOwner not implemented");
        }

        return findRoot(v);
    }

    @NonNull
    public static ViewModelProvider getViewModelProviderOf(LifecycleOwner owner) {
        if (owner instanceof FragmentActivity) {
            return new ViewModelProvider((FragmentActivity) owner);
        } else if (owner instanceof Fragment) {
            return new ViewModelProvider((Fragment) owner);
        } else {
            throw new IllegalArgumentException("unknown owner " + owner);
        }
    }

    @NonNull
    public static Context getContextOf(LifecycleOwner owner) {
        if (owner instanceof FragmentActivity) {
            return (Context) owner;
        } else if (owner instanceof Fragment) {
            return ((Fragment) owner).requireContext();
        } else {
            throw new IllegalArgumentException("unknown owner " + owner);
        }
    }

    @NonNull
    public static FragmentManager getFragmentManagerOf(LifecycleOwner owner) {
        if (owner instanceof FragmentActivity) {
            return ((FragmentActivity) owner).getSupportFragmentManager();
        } else if (owner instanceof Fragment) {
            return ((Fragment) owner).requireFragmentManager();
        } else {
            throw new IllegalArgumentException("unknown owner " + owner);
        }
    }

    @NonNull
    public static FragmentActivity getActivityOf(LifecycleOwner owner) {
        if (owner instanceof FragmentActivity) {
            return (FragmentActivity) owner;
        } else if (owner instanceof Fragment) {
            return ((Fragment) owner).requireActivity();
        } else {
            throw new IllegalArgumentException("unknown owner " + owner);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public static void applyPressAction(View view, View.OnClickListener listener) {
        view.setOnClickListener(listener);

        final PressActionHandler h = new PressActionHandler(view);

        view.setOnLongClickListener(v -> {
            h.press(true);
            return true;
        });

        view.setOnTouchListener((v, e) -> {
            if (e.getAction() != ACTION_DOWN) {
                if (h.pressed) {
                    h.press(false);
                }
            }
            return false;
        });

    }

    public static int getAttribute(Context context, int resId) {
        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(resId, outValue, true);
        return outValue.data;
    }

    private static void setKeyboard(Context context, View view, boolean open) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm == null)
            return;

        if (open) {
            imm.showSoftInput(view, 0);
        } else {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            view.clearFocus();
        }
    }

    public static void showKeyboard(View view) {
        setKeyboard(view.getContext(), view, true);
    }

    public static void hideKeyboard(Activity activity) {
        setKeyboard(activity, activity.getWindow().getDecorView(), false);
    }

    public static void hideKeyboard(Fragment fragment) {
        View view = fragment.getView();
        if (view == null)
            return;
        setKeyboard(fragment.requireContext(), view, false);
    }

    public static void hideKeyboard(Dialog dialog) {
        Window window = dialog.getWindow();
        if (window == null)
            return;

        setKeyboard(dialog.getContext(), window.getDecorView(), false);
    }

    public static Spanned getHtml(@StringRes int resId, Object... formatArgs) {
        String string = AppApplication.get().getString(resId, formatArgs);
        return Html.fromHtml(string);
    }

    private static class PressActionHandler {
        final View view;
        final Handler handler = new Handler();
        final Runnable action;
        boolean pressed;

        private PressActionHandler(View view) {
            this.view = view;
            action = this::apply;
        }

        private void press(boolean state) {
            if (pressed != state) {
                pressed = state;

                if (pressed) {
                    apply();
                }
            }
        }

        private void apply() {
            if (pressed) {
                if (view.isClickable() && view.getVisibility() == View.VISIBLE) {
                    view.performClick();
                    handler.postDelayed(action, 300);
                } else {
                    pressed = false;
                }
            }
        }
    }

    public static int getIntPixelsFromDp(float value) {
        return Math.round(getPixelsFromDp(value));
    }

    public static float getPixelsFromDp(float value) {
        return value * AppApplication.get().getResources().getDisplayMetrics().density;
    }
}
