/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>
 * Last Modified 21/08/18 02:55 PM
 */

package io.simgulary.cms.ui;

import android.text.SpannableString;

import java.util.List;

/**
 * Span creator Listener
 */
public interface SpanListener {
    /**
     * Span creation specs
     *
     * @param s the String
     * @param spans the spans to create in that string
     */
    void onCreateSpan(SpannableString s, List<Object> spans);
}
