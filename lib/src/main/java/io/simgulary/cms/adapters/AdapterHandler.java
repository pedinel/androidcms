/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 06/05/19 04:56 PM
 */

package io.simgulary.cms.adapters;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

/**
 * Helper class for Reactive adapter implementations
 * @param <T> the Adapter data type
 */
final class AdapterHandler<T> {
    /**
     * The LifeCycleOwner that keeps the adapter alive
     */
    final Reference<LifecycleOwner> ownerRef;
    /**
     * The Adapter event notifier
     */
    private final Observer<T> notifier;
    /**
     * The data filter
     */
    Filter<T> filter = null;
    /**
     * The data source that updates the items
     */
    private AttachWrapper<? extends Collection<? extends T>, T> plugged;
    /**
     * The items added to the adapter (raw source)
     */
    private List<T> items = new ArrayList<>();
    /**
     * Displaying items
     */
    private List<T> displayingItems = items;

    AdapterHandler(LifecycleOwner owner, Observer<T> observer) {
        notifier = observer;
        ownerRef = new WeakReference<>(owner);
    }

    void setFilter(Filter<T> filter) {
        if (this.filter == filter)
            return;

        this.filter = filter;

        buildDisplayingItems();
    }

    /**
     * Retrieves the {@link LifecycleOwner} where this {@link Adapter} belongs to
     *
     * @return a {@link LifecycleOwner} object, or {@code null} if the owner does not exists anymore
     */
    LifecycleOwner getOwner() {
        return ownerRef.get();
    }

    <X extends Collection<? extends T>> void setSource(@Nullable LiveData<X> source) {
        LifecycleOwner owner = getOwner();

        if (plugged != null && plugged.source == source)
            return;

        if (plugged != null) {
            plugged.unplug();
            plugged = null;
        }

        if (owner != null && source != null) {
            plugged = new AttachWrapper<>(source, notifier.adapter());
            plugged.plug(owner);
        }
    }

    void setItems(@Nullable Collection<? extends T> newItems) {
        if (items.isEmpty()) {
            if (newItems == null || newItems.isEmpty()) {
                buildDisplayingItems();
                return;
            }
        }

        //update is needed
        if (newItems == null) {
            items = new ArrayList<>();
        } else {
            items = new ArrayList<>(newItems);
        }

        buildDisplayingItems();
    }

    int getItemCount() {
        return displayingItems.size();
    }

    T getItemAt(int position) {
        return displayingItems.get(position);
    }

    long getItemId(int position) {
        T item = getItemAt(position);
        return notifier.getItemId(item);
    }

    private void buildDisplayingItems() {
        Filter<T> filter = this.filter;

        if (filter == null) {
            onItemsChanged(items);
        } else {
            List<T> newItems = new ArrayList<>();
            for (T item : items) {
                if (filter.applies(item)) {
                    newItems.add(item);
                }
            }

            newItems = notifier.onFiltered(newItems);
            onItemsChanged(newItems);
        }
    }

    private void onItemsChanged(List<T> newItems) {
        final List<T> oldItems = displayingItems;
        displayingItems = newItems;
        notifier.onDataSetChanged(oldItems, newItems);
    }
}
