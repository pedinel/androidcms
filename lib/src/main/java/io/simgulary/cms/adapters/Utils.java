/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified by Pedro Parra 30/11/18 12:19 PM
 */

package io.simgulary.cms.adapters;

import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.annotation.AnimRes;

final class Utils {
    private Utils() {
        //no instance
    }

    static void setLayoutAnimation(ViewGroup view, @AnimRes int animRes) {
        if (animRes != 0) {
            LayoutAnimationController anim = AnimationUtils.loadLayoutAnimation(view.getContext(), animRes);
            view.setLayoutAnimation(anim);
        }
    }
}
