/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 11/06/19 10:24 AM
 */

package io.simgulary.cms.adapters;

import android.content.Context;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;

import java.util.Collection;
import java.util.List;

import androidx.annotation.AnimRes;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.simgulary.cms.app.AppApplication;
import io.simgulary.cms.viewmodels.ItemViewModel;

@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class RecyclerAdapter<T, VH extends RecyclerAdapter.ViewHolder<T>> extends RecyclerView.Adapter<VH>
        implements ReactiveAdapter<T> {
    private final ItemPadding itemPadding = new ItemPadding();
/**
     * Operation Handler delegated
     */
    private final AdapterHandler<T> delegated;
    @Nullable private AdapterContentListener<T> contentListener;

    protected RecyclerAdapter(LifecycleOwner owner) {
        setHasStableIds(true);
        delegated = new AdapterHandler<>(owner, new Observer.RecyclerImpl<>(this));
    }

    public final ItemPadding padding() {
        return itemPadding;
    }

    public void setContentListener(@Nullable AdapterContentListener<T> contentListener) {
        this.contentListener = contentListener;
    }

    @Override
    public final void setFilter(Filter<T> filter) {
        delegated.setFilter(filter);
    }

    final void onContentUpdated(List<T> newItems) {
        if (contentListener != null) {
            contentListener.onAdapterContentChanged(this, newItems);
        }
    }
    protected final LifecycleOwner getOwner() {
        return delegated.getOwner();
    }

    @Nullable
    @Override
    public Filter<T> getFilter() {
        return delegated.filter;
    }

    protected long getItemId(T item) {
        return -1;
    }

    protected long getItemLastUpdate(T item) {
        return -1;
    }

    @NonNull
    protected List<T> onFiltered(@NonNull List<T> items) {
        return items;
    }

    @AnimRes
    protected int onGetLayoutAnimation() {
        return 0;
    }

    @Override
    public final <X extends Collection<? extends T>> void setSource(@Nullable LiveData<X> source) {
        delegated.setSource(source);
    }

    @Override
    public final void setItems(@Nullable Collection<? extends T> newItems) {
        delegated.setItems(newItems);
    }

    @Override
    public final void onBindViewHolder(@NonNull VH holder, int position) {
        final T item = delegated.getItemAt(position);
        holder.setItem(item);
    }

    @Override
    public final long getItemId(int position) {
        return delegated.getItemId(position);
    }

    @Override
    public final int getItemCount() {
        return delegated.getItemCount();
    }

    protected T getItemAt(int position) {
        return delegated.getItemAt(position);
    }

    @Override @CallSuper
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager != null) {
            if (layoutManager instanceof GridLayoutManager) {
                GridLayoutManager gridLayoutManager = (GridLayoutManager) layoutManager;
                itemPadding.span = gridLayoutManager.getSpanCount();
                itemPadding.horizontal = gridLayoutManager.getOrientation() == GridLayoutManager.HORIZONTAL;
            } else {
                itemPadding.span = 1;
                itemPadding.horizontal = ((LinearLayoutManager) layoutManager).getOrientation() == LinearLayoutManager.HORIZONTAL;
            }
        }

        Utils.setLayoutAnimation(recyclerView, onGetLayoutAnimation());

        recyclerView.addItemDecoration(itemPadding);
        recyclerView.scheduleLayoutAnimation();
    }

    @Override @CallSuper
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);

        recyclerView.removeItemDecoration(itemPadding);
    }

    public abstract static class ViewHolder<T> extends RecyclerView.ViewHolder {
        protected final ItemViewModel<T> viewModel;
        protected final ViewDataBinding binding;
        protected T lastItem = null;

        protected ViewHolder(ViewDataBinding binding, ItemViewModel<T> viewModel, LifecycleOwner owner) {
            super(binding.getRoot());
            this.binding = binding;
            this.viewModel = viewModel;

            binding.setLifecycleOwner(owner);
        }

        protected final Context getContext() {
            return binding.getRoot().getContext();
        }

        final void setItem(T item) {
            if (lastItem != item) {
                lastItem = item;
                viewModel.setItem(item);
                onChanged(item);
            }
        }

        @CallSuper
        protected void onChanged(T item) {
            binding.invalidateAll();
        }
    }

    public static final class ItemPadding extends RecyclerView.ItemDecoration {
        public boolean horizontal;
        /**
         * The Top Padding (over the first item)
         */
        private int top;
        /**
         * The Bottom Padding (below the last item)
         */
        private int bottom;
        /**
         * The Start Padding (before the first item)
         */
        private int left;
        /**
         * The End Padding (after the last item)
         */
        private int right;
        /**
         * The Inner Padding (between items)
         */
        private int midV;
        private int midH;
        private int span;

        ItemPadding() {}

        /**
         * Converts dp to pixels
         *
         * @param dp the DP
         * @return the pixels
         */
        private static int fromDP(int dp) {
            DisplayMetrics displayMetrics = AppApplication.get().getResources().getDisplayMetrics();
            float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, displayMetrics);
            return (int) pixels;
        }

        /**
         * Changes the Top Padding (over the first item) in dp
         *
         * @param value the padding value
         */
        public ItemPadding setTop(int value) {
            top = fromDP(value);
            return this;
        }

        /**
         * Changes the Start Padding (before the first item) in dp
         *
         * @param value the padding value
         */
        public ItemPadding setLeft(int value) {
            left = fromDP(value);
            return this;
        }

        /**
         * Changes the Bottom Padding (below the last item) in dp
         *
         * @param value the padding value
         */
        public ItemPadding setBottom(int value) {
            bottom = fromDP(value);
            return this;
        }

        /**
         * Changes the End Padding (after the last item) in dp
         *
         * @param value the padding value
         */
        public ItemPadding setRight(int value) {
            right = fromDP(value);
            return this;
        }

        /**
         * Changes the Inner Padding (between items) in dp
         *
         * @param value the padding value
         */
        public ItemPadding setMiddleHorizontal(int value) {
            midH = fromDP(value);
            return this;
        }

        /**
         * Changes the Inner Padding (between items) in dp
         *
         * @param value the padding value
         */
        public ItemPadding setMiddleVertical(int value) {
            midV = fromDP(value);
            return this;
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, RecyclerView parent, @NonNull RecyclerView.State state) {
            int pos = parent.getChildAdapterPosition(view);

            if (span == 1) {
                if (horizontal) {
                    outRect.left = (pos == 0) ? left : midH;
                    outRect.top = top;
                    outRect.right = (pos == state.getItemCount() - 1) ? right : midH;
                    outRect.bottom = bottom;
                } else {
                    outRect.left = left;
                    outRect.top = (pos == 0) ? top : midV;
                    outRect.right = right;
                    outRect.bottom = (pos == state.getItemCount() - 1) ? bottom : midV;
                }
            } else {
                if (horizontal) {
                    outRect.left = (pos < span) ? left : midH;
                    outRect.top = (pos % span == 0) ? top : midV;
                    outRect.right = (pos / span >= (state.getItemCount() - 1) / span) ? right : midH;
                    outRect.bottom = (pos % span == span - 1) ? bottom : midV;
                } else {
                    outRect.left = (pos % span == 0) ? left : midH;
                    outRect.top = (pos < span) ? top : midV;
                    outRect.right = (pos % span == span - 1) ? right : midH;
                    outRect.bottom = (pos / span >= (state.getItemCount() - 1) / span) ? bottom : midV;
                }
            }
        }
    }
}
