/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified by Pedro Parra 30/11/18 03:59 PM
 */

package io.simgulary.cms.adapters;

import java.util.List;

import androidx.recyclerview.widget.DiffUtil;

abstract class Observer<T> {
    abstract long getItemId(T item);

    abstract List<T> onFiltered(List<T> items);

    abstract ReactiveAdapter<T> adapter();

    abstract void onDataSetChanged(List<T> oldItems, List<T> newItems);

    static class BaseImpl<T> extends Observer<T> {
        private final Adapter<T, ?> adapter;

        BaseImpl(Adapter<T, ?> adapter) {
            this.adapter = adapter;
        }

        @Override
        ReactiveAdapter<T> adapter() {
            return adapter;
        }

        @Override
        long getItemId(T item) {
            return adapter.getItemId(item);
        }

        @Override
        List<T> onFiltered(List<T> items) {
            return adapter.onFiltered(items);
        }

        @Override
        void onDataSetChanged(List<T> oldItems, List<T> newItems) {
            adapter.notifyDataSetChanged();
        }
    }

    static class RecyclerImpl<T> extends Observer<T> {
        private final RecyclerAdapter<T, ?> adapter;

        RecyclerImpl(RecyclerAdapter<T, ?> adapter) {
            this.adapter = adapter;
        }

        @Override
        long getItemId(T item) {
            return adapter.getItemId(item);
        }

        @Override
        List<T> onFiltered(List<T> items) {
            return adapter.onFiltered(items);
        }

        @Override
        ReactiveAdapter<T> adapter() {
            return adapter;
        }

        @Override
        void onDataSetChanged(List<T> oldItems, List<T> newItems) {
            DiffUtil.calculateDiff(new DiffUtil.Callback() {
                @Override
                public int getOldListSize() {
                    return oldItems.size();
                }

                @Override
                public int getNewListSize() {
                    return newItems.size();
                }

                @Override
                public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
                    T oldItem = oldItems.get(oldItemPosition);
                    long oldItemId = adapter.getItemId(oldItem);

                    if (oldItemId == -1L)
                        return false;

                    T newItem = newItems.get(newItemPosition);
                    return oldItemId == adapter.getItemId(newItem);
                }

                @Override
                public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                    T oldItem = oldItems.get(oldItemPosition);
                    long itemLastUpdate = adapter.getItemLastUpdate(oldItem);

                    if (itemLastUpdate == -1L)
                        return false;

                    T newItem = newItems.get(newItemPosition);
                    return itemLastUpdate == adapter.getItemLastUpdate(newItem);
                }
            }).dispatchUpdatesTo(adapter);

            adapter.onContentUpdated(newItems);
        }
    }
}
