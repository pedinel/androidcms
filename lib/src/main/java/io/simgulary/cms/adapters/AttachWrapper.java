/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 16/04/19 02:23 PM
 */

package io.simgulary.cms.adapters;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import java.util.Collection;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import io.simgulary.cms.app.AppApplication;

/**
 * Helper class to allow keeping a single source reference for {@link Adapter} or {@link RecyclerAdapter}
 *
 * @param <X> the {@link Collection} subclass
 * @param <T> the source data type
 */
class AttachWrapper<X extends Collection<? extends T>, T> {
    /**
     * The observer that updates the items in the adapter
     */
    private final Observer<X> observer;

    /**
     * The data source
     */
    final LiveData<X> source;

    private final Handler handler = new Handler(Looper.getMainLooper());
    private X itemsToUpdate;
    private long time;

    AttachWrapper(LiveData<X> source, ReactiveAdapter<T> adapter) {
        this.source = source;

        final Runnable updateTask = () -> {
            if (itemsToUpdate != null) {
                adapter.setItems(itemsToUpdate);
                time = SystemClock.elapsedRealtime() + AppApplication.get().getAnimDuration();
                itemsToUpdate = null;
            }
        };

        this.observer = items -> {
            long t = SystemClock.elapsedRealtime();

            if (t >= time) {
                handler.removeCallbacks(updateTask);
                itemsToUpdate = items;
                updateTask.run();
            } else {
                handler.removeCallbacks(updateTask);
                itemsToUpdate = items;
                handler.postDelayed(updateTask, time - t);
            }
        };
    }

    /**
     * Attaches the observer into the adapter
     *
     * @param owner the {@link LifecycleOwner} to keep
     */
    void plug(LifecycleOwner owner) {
        source.observe(owner, observer);
    }

    /**
     * Detaches the observer from the adapter. The source will not update the adapter anymore.
     * This is useful when a new source is attached.
     */
    void unplug() {
        source.removeObserver(observer);
    }
}
