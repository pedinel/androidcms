/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 11/06/19 10:24 AM
 */

package io.simgulary.cms.adapters;

import android.annotation.SuppressLint;
import android.database.DataSetObserver;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import androidx.annotation.AnimRes;
import androidx.annotation.AnyThread;
import androidx.annotation.CallSuper;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import io.simgulary.cms.viewmodels.ItemViewModel;

@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class Adapter<T, VH extends Adapter.ViewHolder<T>> extends BaseAdapter
        implements ReactiveAdapter<T> {

    /**
     * Indicates if the adapter has more than 1 view type
     */
    private final boolean isMultiView;
    /**
     * Holds the attached Views
     */
    private final List<ViewAttach> attaches = new ArrayList<>();
    /**
     * Operation Handler delegated
     */
    private final AdapterHandler<T> delegated;

    protected Adapter(LifecycleOwner owner) {
        isMultiView = getViewTypeCount() > 1;
        delegated = new AdapterHandler<>(owner, new Observer.BaseImpl<>(this));
    }

    /**
     * Retrieves the {@link Adapter} attached to a {@link ViewGroup} (if any).
     *
     * @param view the {@link ViewGroup} that may have an attached {@link Adapter}
     * @param <T> the {@link Adapter} subclass type
     *
     * @return an attached {@link Adapter} object
     */
    @Nullable
    public static <T extends Adapter<?, ?>> T getAdapter(ViewGroup view) {
        Object tag = view.getTag();

        if (tag instanceof Adapter) {
            //noinspection unchecked
            return (T) tag;
        }

        return null;
    }

    /**
     * Attaches the adapter into a {@link ViewGroup} if not already attached.
     *
     * @param view the {@link ViewGroup}
     */
    public final void attach(ViewGroup view) {
        Utils.setLayoutAnimation(view, onGetLayoutAnimation());

        if (view instanceof ListView) {
            ((ListView) view).setAdapter(this);
        } else {
            if (findAttach(view, false) != null) {
                //already attached
                return;
            }

            ViewAttach attach = new ViewAttach(view);
            attaches.add(attach);
            attach.setAdapter(this);
        }

        view.setTag(this);
    }

    /**
     * Detaches the adapter from a {@link ViewGroup} if already attached.
     *
     * @param view the {@link ViewGroup}
     */
    public final void detach(ViewGroup view) {
        if (view instanceof ListView) {
            ((ListView) view).setAdapter(null);
        } else {
            ViewAttach attach = findAttach(view, true);
            if (attach != null) {
                attach.setAdapter(null);
            }
        }

        view.setTag(null);
    }

    public abstract VH onCreateViewHolder(ViewGroup parent, int viewType);

    @NonNull
    protected List<T> onFiltered(@NonNull List<T> items) {
        return items;
    }

    protected final LifecycleOwner getOwner() {
        return delegated.getOwner();
    }

    protected long getItemId(T item) {
        return -1;
    }

    @AnimRes
    protected int onGetLayoutAnimation() {
        return 0;
    }

    protected void onPreUpdate(ViewGroup root) {
    }

    protected void onPostUpdate(ViewGroup root) {
    }

    private ViewAttach findAttach(ViewGroup what, boolean remove) {
        for (ViewAttach e : attaches) {
            if (e.getRoot() == what) {
                if (remove) {
                    attaches.remove(e);
                }

                return e;
            }
        }

        return null;
    }

    @Override
    public final <X extends Collection<? extends T>> void setSource(LiveData<X> source) {
        delegated.setSource(source);
    }

    @Override
    public final void setItems(Collection<? extends T> newItems) {
        delegated.setItems(newItems);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Nullable
    @Override
    public final Filter<T> getFilter() {
        return delegated.filter;
    }

    @Override
    public final int getCount() {
        return delegated.getItemCount();
    }

    @Override
    public final void setFilter(@Nullable Filter<T> filter) {
        delegated.setFilter(filter);
    }

    @Override
    public final T getItem(int position) {
        return delegated.getItemAt(position);
    }

    @Override
    public final long getItemId(int position) {
        return delegated.getItemId(position);
    }

    @Override
    public final View getView(int position, View convertView, ViewGroup parent) {
        final T item = delegated.getItemAt(position);
        final int viewType = (isMultiView) ? getItemViewType(position) : 0;
        ViewHolder<T> holder = null;

        if (convertView != null) {
            //noinspection unchecked
            ViewHolder<T> recycled = (ViewHolder<T>) convertView.getTag();

            if (recycled.viewType == viewType) {
                holder = recycled;
            }
        }

        if (holder == null) {
            holder = onCreateViewHolder(parent, viewType);
            holder.itemView().setTag(holder);
        }

        if (holder.lastItem != item) {
            holder.setItem(item);
        }

        return holder.itemView();
    }

    public abstract static class ViewHolder<T> {
        protected final ItemViewModel<T> viewModel;
        protected final ViewDataBinding binding;
        protected T lastItem = null;
        private int viewType;

        protected ViewHolder(ViewDataBinding binding, ItemViewModel<T> viewModel, LifecycleOwner owner) {
            this.binding = binding;
            this.viewModel = viewModel;

            binding.setLifecycleOwner(owner);
        }

        public final T getItem() {
            return lastItem;
        }

        final void setItem(T item) {
            if (lastItem != item) {
                lastItem = item;
                viewModel.setItem(item);
                onChanged(item);
            }
        }

        @CallSuper
        protected void onChanged(T item) {
            binding.invalidateAll();
        }

        private View itemView() {
            return binding.getRoot();
        }
    }

    private static class ViewAttach {
        private final Reference<ViewGroup> root;
        @SuppressLint("UseSparseArrays")
        private final Map<Integer, Integer> viewTypeMap = new HashMap<>();
        private final DataSetObserver observer;
        @Nullable private Adapter adapter;
        private boolean animate = true;

        public ViewAttach(@NonNull ViewGroup container) {
            root = new WeakReference<>(container);
            observer = new DataSetObserver() {
                @Override
                public void onChanged() {
                    applyDataChanges();
                }

                @Override
                public void onInvalidated() {
                    applyDataChanges();
                }
            };
        }

        @Nullable @AnyThread
        private ViewGroup getRoot() {
            return root.get();
        }

        @AnyThread
        private SparseArray<LinkedList<View>> cacheCycle(Adapter adapter, List<View> children) {
            SparseArray<LinkedList<View>> out = new SparseArray<>(adapter.getViewTypeCount());

            for (View child : children) {
                Integer type = viewTypeMap.get(child.hashCode());

                if (type == null) {
                    continue;
                }

                LinkedList<View> list = out.get(type);

                if (list == null) {
                    list = new LinkedList<>();
                    out.put(type, list);
                }

                list.add(child);
            }

            viewTypeMap.clear();

            return out;
        }

        @MainThread
        private void updateViews(Adapter o) {
            int size = o.getCount();

            ViewGroup root = getRoot();

            if (root == null) {
                return;
            }

            o.onPreUpdate(root);

            final List<View> childList = new ArrayList<>(root.getChildCount());

            for (int i = root.getChildCount() - 1; i >= 0; i--) {
                childList.add(root.getChildAt(i));
            }

            final SparseArray<LinkedList<View>> cache = cacheCycle(o, childList);
            childList.clear();

            for (int i = 0; i < size; ++i) {
                int viewType = o.getItemViewType(i);

                LinkedList<View> cacheList = cache.get(viewType);
                View convertView = (cacheList == null || cacheList.isEmpty()) ? null : cacheList.removeFirst();

                convertView = o.getView(i, convertView, root);
                root.addView(convertView);

                viewTypeMap.put(convertView.hashCode(), viewType);
            }

            o.onPostUpdate(root);

            if (animate && size > 0) {
                root.scheduleLayoutAnimation();
                animate = false;
            }
        }

        @MainThread
        private void applyDataChanges() {
            ViewGroup root = getRoot();

            if (root == null) {
                return;
            }

            root.removeAllViews();

            Adapter o = adapter;

            if (o == null)
                return;

            updateViews(o);
        }

        @MainThread
        private void setAdapter(@Nullable Adapter adapter) {
            if (this.adapter == adapter)
                return;

            if (this.adapter != null) {
                this.adapter.unregisterDataSetObserver(observer);
            }

            this.adapter = adapter;

            if (adapter != null) {
                adapter.registerDataSetObserver(observer);
            }

            applyDataChanges();
        }
    }





}
