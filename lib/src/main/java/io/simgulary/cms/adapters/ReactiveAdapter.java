/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 16/04/19 02:23 PM
 */

package io.simgulary.cms.adapters;

import java.util.Collection;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;

public interface ReactiveAdapter<T> {
    /**
     * Sets the items data source. This replaces any existing data source.
     *
     * @param source the data source
     * @param <X> the {@link Collection} subclass
     */
    <X extends Collection<? extends T>> void setSource(@Nullable LiveData<X> source);

    /**
     * Updates the items list.
     *
     * @param newItems the new Item list. The {@link Adapter} will create a copy of the input
     *                 before updating the source.
     */
    void setItems(@Nullable Collection<? extends T> newItems);

    void setFilter(@Nullable Filter<T> filter);

    @Nullable
    Filter<T> getFilter();
}
