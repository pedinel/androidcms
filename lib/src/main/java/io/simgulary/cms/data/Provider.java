/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 01/08/19 12:57 PM
 */

package io.simgulary.cms.data;

import org.apache.commons.collections4.map.ReferenceMap;

import java.util.Map;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.simgulary.cms.lifecycle.Mutable;
import io.simgulary.cms.lifecycle.Observable;

import static org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength.HARD;
import static org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength.SOFT;
import static org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength.WEAK;

public final class Provider<T> extends DataProvider<T, Long> {
    public Provider(InstanceCreator<T> initializer) {
        super(initializer);
    }

    public interface InstanceCreator<T> extends DataProvider.InstanceCreator<T, Long> {
        @Override
        T onCreate(Long key);
    }
}
