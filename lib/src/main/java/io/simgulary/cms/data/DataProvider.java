package io.simgulary.cms.data;

import org.apache.commons.collections4.map.ReferenceMap;

import java.util.Map;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.simgulary.cms.lifecycle.Mutable;
import io.simgulary.cms.lifecycle.Observable;

import static org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength.HARD;
import static org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength.SOFT;
import static org.apache.commons.collections4.map.AbstractReferenceMap.ReferenceStrength.WEAK;

public class DataProvider<RelatedType, KeyType> {
    private final Map<KeyType, RelatedType> cache = new ReferenceMap<>(HARD, SOFT);
    private final Map<KeyType, Mutable<RelatedType>> observables = new ReferenceMap<>(HARD, WEAK);
    private final InstanceCreator<RelatedType, KeyType> initializer;

    public DataProvider(InstanceCreator<RelatedType, KeyType> initializer) {
        this.initializer = initializer;
    }

    public final void clear() {
        cache.clear();
        observables.clear();
    }

    private RelatedType init(@NonNull KeyType key) {
        RelatedType out = initializer.onCreate(key);
        cache.put(key, out);

        Mutable<RelatedType> w = observables.get(key);

        if (w != null) {
            w.setValue(out);
        }

        return out;
    }

    @CheckResult
    @NonNull
    public RelatedType obtain(@NonNull KeyType key) {
        RelatedType out = cache.get(key);

        if (out == null) {
            return init(key);
        }

        return out;
    }

    @CheckResult
    @Nullable
    public RelatedType retrieve(@NonNull KeyType key) {
        return cache.get(key);
    }

    @CheckResult
    public Observable<RelatedType> observable(@NonNull KeyType key) {
        return observable(key, false);
    }

    @CheckResult
    public Observable<RelatedType> observable(@NonNull KeyType key, boolean forceInitialization) {
        Mutable<RelatedType> w = observables.get(key);

        if (w == null) {
            w = Mutable.create();

            observables.put(key, w);

            RelatedType value;

            if (forceInitialization) {
                value = obtain(key);
            } else {
                value = cache.get(key);
            }

            if (value != null) {
                w.setValue(value);
            }
        }

        return w;
    }

    @NonNull
    @Override
    public String toString() {
        return cache.toString();
    }

    public interface InstanceCreator<ObjectType, KeyType> {
        ObjectType onCreate(KeyType key);
    }
}
