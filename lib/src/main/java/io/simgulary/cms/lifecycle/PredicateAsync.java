/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 15/04/19 03:05 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.WorkerThread;

public interface PredicateAsync<X> {
    @WorkerThread
    boolean test(X item);
}
