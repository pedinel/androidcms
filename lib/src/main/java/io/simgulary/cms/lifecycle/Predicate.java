/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>
 * Last Modified 25/09/18 12:25 PM
 */

package io.simgulary.cms.lifecycle;

public interface Predicate<X> {
    boolean test(X item);
}
