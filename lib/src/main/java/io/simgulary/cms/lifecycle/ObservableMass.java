/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 03:08 PM
 */

package io.simgulary.cms.lifecycle;

import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.Callable;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

public class ObservableMass<CT extends Collection<? extends T>, T>
        extends Observable<CT> {

    @MainThread
    @CheckResult
    public final <K> ObservableMap<K, T> toDictionary(@NonNull Function<T, K> func) {
        return Observables.toDictionary(this, func);
    }

    @MainThread
    @CheckResult
    public final ObservableList<T> concat(@NonNull LiveData<? extends Collection<? extends T>> source) {
        return Observables.concat(this, source);
    }

    @MainThread
    @CheckResult
    public final <O> ObservableList<O> mapEach(@NonNull FunctionAsync<T, O> func) {
        return Observables.mapEach(this, func);
    }

    @MainThread
    @CheckResult
    public final <O> ObservableList<O> mapEachWithIndex(@NonNull FunctionListAsync<T, O> func) {
        return Observables.mapEach(this, func);
    }

    @MainThread
    @CheckResult
    public final <O> ObservableList<O> switchMapEach(@NonNull Function<T, ? extends Observable<? extends O>> func) {
        return Observables.switchMapEach(this, func);
    }

    @MainThread
    @CheckResult
    public final <O> ObservableList<Holder2<T, O>> switchMapEachChained(@NonNull Function<T, ? extends Observable<O>> func) {
        return Observables.switchMapEachChained(this, func);
    }

    @MainThread
    @CheckResult
    public ObservableMass<CT, T> each(@NonNull InterceptorAsync<T> interceptor) {
        return Observables.each(this, interceptor);
    }

    @MainThread
    @CheckResult
    public final <I>
    ObservableList<Holder2<T, I>> combineEachWith(@NonNull LiveData<I> source) {
        return Observables.combineEach(this, source);
    }

    @MainThread
    @CheckResult
    public final ObservableList<T> sort(Comparator<? super T> comparator) {
        return Observables.sort(this, comparator);
    }

    @MainThread
    @CheckResult
    public final ObservableList<T> filter(@NonNull Predicate<? super T> predicate) {
        return Observables.filter(this, predicate);
    }

    @MainThread
    @CheckResult
    public final Observable<T> find(@NonNull PredicateAsync<T> predicate) {
        return Observables.find(this, predicate);
    }

    @MainThread
    @CheckResult
    public final ObservableList<T> add(T whatToAdd) {
        return Observables.add(this, whatToAdd);
    }

    @MainThread
    @CheckResult
    public final ObservableList<T> replace(T whatToAdd, @NonNull Predicate<T> predicate) {
        return Observables.replace(this, whatToAdd, predicate);
    }

    @MainThread
    @CheckResult
    public final ObservableList<T> replaceOrAdd(T whatToAdd, Predicate<T> predicate) {
        return Observables.replaceOrAdd(this, whatToAdd, predicate);
    }

    @Override
    public ObservableMass<CT, T> repeat(long millis) {
        return Observables.repeat(this, millis);
    }

    @Override
    public ObservableMass<CT, T> avoidNulls() {
        return Observables.avoidNulls(new ObservableMassImpl<>(), this);
    }

    @Override
    public ObservableMass<CT, T> reduce(@NonNull Combine2Function<CT, CT, CT> func) {
        return Observables.reduce(new ObservableMassImpl<>(), this, func);
    }

    @Override
    public ObservableMass<CT, T> reduceAsync(@NonNull Combine2Function<CT, CT, CT> func) {
        return Observables.reduceAsync(new ObservableMassImpl<>(), this, func);
    }

    @Override
    public ObservableMass<CT, T> merge(@NonNull LiveData<? extends CT> source) {
        return Observables.merge(new ObservableMassImpl<>(), this, source);
    }

    @Override
    public ObservableMass<CT, T> merge(@NonNull LiveData<? extends CT> source, @NonNull Combine2Function<CT, CT, CT> func) {
        return Observables.merge(new ObservableMassImpl<>(), this, source, func);
    }

    @Override
    public ObservableMass<CT, T> intercept(@NonNull Interceptor<CT> interceptor) {
        return Observables.chain(new ObservableMassImpl<>(), this, (value, emitter) -> {
            interceptor.onIntercept(value);
            emitter.onNext(value);
        });
    }

    @Override
    public ObservableMass<CT, T> condition(@NonNull Equator<? super CT> equator) {
        return Observables.condition(new ObservableMassImpl<>(), this, equator);
    }

    @Override
    public ObservableMass<CT, T> conditionAllowingNulls(@NonNull Equator<? super CT> equator) {
        return Observables.conditionAllowingNulls(new ObservableMassImpl<>(), this, equator);
    }

    @Override
    public <I> ObservableMass<CT, T> mapOnNull(@NonNull Observable<I> source, @NonNull Function<I, CT> func) {
        return Observables.mapOnNull(new ObservableMassImpl<>(), this, source, func);
    }

    @Override
    public <I> ObservableMass<CT, T> switchMapOnNull(@NonNull Observable<I> source, @NonNull Function<I, LiveData<CT>> func) {
        return Observables.switchMapOnNull(new ObservableMassImpl<>(), this, source, func);
    }

    @Override
    public <I extends CT> ObservableMass<CT, T> onNull(@NonNull Callable<? extends LiveData<I>> source) {
        try {
            return Observables.mapOnNull(new ObservableMassImpl<>(), this, source, input -> input);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ObservableMass<CT, T> setDefault(CT initialValue) {
        return (ObservableMass<CT, T>) super.setDefault(initialValue);
    }
}
