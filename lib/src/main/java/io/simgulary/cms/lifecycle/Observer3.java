/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:43 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

/**
 * A simple callback that can receive from {@link LiveData}.
 *
 * @param <A> The type of the parameter 1
 * @param <B> The type of the parameter 2
 * @param <C> The type of the parameter 3
 *
 * @see LiveData LiveData - for a usage description.
 */
public interface Observer3<A, B, C> extends Observer<Holder3<? extends A, ? extends B, ? extends C>> {
    /**
     * Called when the data is changed.
     * @param a  The new data 1
     * @param b  The new data 2
     * @param c  The new data 3
     */
    void onChanged(A a, B b, C c);

    @Override
    default void onChanged(Holder3<? extends A, ? extends B, ? extends C> holder) {
        onChanged(holder.value1, holder.value2, holder.value3);
    }
}
