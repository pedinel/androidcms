/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 15/04/19 11:16 AM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;

/**
 * Represents a function.
 *
 * @param <I> the type of the input to the function
 * @param <O> the type of the output of the function
 */
public interface Function<I, O> {
    /**
     * Applies this function to the given input.
     *
     * @param input the input
     * @return the function result.
     */
    @MainThread
    O apply(I input);
}
