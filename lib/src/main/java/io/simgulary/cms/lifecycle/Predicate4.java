/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:50 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;

public interface Predicate4<A, B, C, D> extends Predicate<Holder4<? extends A, ? extends B, ? extends C, ? extends D>> {
    @MainThread
    boolean test(A a, B b, C c, D d);

    @Override
    default boolean test(Holder4<? extends A, ? extends B, ? extends C, ? extends D> item) {
        return test(item.value1, item.value2, item.value3, item.value4);
    }
}
