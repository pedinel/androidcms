/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 17/05/19 11:40 AM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;

interface IMutable<T> {
    @MainThread
    default void setIfNotEquals(@Nullable T newValue) {
        @Nullable T oldValue = getValue();

        if (oldValue == newValue)
            return;

        if (oldValue != null && oldValue.equals(newValue))
            return;

        setValue(newValue);
    }

    @MainThread
    void setValue(T value);

    @WorkerThread
    void postValue(T value);

    T getValue();
}
