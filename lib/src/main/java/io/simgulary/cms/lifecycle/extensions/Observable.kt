/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 15/11/19 11:35 AM
 */

package io.simgulary.cms.lifecycle.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import io.simgulary.cms.threads.Threads

class Observable<T> : MediatorLiveData<T?>() {
    private var taskDelegate : (task: () -> Unit) -> Unit = TASK_NOW
    private val observerWrappers : MutableMap<Observer<in T?>, Observer<in T?>> = HashMap()

    internal fun registerWrapper(observer: Observer<in T?>, wrapper: Observer<in T?>) {
        observerWrappers[observer] = wrapper
    }

    internal fun unregisterWrapper(observer: Observer<in T?>) {
        observerWrappers.remove(wrapper(observer))
    }

    internal fun wrapper(observer: Observer<in T?>): Observer<in T?> {
        return observerWrappers[observer] ?: observer
    }

    internal fun <I> addTrigger(source: LiveData<I>, task: (I?, Observable<T>) -> Unit): Observable<T> {
        addSource(source) {
            taskDelegate.invoke { task.invoke(it, this) }
        }

        return this
    }

    override fun removeObserver(observer: Observer<in T?>) {
        val w = wrapper(observer)

        if (w !== observer) {
            unregisterWrapper(w)
        }

        super.removeObserver(w)
    }

    internal fun doNow() : Observable<T> {
        if (taskDelegate != TASK_NOW) {
            taskDelegate = TASK_NOW
        }
        return this
    }

    internal fun doAsync() : Observable<T> {
        if (taskDelegate != TASK_ASYNC) {
            taskDelegate = TASK_ASYNC
        }
        return this
    }

    internal fun doLater() : Observable<T> {
        if (taskDelegate != TASK_LATER) {
            taskDelegate = TASK_LATER
        }
        return this
    }

    internal fun update(value: T?) {
        if (taskDelegate == TASK_ASYNC) {
            postValue(value)
        } else {
            setValue(value)
        }
    }

    companion object {
        val TASK_NOW : (task: () -> Unit) -> Unit = { it.invoke() }
        val TASK_LATER : (task: () -> Unit) -> Unit = { Threads.executeOnMainThread(it) }
        val TASK_ASYNC : (task: () -> Unit) -> Unit = { Threads.executeAsync(it) }
    }
}
