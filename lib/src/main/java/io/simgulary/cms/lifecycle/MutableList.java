/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 07/06/19 10:51 AM
 */

package io.simgulary.cms.lifecycle;

import java.util.List;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

public class MutableList<T> extends ObservableList<T> implements IMutable<List<T>> {
    public static <T> MutableList<T> create() {
        return new MutableList<>();
    }

    @Override
    public MutableList<T> repeat(long millis) {
        return Observables.repeat(this, millis);
    }

    @Override
    public MutableList<T> setDefault(List<T> initialValue) {
        return (MutableList<T>) super.setDefault(initialValue);
    }

    @MainThread
    public void addSource(@NonNull LiveData<? extends List<T>> source) {
        //noinspection deprecation
        super.addSource(source, super::setValue);
    }
}
