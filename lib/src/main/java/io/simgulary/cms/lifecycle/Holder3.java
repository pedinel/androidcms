/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:41 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.NonNull;
import androidx.core.util.ObjectsCompat;

public class Holder3<A, B, C> {
    public final A value1;
    public final B value2;
    public final C value3;

    public Holder3(A value1,
                   B value2,
                   C value3) {

        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Holder3)) {
            return false;
        }
        Holder3<?, ?, ?> p = (Holder3<?, ?, ?>) o;
        return ObjectsCompat.equals(p.value1, value1)
                && ObjectsCompat.equals(p.value2, value2)
                && ObjectsCompat.equals(p.value3, value3);
    }

    /**
     * Compute a hash code using the hash codes of the underlying objects
     *
     * @return a hashcode of the Pair
     */
    @Override
    public int hashCode() {
        return (value1 == null ? 0 : value1.hashCode())
                ^ (value2 == null ? 0 : value2.hashCode())
                ^ (value3 == null ? 0 : value3.hashCode());
    }

    @Override
    public String toString() {
        return "Holder3{"
                + value1 + " "
                + value2 + " "
                + value3 + "}";
    }

    @NonNull
    public static <A, B, C> Holder3<A, B, C> create(A a, B b, C c) {
        return new Holder3<>(a, b, c);
    }
}
