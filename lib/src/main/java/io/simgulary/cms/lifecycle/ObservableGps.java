/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 22/05/19 03:06 PM
 */

package io.simgulary.cms.lifecycle;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.simgulary.cms.app.AppApplication;
import io.simgulary.cms.gps.GpsHandler;
import io.simgulary.cms.utils.Logger;

public class ObservableGps extends Observable<Location> {
    private static final Logger log = Logger.getLogger(ObservableGps.class);
    private static final GpsHandler<Location> gpsHandler;

    static {
        gpsHandler = new GpsHandler<Location>(AppApplication.get(), 1, 0) {
            @NonNull
            @Override
            protected Location onCreateLocation(@Nullable Location lastLocation, @NonNull Location location) {
                return location;
            }
        };

         gpsHandler.enable();
    }

    private final Mutable<Location> mDelegated = Mutable.create();
    private final GpsHandler.FullGpsListener<Location> mListener;

    public static GpsHandler<Location> getGpsHandler() {
        return gpsHandler;
    }

    public ObservableGps() {
        mListener = new GpsHandler.FullGpsListener<Location>() {
            @Override
            public void onLocationEnabled(@Nullable Location lastKnownLocation) {
                if (lastKnownLocation != null) {
                    mDelegated.postValue(lastKnownLocation);
                }
            }

            @Override
            public void onLocationDisabled() {
                log.debug("Location disabled");
            }

            @Override
            public void onLocationAvailable() {
                log.debug("Location available");
            }

            @Override
            public void onLocationUnavailable() {
                log.debug("Location unavailable");
            }

            @Override
            public void onNoAvailableProvider() {
                log.debug("No providers available");
            }

            @Override
            public void onLocationChanged(@NonNull Location location) {
                log.debug("Location changed: " + location);
                mDelegated.postValue(location);
            }
        };

        addSource(mDelegated.distinct(), this::setValue);
    }

    @Override
    protected void onActive() {
        super.onActive();

        gpsHandler.addLocationListener(mListener);
    }

    @Override
    protected void onInactive() {
        super.onInactive();

        gpsHandler.removeLocationListener(mListener);
    }
}
