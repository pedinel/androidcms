/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 21/06/19 05:22 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import io.simgulary.cms.threads.Threads;

public class Mutable<T> extends Observable<T> implements IMutable<T> {
    Mutable() {
    }

    @MainThread
    @CheckResult
    public static <T> Mutable<T> create() {
        return new Mutable<>();
    }

    @MainThread
    @CheckResult
    public static <T> Mutable<T> doOnActive(Runnable task) {
        return new Mutable<T>() {
            @Override
            protected void onActive() {
                super.onActive();

                task.run();
            }
        };
    }

    @MainThread
    @CheckResult
    public static <T> Mutable<T> create(@NonNull CreatorAsync<T> creator) {
        return new Mutable<T>() {
            @Override
            protected void onActive() {
                super.onActive();

                Threads.executeLastAsync(this, () -> {
                    if (getValue() == null) {
                        creator.onEmit(new Emitter<T>() {
                            @Override
                            public void onNext(T value) {
                                postValue(value);
                            }

                            @Override
                            public T value() {
                                return getValue();
                            }
                        });
                    }
                });
            }
        };
    }

    @MainThread
    @CheckResult
    public static <T> Mutable<T> wrap(@NonNull LiveData<T> liveData) {
        Mutable<T> out = create();

        out.addSource(liveData, out::setValue);

        return out;
    }

    @MainThread
    @CheckResult
    public static <T> Mutable<T> of(T value) {
        Mutable<T> out = create();

        doOnMainThread(() -> {
            out.setValue(value);
        });

        return out;
    }

    @Override
    public Mutable<T> repeat(long millis) {
        return Observables.repeat(this, millis);
    }

    @Override
    public Mutable<T> setDefault(T initialValue) {
        return (Mutable<T>) super.setDefault(initialValue);
    }

    @MainThread
    public void addSource(@NonNull LiveData<? extends T> source) {
        //noinspection deprecation
        super.addSource(source, this::setValue);
    }

    @MainThread
    public <S> void addSourceOnce(@NonNull LiveData<S> source, @NonNull Observer<? super S> onChanged) {
        super.addSource(source, s -> {
            super.removeSource(source);
            onChanged.onChanged(s);
        });
    }
}
