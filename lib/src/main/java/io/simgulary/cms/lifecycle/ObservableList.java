/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/06/19 01:00 PM
 */

package io.simgulary.cms.lifecycle;

import java.util.List;
import java.util.concurrent.Callable;

import androidx.annotation.CheckResult;
import androidx.annotation.IntRange;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

public class ObservableList<T>
        extends ObservableMass<List<T>, T> {
    @Override
    public ObservableList<T> each(@NonNull InterceptorAsync<T> interceptor) {
        return Observables.each(this, interceptor);
    }

    @Override
    public ObservableList<T> repeat(long millis) {
        return Observables.repeat(this, millis);
    }

    public ObservableList<T> skipNullValues() {
        return Observables.filter(this, item -> item != null);
    }

    public ObservableList<T> waitAllValues() {
        return waitAllValues(item -> item != null);
    }

    public ObservableList<T> waitAllValues(Predicate<T> condition) {
        return Observables.chainAsync(new ObservableListImpl<>(), this, (value, emitter) -> {
            if (value != null) {
                for (T e : value) {
                    if (!condition.test(e)) {
                        return;
                    }
                }

                emitter.onNext(value);
            }
        });
    }

    @Override
    public ObservableList<T> reduce(@NonNull Combine2Function<List<T>, List<T>, List<T>> func) {
        return Observables.reduce(new ObservableListImpl<>(), this, func);
    }

    @Override
    public ObservableList<T> reduceAsync(@NonNull Combine2Function<List<T>, List<T>, List<T>> func) {
        return Observables.reduceAsync(new ObservableListImpl<>(), this, func);
    }

    @Override
    public ObservableList<T> merge(@NonNull LiveData<? extends List<T>> source) {
        return Observables.merge(new ObservableListImpl<>(), this, source);
    }

    @Override
    public ObservableList<T> merge(@NonNull LiveData<? extends List<T>> source, @NonNull Combine2Function<List<T>, List<T>, List<T>> func) {
        return Observables.merge(new ObservableListImpl<>(), this, source, func);
    }

    @Override
    public ObservableList<T> intercept(@NonNull Interceptor<List<T>> interceptor) {
        return Observables.chain(new ObservableListImpl<>(), this, (value, emitter) -> {
            interceptor.onIntercept(value);
            emitter.onNext(value);
        });
    }

    @MainThread
    @CheckResult
    public Observable<T> atIndex(@IntRange(from = 0) int index) {
        return Observables.mapAsync(new ObservableImpl<>(), this, input -> {
            if (input == null || input.isEmpty() || (index >= input.size())) {
                return null;
            }

            return input.get(index);
        });
    }

    @MainThread
    @CheckResult
    public Observable<T> first() {
        return Observables.map(new ObservableImpl<>(), this, input -> {
            if (input == null || input.isEmpty()) {
                return null;
            }

            return input.get(0);
        });
    }

    @MainThread
    @CheckResult
    public Observable<T> last() {
        return Observables.map(new ObservableImpl<>(), this, input -> {
            if (input == null || input.isEmpty()) {
                return null;
            }

            return input.get(input.size() - 1);
        });
    }

    @Override
    public ObservableList<T> condition(@NonNull Equator<? super List<T>> equator) {
        return Observables.condition(new ObservableListImpl<>(), this, equator);
    }

    @Override
    public ObservableList<T> conditionAllowingNulls(@NonNull Equator<? super List<T>> equator) {
        return Observables.conditionAllowingNulls(new ObservableListImpl<>(), this, equator);
    }

    @Override
    public <I> ObservableList<T> mapOnNull(@NonNull Observable<I> source, @NonNull Function<I, List<T>> func) {
        return Observables.mapOnNull(new ObservableListImpl<>(), this, source, func);
    }

    @Override
    public <I> ObservableList<T> switchMapOnNull(@NonNull Observable<I> source, @NonNull Function<I, LiveData<List<T>>> func) {
        return Observables.switchMapOnNull(new ObservableListImpl<>(), this, source, func);
    }

    @Override
    public <I extends List<T>> ObservableList<T> onNull(@NonNull Callable<? extends LiveData<I>> source) {
        try {
            return Observables.mapOnNull(new ObservableListImpl<>(), this, source, input -> input);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ObservableList<T> setDefault(List<T> initialValue) {
        return (ObservableList<T>) super.setDefault(initialValue);
    }
}
