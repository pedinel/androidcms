/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 12/07/19 12:56 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;

public class Observable2<T1, T2> extends Observable<Holder2<T1, T2>> {
    @MainThread
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer2<? super T1, ? super T2> observer) {
        super.observe(owner, observer);
    }

    @MainThread
    public void observeForever(@NonNull Observer2<? super T1, ? super T2> observer) {
        super.observeForever(observer);
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @Nullable Predicate2<? super T1, ? super T2> condition,
                                  @NonNull Observer2<? super T1, ? super T2> observer) {

        super.observeOnce(owner, condition, observer);
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @NonNull Observer2<? super T1, ? super T2> observer) {

        super.observeOnce(owner, observer);
    }

    @MainThread
    public final void observeOnceIfNotNull(@NonNull LifecycleOwner owner,
                                           @NonNull Observer2<? super T1, ? super T2> observer) {

        super.observeOnceIfNotNull(owner, observer);
    }

    @Override
    public Observable2<T1, T2> repeat(long millis) {
        return Observables.repeat(this, millis);
    }

    @Override
    public Observable2<T1, T2> avoidNulls() {
        return Observables.avoidNulls(new Observable2<>(), this);
    }

    @Override
    public Observable2<T1, T2> setDefault(Holder2<T1, T2> initialValue) {
        return (Observable2<T1, T2>) super.setDefault(initialValue);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> map(@NonNull Function2<T1, T2, O> func) {
        return Observables.map(new ObservableImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> switchMap(@NonNull Function2<T1, T2, LiveData<O>> func) {
        return Observables.switchMap(new ObservableImpl<>(), this, func);
    }

    // TODO: 24/5/2019 continue implementations
}
