/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 20/11/20 11:43 AM.
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;

/**
 * Represents a function.
 *
 * @param <A> the type of the first input to the function
 * @param <B> the type of the second input to the function
 * @param <C> the type of the third input to the function
 * @param <D> the type of the fourth input to the function
 * @param <E> the type of the fourth input to the function
 * @param <R> the type of the output of the function
 */
public interface Function5<A, B, C, D, E, R> extends Function<Holder5<A, B, C, D, E>, R> {
    /**
     * Applies this function to the given input.
     *
     * @param a the first input
     * @param b the second input
     * @param c the third input
     * @param d the fourth input
     * @param e the fifth input
     * @return the function result.
     */
    @MainThread
    R apply(A a, B b, C c, D d, E e);

    @Override
    default R apply(Holder5<A, B, C, D, E> input) {
        return apply(input.value1, input.value2, input.value3, input.value4, input.value5);
    }
}
