/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 15/05/19 03:52 PM
 */

package io.simgulary.cms.lifecycle;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import java.util.Date;

public class ObservableDate extends Observable<Date> {
    private final long millis;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private final Runnable action = new Runnable() {
        @Override
        public void run() {
            if (!running) {
                return;
            }

            long t = SystemClock.elapsedRealtime();
            long left = millis + lastUpdateTime - t;

            if (left <= 0L) {
                updateNow(t);
                left = millis;
            }

            handler.postDelayed(this, left);
        }
    };

    private long lastUpdateTime;
    private boolean running = false;

    public ObservableDate(long intervalInMillis) {
        this.millis = intervalInMillis;
    }

    public void updateNow(long t) {
        setValue(new Date());
        lastUpdateTime = t;
    }

    @Override
    protected void onActive() {
        running = true;

        super.onActive();

        if (getValue() == null) {
            updateNow(SystemClock.elapsedRealtime());
            handler.postDelayed(action, millis);
        } else {
            handler.post(action);
        }
    }

    @Override
    protected void onInactive() {
        running = false;

        super.onInactive();

        handler.removeCallbacks(action);
    }
}
