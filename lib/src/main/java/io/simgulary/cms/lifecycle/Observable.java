/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:51 PM
 */

package io.simgulary.cms.lifecycle;

import android.os.Looper;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import io.simgulary.cms.threads.Threads;

public class Observable<T> extends MediatorLiveData<T> {
    static void doOnMainThread(Runnable runnable) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            runnable.run();
        } else {
            Threads.executeOnMainThread(runnable);
        }
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @Nullable Predicate<? super T> condition,
                                  @NonNull Observer<? super T> observer) {

        observe(owner, new Observer<T>() {
            @Override
            public void onChanged(T s) {
                if (condition == null || condition.test(s)) {
                    removeObserver(this);
                    observer.onChanged(s);
                }
            }
        });
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @NonNull Observer<? super T> observer) {

        observeOnce(owner, null, observer);
    }

    @MainThread
    public final void observeOnceIfNotNull(@NonNull LifecycleOwner owner, @NonNull Observer<? super T> observer) {
        //noinspection Convert2MethodRef
        observeOnce(owner, s -> s != null, observer);
    }

    @MainThread
    public final void observeOnce(@Nullable Predicate<? super T> condition,
                                  @NonNull Observer<? super T> observer) {

        observeForever(new Observer<T>() {
            @Override
            public void onChanged(T s) {
                if (condition == null || condition.test(s)) {
                    removeObserver(this);
                    observer.onChanged(s);
                }
            }
        });
    }

    @MainThread
    public final void observeOnce(@NonNull Observer<? super T> observer) {
        observeOnce((Predicate<T>) null, observer);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> map(@NonNull Function<T, O> func) {
        return Observables.map(new ObservableImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public Observable<T> repeat(long millis) {
        return Observables.repeat(this, millis);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> mapAsync(@NonNull FunctionAsync<T, O> func) {
        return Observables.mapAsync(new ObservableImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O, CT extends Collection<? extends O>> ObservableMass<CT, O> toMass(@NonNull Function<T, CT> func) {
        return Observables.map(new ObservableMassImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> ObservableList<O> toList(@NonNull Function<T, List<O>> func) {
        return Observables.map(new ObservableListImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> ObservableList<O> toListAsync(@NonNull FunctionAsync<T, List<O>> func) {
        return Observables.mapAsync(new ObservableListImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public final <K, V> ObservableMap<K, V> toMap(@NonNull Function<T, Map<K, V>> func) {
        return Observables.map(new ObservableMapImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> switchMap(@NonNull Function<T, LiveData<O>> func) {
        return Observables.switchMap(new ObservableImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O, CT extends Collection<? extends O>>
    ObservableMass<CT, O> switchMapToMass(@NonNull Function<T, LiveData<CT>> func) {
        return Observables.switchMap(new ObservableMassImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O>
    ObservableList<O> switchMapToList(@NonNull Function<T, LiveData<List<O>>> func) {
        return Observables.switchMap(new ObservableListImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> chain(@NonNull ChainedEmitter<O, T> emitter) {
        return Observables.chain(new ObservableImpl<>(), this, emitter);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> chainAsync(@NonNull ChainedEmitterAsync<O, T> emitter) {
        return Observables.chainAsync(new ObservableImpl<>(), this, emitter);
    }

    @MainThread
    @CheckResult
    public final <O, CT extends Collection<? extends O>>
    ObservableMass<CT, O> chainToMass(@NonNull ChainedEmitter<CT, T> emitter) {
        return Observables.chain(new ObservableMassImpl<>(), this, emitter);
    }

    @MainThread
    @CheckResult
    public final <O, CT extends Collection<? extends O>>
    ObservableMass<CT, O> chainAsyncToMass(@NonNull ChainedEmitterAsync<CT, T> emitter) {
        return Observables.chainAsync(new ObservableMassImpl<>(), this, emitter);
    }

    @MainThread
    @CheckResult
    public Observable<T> avoidNulls() {
        return Observables.avoidNulls(new ObservableImpl<>(), this);
    }

    @MainThread
    @CheckResult
    public final <I>
    Observable2<T, I> combineWith(@NonNull LiveData<I> source) {
        return Observables.combine(this, source);
    }

    @MainThread
    @CheckResult
    public final <A, B>
    Observable3<T, A, B> combineWith(@NonNull LiveData<A> a, @NonNull LiveData<B> b) {
        return Observables.combine(this, a, b);
    }

    @MainThread
    @CheckResult
    public final <A, B, C>
    Observable4<T, A, B, C> combineWith(@NonNull LiveData<A> a, @NonNull LiveData<B> b, @NonNull LiveData<C> c) {
        return Observables.combine(this, a, b, c);
    }

    @MainThread
    @CheckResult
    public final <I, O>
    Observable<O> combineWith(@NonNull LiveData<I> source,
                              @NonNull Combine2Function<T, I, O> func) {

        return Observables.combine(this, source, func);
    }

    @MainThread
    @CheckResult
    public final <T1, T2, O>
    Observable<O> combineWith(@NonNull LiveData<T1> source1,
                              @NonNull LiveData<T2> source2,
                              @NonNull Combine3Function<T, T1, T2, O> func) {

        return Observables.combine(this, source1, source2, func);
    }

    @MainThread
    @CheckResult
    public final <T1, T2, T3, O>
    Observable<O> combineWith(@NonNull LiveData<T1> source1,
                              @NonNull LiveData<T2> source2,
                              @NonNull LiveData<T3> source3,
                              @NonNull Combine4Function<T, T1, T2, T3, O> func) {

        return Observables.combine(this, source1, source2, source3, func);
    }

    @MainThread
    @CheckResult
    public final <I, O>
    Observable<O> combineWithAsync(@NonNull LiveData<I> source,
                                   @NonNull Combine2Function<T, I, O> func) {

        return Observables.combineAsync(this, source, func);
    }

    @MainThread
    @CheckResult
    public final <T1, T2, O>
    Observable<O> combineWithAsync(@NonNull LiveData<T1> source1,
                                   @NonNull LiveData<T2> source2,
                                   @NonNull Combine3Function<T, T1, T2, O> func) {

        return Observables.combineAsync(this, source1, source2, func);
    }

    @MainThread
    @CheckResult
    public final <T1, T2, T3, O>
    Observable<O> combineWithAsync(@NonNull LiveData<T1> source1,
                                   @NonNull LiveData<T2> source2,
                                   @NonNull LiveData<T3> source3,
                                   @NonNull Combine4Function<T, T1, T2, T3, O> func) {

        return Observables.combineAsync(this, source1, source2, source3, func);
    }

    @MainThread
    @CheckResult
    public Observable<T> reduce(@NonNull Combine2Function<T, T, T> func) {
        return Observables.reduce(new ObservableImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public Observable<T> reduceAsync(@NonNull Combine2Function<T, T, T> func) {
        return Observables.reduceAsync(new ObservableImpl<>(), this, func);
    }

    @MainThread
    @CheckResult
    public Observable<T> merge(@NonNull LiveData<? extends T> source) {
        return Observables.merge(new ObservableImpl<>(), this, source);
    }

    @MainThread
    @CheckResult
    public Observable<T> merge(@NonNull LiveData<? extends T> source, @NonNull Combine2Function<T, T, T> func) {
        return Observables.merge(new ObservableImpl<>(), this, source, func);
    }

    @MainThread
    @CheckResult
    public Observable<T> intercept(@NonNull Interceptor<T> interceptor) {
        return Observables.chain(new ObservableImpl<>(), this, (value, emitter) -> {
            interceptor.onIntercept(value);
            emitter.onNext(value);
        });
    }

    @MainThread
    @CheckResult
    public Observable<T> condition(@NonNull Equator<? super T> equator) {
        return Observables.condition(new ObservableImpl<>(), this, equator);
    }

    @MainThread
    @CheckResult
    public Observable<T> conditionAllowingNulls(@NonNull Equator<? super T> equator) {
        return Observables.conditionAllowingNulls(new ObservableImpl<>(), this, equator);
    }

    @MainThread
    @CheckResult
    public Observable<T> distinct() {
        return Observables.distinct(new ObservableImpl<>(), this);
    }

    @MainThread
    @CheckResult
    public <I> Observable<T> mapOnNull(@NonNull Observable<I> source, @NonNull Function<I, T> func) {
        return Observables.mapOnNull(new ObservableImpl<>(), this, source, func);
    }

    @MainThread
    @CheckResult
    public <I> Observable<T> switchMapOnNull(@NonNull Observable<I> alternative, @NonNull Function<I, LiveData<T>> func) {
        return Observables.switchMapOnNull(new ObservableImpl<>(), this, alternative, func);
    }

    @MainThread
    @CheckResult
    public <I extends T> Observable<T> onNull(@NonNull Callable<? extends LiveData<I>> source) {
        return Observables.mapOnNull(new ObservableImpl<>(), this, source, input -> input);
    }

    @MainThread
    @CheckResult
    public Observable<T> onNull(@NonNull T valueIfNull) {
        return Observables.map(new ObservableImpl<>(), this, input -> input != null ? input : valueIfNull);
    }

    @MainThread
    @CheckResult
    public Observable<T> setDefault(T initialValue) {
        addSource(Observables.of(initialValue), value -> {
            if (getValue() == null) {
                setValue(value);
            }
        });
        return this;
    }

    @MainThread
    @CheckResult
    public Observable<T> withDelay(int delayInMillis, boolean ignoreDelayIfNull) {
        if (delayInMillis <= 0) {
            return this;
        } else {
            ObservableImpl<T> out = new ObservableImpl<>();
            out.addSource(this, new Observer<T>() {
                Threads.DelayedTask task;
                final Runnable action = () -> {
                    T value = Observable.this.getValue();
                    if (out.getValue() != value) {
                        out.setValue(value);
                    }
                };

                @Override
                public void onChanged(T value) {
                    if (out.getValue() == null && ignoreDelayIfNull) {
                        out.setValue(Observable.this.getValue());
                    } else {
                        if (task != null) {
                            task.cancel();
                        }

                        task = Threads.executeDelayed(delayInMillis, action);
                    }

                }
            });

            return out;
        }
    }

    @MainThread
    @CheckResult
    public Observable<T> withDelay(int delayInMillis) {
        return withDelay(delayInMillis, false);
    }

    @MainThread
    @CheckResult
    public final <O> O getValue(Function<T, O> func) {
        return func.apply(super.getValue());
    }

    @MainThread
    @CheckResult
    public final T getValue(T valueIfNull) {
        T value = getValue();
        return value == null ? valueIfNull : value;
    }

    @MainThread
    @CheckResult
    public final <O> O getValue(O valueIfNull, Function<T, O> func) {
        T value = getValue();
        return value == null ? valueIfNull : func.apply(value);
    }

    @NonNull
    @Override
    public String toString() {
        return String.valueOf(getValue());
    }

    static class ObservableImpl<T> extends Observable<T> {}

    static class ObservableMassImpl<CT extends Collection<? extends T>, T> extends ObservableMass<CT, T> {}

    static class ObservableListImpl<T> extends ObservableList<T> {}

    static class ObservableMapImpl<K, V> extends ObservableMap<K, V> {}
}
