/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:50 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;

public class Observable4<T1, T2, T3, T4> extends Observable<Holder4<T1, T2, T3, T4>> {
    @MainThread
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer4<? super T1, ? super T2, ? super T3, ? super T4> observer) {
        super.observe(owner, observer);
    }

    @MainThread
    public void observeForever(@NonNull Observer4<? super T1, ? super T2, ? super T3, ? super T4> observer) {
        super.observeForever(observer);
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @Nullable Predicate4<? super T1, ? super T2, ? super T3, ? super T4> condition,
                                  @NonNull Observer4<? super T1, ? super T2, ? super T3, ? super T4> observer) {

        super.observeOnce(owner, condition, observer);
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @NonNull Observer4<? super T1, ? super T2, ? super T3, ? super T4> observer) {

        super.observeOnce(owner, observer);
    }

    @MainThread
    public final void observeOnceIfNotNull(@NonNull LifecycleOwner owner,
                                           @NonNull Observer4<? super T1, ? super T2, ? super T3, ? super T4> observer) {

        super.observeOnceIfNotNull(owner, observer);
    }

    @Override
    public Observable4<T1, T2, T3, T4> repeat(long millis) {
        return Observables.repeat(this, millis);
    }

    @Override
    public Observable4<T1, T2, T3, T4> avoidNulls() {
        return Observables.avoidNulls(new Observable4<>(), this);
    }

    @Override
    public Observable4<T1, T2, T3, T4> setDefault(Holder4<T1, T2, T3, T4> initialValue) {
        return (Observable4<T1, T2, T3, T4>) super.setDefault(initialValue);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> map(@NonNull Function4<T1, T2, T3, T4, O> func) {
        return Observables.map(new ObservableImpl<>(), this, func);
    }

    // TODO: 24/5/2019 continue implementations
}
