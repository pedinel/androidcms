/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:50 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;

/**
 * Represents a function.
 *
 * @param <A> the type of the first input to the function
 * @param <B> the type of the second input to the function
 * @param <C> the type of the third input to the function
 * @param <D> the type of the fourth input to the function
 * @param <R> the type of the output of the function
 */
public interface Function4<A, B, C, D, R> extends Function<Holder4<A, B, C, D>, R> {
    /**
     * Applies this function to the given input.
     *
     * @param a the first input
     * @param b the second input
     * @param c the third input
     * @return the function result.
     */
    @MainThread
    R apply(A a, B b, C c, D d);

    @Override
    default R apply(Holder4<A, B, C, D> input) {
        return apply(input.value1, input.value2, input.value3, input.value4);
    }
}
