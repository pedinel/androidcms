/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 20/11/20 11:34 AM.
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.NonNull;
import androidx.core.util.ObjectsCompat;

public class Holder5<A, B, C, D, E> {
    public final A value1;
    public final B value2;
    public final C value3;
    public final D value4;
    public final E value5;

    public Holder5(A value1,
                   B value2,
                   C value3,
                   D value4,
                   E value5) {

        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
        this.value5 = value5;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Holder5)) {
            return false;
        }
        Holder5<?, ?, ?, ?, ?> p = (Holder5<?, ?, ?, ?, ?>) o;
        return ObjectsCompat.equals(p.value1, value1)
                && ObjectsCompat.equals(p.value2, value2)
                && ObjectsCompat.equals(p.value3, value3)
                && ObjectsCompat.equals(p.value4, value4)
                && ObjectsCompat.equals(p.value5, value5);
    }

    /**
     * Compute a hash code using the hash codes of the underlying objects
     *
     * @return a hashcode of the Pair
     */
    @Override
    public int hashCode() {
        return (value1 == null ? 0 : value1.hashCode())
                ^ (value2 == null ? 0 : value2.hashCode())
                ^ (value3 == null ? 0 : value3.hashCode())
                ^ (value4 == null ? 0 : value4.hashCode())
                ^ (value5 == null ? 0 : value5.hashCode());
    }

    @Override
    public String toString() {
        return "Holder5{"
                + value1 + " "
                + value2 + " "
                + value3 + " "
                + value4 + " "
                + value5 + "}";
    }

    @NonNull
    public static <A, B, C, D, E> Holder5<A, B, C, D, E>
    create(A a, B b, C c, D d, E e) {
        return new Holder5<>(a, b, c, d, e);
    }
}
