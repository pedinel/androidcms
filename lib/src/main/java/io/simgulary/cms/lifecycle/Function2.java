/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:35 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;

/**
 * Represents a function.
 *
 * @param <A> the type of the first input to the function
 * @param <B> the type of the second input to the function
 * @param <R> the type of the output of the function
 */
public interface Function2<A, B, R> extends Function<Holder2<A, B>, R> {
    /**
     * Applies this function to the given input.
     *
     * @param a the first input
     * @param b the second input
     * @return the function result.
     */
    @MainThread
    R apply(A a, B b);

    @Override
    default R apply(Holder2<A, B> input) {
        return apply(input.value1, input.value2);
    }
}
