/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 28/05/19 04:27 PM
 */

package io.simgulary.cms.lifecycle;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Callable;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import io.simgulary.cms.threads.Threads;

public final class ObservableMaps {
    private ObservableMaps() {
        //no instance
    }

    @MainThread
    @CheckResult
    public static <K, V>
    ObservableMap<K, V> create(@NonNull CreatorAsync<Map<K, V>> emitter) {
        return new ObservableMap<K, V>() {
            @Override
            protected void onActive() {
                super.onActive();

                Threads.executeLastAsync(this, () -> {
                    emitter.onEmit(new Emitter<Map<K, V>>() {
                        @Override
                        public void onNext(Map<K, V> value) {
                            postValue(value);
                        }

                        @Override
                        public Map<K, V> value() {
                            return getValue();
                        }
                    });
                });
            }
        };
    }

    @MainThread
    @CheckResult
    public static <K, V>
    ObservableMap<K, V> from(@NonNull Callable<Map<K, V>> initializer) {
        return create(emitter -> {
            try {
                emitter.onNext(initializer.call());
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @MainThread
    @CheckResult
    public static <K, V> ObservableMap<K, V> wrap(@NonNull LiveData<? extends Map<K, V>> liveData) {
        ObservableMap<K, V> out = new Observable.ObservableMapImpl<>();

        out.addSource(liveData, out::setValue);

        return out;
    }

    @MainThread
    @CheckResult
    public static <K, V> ObservableMap<K, V> of(Map<K, V> value) {
        ObservableMap<K, V> out = new Observable.ObservableMapImpl<>();
        out.setValue(value);
        return out;
    }

    public static <K, V> ObservableList<V> toList(@NonNull LiveData<? extends Map<K, V>> in) {
        return Observables.mapAsync(new Observable.ObservableListImpl<>(), in, input -> new ArrayList<>(input.values()));
    }
}
