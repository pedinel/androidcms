/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 25/04/19 12:32 PM
 */

package io.simgulary.cms.lifecycle;

/**
 * Represents a function.
 *
 * @param <T1> the type of the first input to the function
 * @param <T2> the type of the second input to the function
 * @param <T3> the type of the third input to the function
 * @param <T4> the type of the fourth input to the function
 * @param <T5> the type of the fifth input to the function
 * @param <O> the type of the output of the function
 */
public interface Combine5Function<T1, T2, T3, T4, T5, O> {
    /**
     * Applies this function to the given input.
     *
     * @param t1 the first input
     * @param t2 the second input
     * @param t3 the third input
     * @return the function result.
     */
    O apply(T1 t1, T2 t2, T3 t3, T4 t4, T5 t5);
}
