/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/06/19 02:24 PM
 */

package io.simgulary.cms.lifecycle.extensions

import androidx.annotation.CheckResult
import androidx.lifecycle.LiveData

@CheckResult
fun <I, O> LiveData<out I>.then(function: (value: I, next: Observable<O>) -> Unit,
                                onNull: (next: Observable<O>) -> Unit) : Observable<O> {
    return Observable<O>().addTrigger(this) { value, next ->
        if (value != null)
            function(value, next)
        else
            onNull(next)
    }
}

@CheckResult
fun <I, O> LiveData<out I>.map(function: (I) -> O, onNull: (() -> O?) = {null}) : Observable<O> {
    return then(function = {value, next ->
        val result = function(value)
        next.update(result)
    }, onNull = {
        val result = onNull()
        it.update(result)
    })
}
