/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:43 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.NonNull;
import androidx.core.util.ObjectsCompat;

public class Holder2<A, B> {
    public final A value1;
    public final B value2;

    public Holder2(A value1,
                   B value2) {

        this.value1 = value1;
        this.value2 = value2;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Holder2)) {
            return false;
        }
        Holder2<?, ?> p = (Holder2<?, ?>) o;
        return ObjectsCompat.equals(p.value1, value1)
                && ObjectsCompat.equals(p.value2, value2);
    }

    /**
     * Compute a hash code using the hash codes of the underlying objects
     *
     * @return a hashcode of the Pair
     */
    @Override
    public int hashCode() {
        return (value1 == null ? 0 : value1.hashCode())
                ^ (value2 == null ? 0 : value2.hashCode());
    }

    @Override
    public String toString() {
        return "Holder3{"
                + value1 + " "
                + value2 + "}";
    }

    @NonNull
    public static <A, B> Holder2<A, B> create(A a, B b) {
        return new Holder2<>(a, b);
    }
}
