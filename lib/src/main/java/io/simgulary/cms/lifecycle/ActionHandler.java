/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 14/06/19 03:21 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

public final class ActionHandler<I, O> {
    private final MediatorLiveData<O> source;
    private final ActionObserver<I, O> observer;
    private final Observer<I> delegated;

    static <O, I, Result extends MediatorLiveData<O>>
    Result then(@NonNull LiveData<I> source,
                @NonNull Result result,
                @NonNull ActionObserver<I, O> observer) {

        result.addSource(source, new ActionHandler<>(result, observer).delegated);
        return result;
    }

    private ActionHandler(@NonNull MediatorLiveData<O> source, @NonNull ActionObserver<I, O> observer) {
        this.source = source;
        this.observer = observer;
        this.delegated = this::onChanged;
    }

    public O getValue() {
        return source.getValue();
    }

    public void setValue(O value) {
        source.setValue(value);
    }

    protected void onChanged(I i) {
        observer.onChanged(i, this);
    }
}
