/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 29/04/19 11:33 AM
 */

package io.simgulary.cms.lifecycle;

public interface Equator<X> {
    boolean test(X l, X r);
}
