/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 14/06/19 03:21 PM
 */

package io.simgulary.cms.lifecycle;

public interface ActionObserver<I, O> {
    void onChanged(I i, ActionHandler<I, O> action);

    static <I, O> ActionObserver<I, O> function(Function<I, O> function) {
        return (i, action) -> action.setValue(function.apply(i));
    }
}
