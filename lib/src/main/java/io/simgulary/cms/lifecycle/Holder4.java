/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:43 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.NonNull;
import androidx.core.util.ObjectsCompat;

public class Holder4<A, B, C, D> {
    public final A value1;
    public final B value2;
    public final C value3;
    public final D value4;

    public Holder4(A value1,
                   B value2,
                   C value3,
                   D value4) {

        this.value1 = value1;
        this.value2 = value2;
        this.value3 = value3;
        this.value4 = value4;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Holder4)) {
            return false;
        }
        Holder4<?, ?, ?, ?> p = (Holder4<?, ?, ?, ?>) o;
        return ObjectsCompat.equals(p.value1, value1)
                && ObjectsCompat.equals(p.value2, value2)
                && ObjectsCompat.equals(p.value3, value3)
                && ObjectsCompat.equals(p.value4, value4);
    }

    /**
     * Compute a hash code using the hash codes of the underlying objects
     *
     * @return a hashcode of the Pair
     */
    @Override
    public int hashCode() {
        return (value1 == null ? 0 : value1.hashCode())
                ^ (value2 == null ? 0 : value2.hashCode())
                ^ (value3 == null ? 0 : value3.hashCode())
                ^ (value4 == null ? 0 : value4.hashCode());
    }

    @Override
    public String toString() {
        return "Holder4{"
                + value1 + " "
                + value2 + " "
                + value3 + " "
                + value4 + "}";
    }

    @NonNull
    public static <A, B, C, D> Holder4<A, B, C, D>
    create(A a, B b, C c, D d) {
        return new Holder4<>(a, b, c, d);
    }
}
