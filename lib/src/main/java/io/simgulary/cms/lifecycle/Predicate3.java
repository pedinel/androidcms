/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:43 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;

public interface Predicate3<A, B, C> extends Predicate<Holder3<? extends A, ? extends B, ? extends C>> {
    @MainThread
    boolean test(A a, B b, C c);

    @Override
    default boolean test(Holder3<? extends A, ? extends B, ? extends C> item) {
        return test(item.value1, item.value2, item.value3);
    }
}
