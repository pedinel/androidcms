/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 20/11/20 11:35 AM.
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;

public interface Predicate5<A, B, C, D, E> extends Predicate<Holder5<? extends A, ? extends B, ? extends C, ? extends D, ? extends E>> {
    @MainThread
    boolean test(A a, B b, C c, D d, E e);

    @Override
    default boolean test(Holder5<? extends A, ? extends B, ? extends C, ? extends D, ? extends E> item) {
        return test(item.value1, item.value2, item.value3, item.value4, item.value5);
    }
}
