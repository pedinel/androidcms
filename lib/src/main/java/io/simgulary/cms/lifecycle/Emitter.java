/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 28/05/19 04:11 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;

public interface Emitter<T> {
    @MainThread
    void onNext(T value);

    @CheckResult
    T value();
}
