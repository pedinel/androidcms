/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 28/06/19 04:50 PM
 */

package io.simgulary.cms.lifecycle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import io.simgulary.cms.utils.Identifiable;

public class Functions {
    private Functions() {
        //no instance
    }

    @MainThread
    @CheckResult
    public static <T extends Identifiable>
    List<T> unique(Collection<? extends T> oldValues, Collection<? extends T> newValues) {
        if (oldValues == null) {
            return (newValues != null) ? new ArrayList<>(newValues) : null;
        } else if (newValues == null) {
            return new ArrayList<>(oldValues);
        }

        Map<Long, T> dictionary = new LinkedHashMap<>(oldValues.size() + newValues.size());

        for (T t : oldValues) {
            dictionary.put(t.getId(), t);
        }

        for (T t : newValues) {
            T replaced = dictionary.put(t.getId(), t);

            if (replaced != null) {
                Date d1 = replaced.getUpdatedAt();
                Date d2 = t.getUpdatedAt();

                if (d1 != null && d2 != null && d1.after(d2)) {
                    dictionary.put(replaced.getId(), replaced);
                }
            }
        }

        return new ArrayList<>(dictionary.values());
    }

    public static <T extends Identifiable> boolean isOutdated(T oldValue, T newValue) {
        if (oldValue == newValue) {
            return false;
        } else {
            return oldValue.getUpdatedAt() == null
                    || newValue.getUpdatedAt() == null
                    || oldValue.getUpdatedAt().before(newValue.getUpdatedAt());
        }
    }
}
