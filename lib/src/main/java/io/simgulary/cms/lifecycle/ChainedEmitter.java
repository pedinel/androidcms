/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 16/04/19 12:02 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;

public interface ChainedEmitter<T, V> {
    @MainThread
    void onChanged(V value, @NonNull Emitter<T> emitter);
}
