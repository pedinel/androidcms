/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 14/06/19 02:13 PM
 */

package io.simgulary.cms.lifecycle;

public final class Conditions {
    @SuppressWarnings("Convert2MethodRef")
    public static final Predicate<Object> NOT_NULL = item -> item != null;
    @SuppressWarnings("Convert2MethodRef")
    public static final Predicate<Object> NULL = item -> item == null;

    private Conditions() {
        //no instance
    }
}
