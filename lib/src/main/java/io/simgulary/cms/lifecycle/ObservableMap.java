/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 16/09/20 12:34 PM.
 */

package io.simgulary.cms.lifecycle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

public class ObservableMap<K, V>
        extends Observable<Map<K, V>> {

    @MainThread
    @CheckResult
    public final ObservableMass<Collection<V>, V> toMass() {
        return Observables.map(new ObservableMassImpl<>(), this, Map::values);
    }

    @MainThread
    @CheckResult
    public final ObservableList<V> toList() {
        return Observables.mapAsync(new ObservableListImpl<>(), this, input -> {
            return new ArrayList<>(input.values());
        });
    }

    @MainThread
    @CheckResult
    public final ObservableMap<K, V> concat(@NonNull LiveData<? extends Map<? extends K, ? extends V>> source) {
        return Observables.concat(this, source);
    }

    @MainThread
    @CheckResult
    public final ObservableMap<K, V> each(@NonNull InterceptorAsync<? super V> interceptor) {
        return Observables.each(this, interceptor);
    }

    @MainThread
    @CheckResult
    public final ObservableMap<K, V> filter(@NonNull PredicateAsync<Map.Entry<K, V>> predicate) {
        return Observables.filter(this, predicate);
    }


    @MainThread
    @CheckResult
    public final Observable<V> get(@NonNull K key) {
        return map(e -> e.get(key));
    }

    @MainThread
    @CheckResult
    public final Observable<V> find(@NonNull PredicateAsync<Map.Entry<K, V>> predicate) {
        return Observables.find(this, predicate);
    }

    @MainThread
    @CheckResult
    public final Observable<V> findKey(@NonNull PredicateAsync<K> predicate) {
        return Observables.find(this, item -> {
            return predicate.test(item.getKey());
        });
    }

    @MainThread
    @CheckResult
    public final Observable<V> findValue(@NonNull PredicateAsync<V> predicate) {
        return Observables.find(this, item -> {
            return predicate.test(item.getValue());
        });
    }

    @Override
    public ObservableMap<K, V> merge(@NonNull LiveData<? extends Map<K, V>> source) {
        return Observables.merge(new ObservableMapImpl<>(), this, source);
    }

    @Override
    public ObservableMap<K, V> merge(@NonNull LiveData<? extends Map<K, V>> source, @NonNull Combine2Function<Map<K, V>, Map<K, V>, Map<K, V>> func) {
        return Observables.merge(new ObservableMapImpl<>(), this, source, func);
    }

    @Override
    public ObservableMap<K, V> intercept(@NonNull Interceptor<Map<K, V>> interceptor) {
        return Observables.chain(new ObservableMapImpl<>(), this, (value, emitter) -> {
            interceptor.onIntercept(value);
            emitter.onNext(value);
        });
    }

    @Override
    public ObservableMap<K, V> condition(@NonNull Equator<? super Map<K, V>> equator) {
        return Observables.condition(new ObservableMapImpl<>(), this, equator);
    }

    @Override
    public ObservableMap<K, V> setDefault(Map<K, V> initialValue) {
        return (ObservableMap<K, V>) super.setDefault(initialValue);
    }

    @Override
    public ObservableMap<K, V> repeat(long millis) {
        return Observables.repeat(this, millis);
    }

    @Override
    public ObservableMap<K, V> avoidNulls() {
        return Observables.avoidNulls(new ObservableMapImpl<>(), this);
    }

    @Override
    public ObservableMap<K, V> reduce(@NonNull Combine2Function<Map<K, V>, Map<K, V>, Map<K, V>> func) {
        return Observables.reduce(new ObservableMapImpl<>(), this, func);
    }
}
