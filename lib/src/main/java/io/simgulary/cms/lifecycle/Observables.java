/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 20/11/20 11:54 AM.
 */

package io.simgulary.cms.lifecycle;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;
import io.simgulary.cms.threads.Threads;

public final class Observables {
    private static final Observable<?> NULL = Observables.of(null);
    private static final Object NO_DATA = new Object();

    private Observables() {
        //no instance
    }

    @CheckResult
    public static <T> Observable<T> getNull() {
        //noinspection unchecked
        return (Observable<T>) NULL;
    }

    @MainThread
    @CheckResult
    public static <T> Observable<T> create(@NonNull Creator<T> creator) {
        return new Observable<T>() {
            @Override
            protected void onActive() {
                super.onActive();

                creator.onEmit(new Emitter<T>() {
                    @Override
                    public void onNext(T value) {
                        postValue(value);
                    }

                    @Override
                    public T value() {
                        return getValue();
                    }
                });
            }
        };
    }

    @MainThread
    @CheckResult
    public static <T> Observable<T> from(@NonNull Callable<T> initializer) {
        return new Observable<T>() {
            final AtomicBoolean initialize = new AtomicBoolean(true);

            @Override
            protected void onActive() {
                super.onActive();

                if (initialize.get()) {
                    initialize.set(false);

                    Threads.executeAsync(() -> {
                        try {
                            postValue(initializer.call());
                        }
                        catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    });
                }
            }
        };
    }

    @MainThread
    @CheckResult
    public static <T> Observable<T> wrap(@NonNull LiveData<T> liveData) {
        Observable<T> out = new Observable.ObservableImpl<>();

        out.addSource(liveData, out::setValue);

        return out;
    }

    @MainThread
    @CheckResult
    public static <T> Observable<T> of(T value) {
        Observable<T> out = new Observable.ObservableImpl<>();
        out.setValue(value);
        return out;
    }

    @MainThread
    @CheckResult
    public static <T1, T2> Observable2<T1, T2> combine(@NonNull LiveData<T1> source1,
                                                       @NonNull LiveData<T2> source2) {
        return combine(false, false, source1, source2);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3> Observable3<T1, T2, T3> combine(@NonNull LiveData<T1> source1,
                                                               @NonNull LiveData<T2> source2,
                                                               @NonNull LiveData<T3> source3) {
        return combine(false, false, source1, source2, source3);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4> Observable4<T1, T2, T3, T4>
    combine(@NonNull LiveData<T1> source1,
            @NonNull LiveData<T2> source2,
            @NonNull LiveData<T3> source3,
            @NonNull LiveData<T4> source4) {
        return combine(false, false, source1, source2, source3, source4);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4, T5> Observable5<T1, T2, T3, T4, T5>
    combine(@NonNull LiveData<T1> source1,
            @NonNull LiveData<T2> source2,
            @NonNull LiveData<T3> source3,
            @NonNull LiveData<T4> source4,
            @NonNull LiveData<T5> source5) {
        return combine(false, false, source1, source2, source3, source4, source5);
    }

    @MainThread
    @CheckResult
    public static <OT extends Collection<? extends T1>, T1, T2>
    ObservableList<Holder2<T1, T2>> combineEach(@NonNull LiveData<OT> origin,
                                             @NonNull LiveData<T2> source) {
        return combine(new Observable.ObservableListImpl<>(),
                false,
                true,
                in -> {
                    //noinspection unchecked
                    OT list = (OT) in[0];
                    //noinspection unchecked
                    T2 obj = (T2) in[1];
                    List<Holder2<T1, T2>> out = new ArrayList<>();
                    for (T1 e : list) {
                        out.add(Holder2.create(e, obj));
                    }
                    return out;
                },
                origin,
                source
        );
    }

    @MainThread
    @CheckResult
    public static <T1, T2> Observable2<T1, T2> combineAny(@NonNull LiveData<T1> source1,
                                                               @NonNull LiveData<T2> source2) {
        return combine(true, false, source1, source2);
    }

    @SuppressWarnings("unchecked")
    @MainThread
    @CheckResult
    public static <I, O> Observable<O> combineAny(@NonNull LiveData<I>[] sources,
                                                  @NonNull Function<I[], O> func) {
        return combine(new Observable.ObservableImpl<>(), true, true,
                in -> { return func.apply((I[]) in); },
                sources);
    }

    @SuppressWarnings("unchecked")
    @MainThread
    @CheckResult
    public static <T1, T2> Observable2<T1, T2> combine(boolean allowSkipping,
                                                       boolean async,
                                                       @NonNull LiveData<T1> source1,
                                                       @NonNull LiveData<T2> source2) {
        return combine(new Observable2<>(), allowSkipping, async,
                in -> { return Holder2.create((T1) in[0], (T2) in[1]); },
                source1, source2);
    }

    @SuppressWarnings("unchecked")
    @MainThread
    @CheckResult
    public static <T1, T2, T3> Observable3<T1, T2, T3>
    combine(boolean allowSkipping,
            boolean async,
            @NonNull LiveData<T1> source1,
            @NonNull LiveData<T2> source2,
            @NonNull LiveData<T3> source3) {
        return combine(new Observable3<>(), allowSkipping, async,
                in -> { return Holder3.create((T1) in[0], (T2) in[1], (T3) in[2]); },
                source1, source2, source3);
    }

    @SuppressWarnings("unchecked")
    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4> Observable4<T1, T2, T3, T4>
    combine(boolean allowSkipping,
            boolean async,
            @NonNull LiveData<T1> source1,
            @NonNull LiveData<T2> source2,
            @NonNull LiveData<T3> source3,
            @NonNull LiveData<T4> source4) {
        return combine(new Observable4<>(), allowSkipping, async,
                in -> { return Holder4.create((T1) in[0], (T2) in[1], (T3) in[2], (T4) in[3]); },
                source1, source2, source3, source4);
    }

    @SuppressWarnings("unchecked")
    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4, T5> Observable5<T1, T2, T3, T4, T5>
    combine(boolean allowSkipping,
            boolean async,
            @NonNull LiveData<T1> source1,
            @NonNull LiveData<T2> source2,
            @NonNull LiveData<T3> source3,
            @NonNull LiveData<T4> source4,
            @NonNull LiveData<T5> source5) {
        return combine(new Observable5<>(), allowSkipping, async,
                in -> { return Holder5.create((T1) in[0], (T2) in[1], (T3) in[2], (T4) in[3], (T5) in[4]); },
                source1, source2, source3, source4, source5);
    }

    @SuppressWarnings("unchecked")
    @MainThread
    @CheckResult
    public static <T1, T2, O> Observable<O> combine(boolean allowSkipping,
                                                    boolean async,
                                                    @NonNull LiveData<T1> source1,
                                                    @NonNull LiveData<T2> source2,
                                                    @NonNull Combine2Function<T1, T2, O> func) {
        return combine(new Observable.ObservableImpl<>(), allowSkipping, async,
                in -> { return func.apply((T1) in[0], (T2) in[1]); },
                source1, source2);
    }

    @SuppressWarnings("unchecked")
    @MainThread
    @CheckResult
    public static <T1, T2, T3, O>
    Observable<O> combine(boolean allowSkipping,
                          boolean async,
                          @NonNull LiveData<T1> source1,
                          @NonNull LiveData<T2> source2,
                          @NonNull LiveData<T3> source3,
                          @NonNull Combine3Function<T1, T2, T3, O> func) {
        return combine(new Observable.ObservableImpl<>(), allowSkipping, async,
                in -> { return func.apply((T1) in[0], (T2) in[1], (T3) in[2]); },
                source1, source2, source3);
    }

    @SuppressWarnings("unchecked")
    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4, O>
    Observable<O> combine(boolean allowSkipping,
                          boolean async,
                          @NonNull LiveData<T1> source1,
                          @NonNull LiveData<T2> source2,
                          @NonNull LiveData<T3> source3,
                          @NonNull LiveData<T4> source4,
                          @NonNull Combine4Function<T1, T2, T3, T4, O> func) {
        return combine(new Observable.ObservableImpl<>(), allowSkipping, async,
                in -> { return func.apply((T1) in[0], (T2) in[1], (T3) in[2], (T4) in[3]); },
                source1, source2, source3, source4);
    }

    @SuppressWarnings("unchecked")
    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4, T5, O>
    Observable<O> combine(boolean allowSkipping,
                          boolean async,
                          @NonNull LiveData<T1> source1,
                          @NonNull LiveData<T2> source2,
                          @NonNull LiveData<T3> source3,
                          @NonNull LiveData<T4> source4,
                          @NonNull LiveData<T5> source5,
                          @NonNull Combine5Function<T1, T2, T3, T4, T5, O> func) {
        return combine(new Observable.ObservableImpl<>(), allowSkipping, async,
                in -> { return func.apply((T1) in[0], (T2) in[1], (T3) in[2], (T4) in[3], (T5) in[4]); },
                source1, source2, source3, source4, source5);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, O> Observable<O> combine(@NonNull LiveData<T1> source1,
                                                    @NonNull LiveData<T2> source2,
                                                    @NonNull Combine2Function<T1, T2, O> func) {
        return combine(false, false, source1, source2, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, O> Observable<O> combineAny(@NonNull LiveData<T1> source1,
                                                       @NonNull LiveData<T2> source2,
                                                       @NonNull Combine2Function<T1, T2, O> func) {
        return combine(true, false, source1, source2, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3, O>
    Observable<O> combine(@NonNull LiveData<T1> source1,
                          @NonNull LiveData<T2> source2,
                          @NonNull LiveData<T3> source3,
                          @NonNull Combine3Function<T1, T2, T3, O> func) {
        return combine(false, false, source1, source2, source3, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3, O>
    Observable<O> combineAny(@NonNull LiveData<T1> source1,
                             @NonNull LiveData<T2> source2,
                             @NonNull LiveData<T3> source3,
                             @NonNull Combine3Function<T1, T2, T3, O> func) {
        return combine(true, false, source1, source2, source3, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4, O>
    Observable<O> combine(@NonNull LiveData<T1> source1,
                          @NonNull LiveData<T2> source2,
                          @NonNull LiveData<T3> source3,
                          @NonNull LiveData<T4> source4,
                          @NonNull Combine4Function<T1, T2, T3, T4, O> func) {
        return combine(false, false, source1, source2, source3, source4, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4, T5, O>
    Observable<O> combine(@NonNull LiveData<T1> source1,
                          @NonNull LiveData<T2> source2,
                          @NonNull LiveData<T3> source3,
                          @NonNull LiveData<T4> source4,
                          @NonNull LiveData<T5> source5,
                          @NonNull Combine5Function<T1, T2, T3, T4, T5, O> func) {
        return combine(false, false, source1, source2, source3, source4, source5, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, O> Observable<O> combineAsync(@NonNull LiveData<T1> source1,
                                                         @NonNull LiveData<T2> source2,
                                                         @NonNull Combine2Function<T1, T2, O> func) {
        return combine(false, true, source1, source2, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, O> Observable<O> combineAnyAsync(@NonNull LiveData<T1> source1,
                                                            @NonNull LiveData<T2> source2,
                                                            @NonNull Combine2Function<T1, T2, O> func) {
        return combine(true, true, source1, source2, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3, O>
    Observable<O> combineAsync(@NonNull LiveData<T1> source1,
                               @NonNull LiveData<T2> source2,
                               @NonNull LiveData<T3> source3,
                               @NonNull Combine3Function<T1, T2, T3, O> func) {
        return combine(false, true, source1, source2, source3, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4, O>
    Observable<O> combineAsync(@NonNull LiveData<T1> source1,
                               @NonNull LiveData<T2> source2,
                               @NonNull LiveData<T3> source3,
                               @NonNull LiveData<T4> source4,
                               @NonNull Combine4Function<T1, T2, T3, T4, O> func) {
        return combine(false, true, source1, source2, source3, source4, func);
    }

    @MainThread
    @CheckResult
    public static <T1, T2, T3, T4, T5, O>
    Observable<O> combineAsync(@NonNull LiveData<T1> source1,
                               @NonNull LiveData<T2> source2,
                               @NonNull LiveData<T3> source3,
                               @NonNull LiveData<T4> source4,
                               @NonNull LiveData<T5> source5,
                               @NonNull Combine5Function<T1, T2, T3, T4, T5, O> func) {
        return combine(false, true, source1, source2, source3, source4, source5, func);
    }

    @MainThread
    @CheckResult
    public static <T, CT extends Collection<? extends T>>
    ObservableList<T> sort(LiveData<CT> source, Comparator<? super T> comparator) {
        return mapAsync(new Observable.ObservableListImpl<>(), source, input -> {
            if (input == null)
                return null;

            List<T> out = new ArrayList<>(input);
            Collections.sort(out, comparator);
            return out;
        });
    }

    @MainThread
    @CheckResult
    public static <T, CT extends Collection<? extends T>>
    ObservableList<T> filter(@NonNull LiveData<CT> source,
                             @NonNull Predicate<? super T> predicate) {

        return mapAsync(new Observable.ObservableListImpl<>(), source, input -> {
            if (input == null)
                return null;

            List<T> out = new ArrayList<>(input.size());

            for (T e : input) {
                if (predicate.test(e))
                    out.add(e);
            }

            return out;
        });
    }

    static <O, I, R extends Observable<O>>
    R mapOnNull(@NonNull R out,
                @NonNull Observable<O> source,
                @NonNull Callable<? extends LiveData<I>> alternative,
                @NonNull Function<I, O> func) {
        out.addSource(source, new Observer<O>() {
            private LiveData<I> attached;

            @Override
            public void onChanged(@Nullable O input) {
                if (input != null) {
                    //use standard source
                    if (attached != null) {
                        out.removeSource(attached);

                        //free resource
                        attached = null;
                    }

                    out.setValue(input);
                } else {
                    //use mapped source
                    if (attached == null) {
                        try {
                            attached = alternative.call();
                        }
                        catch (Exception e) {
                            throw new RuntimeException(e);
                        }

                        Observables.map(out, attached, func);
                    }
                }
            }
        });

        return out;
    }

    static <O, I, R extends Observable<O>>
    R mapOnNull(@NonNull R out,
                @NonNull Observable<O> source,
                @NonNull Observable<I> alternative,
                @NonNull Function<I, O> func) {
        return mapOnNull(out, source, () -> alternative, func);
    }

    static <O, I, R extends Observable<O>>
    R switchMapOnNull(R out, Observable<O> source, Observable<I> alternative, Function<I, LiveData<O>> func) {
        out.addSource(source, new Observer<O>() {
            boolean attached;

            @Override
            public void onChanged(@Nullable O input) {
                if (input != null) {
                    //use standard source
                    if (attached) {
                        out.removeSource(alternative);
                        attached = false;
                    }

                    out.setValue(input);
                } else {
                    //use mapped source
                    if (!attached) {
                        Observables.switchMap(out, alternative, func);
                        attached = true;
                    }
                }
            }
        });

        return out;
    }

    @MainThread
    @CheckResult
    static <K, V>
    ObservableMap<K, V> filter(@NonNull ObservableMap<K, V> source,
                               @NonNull PredicateAsync<Map.Entry<K, V>> predicate) {

        return mapAsync(new Observable.ObservableMapImpl<>(), source, input -> {
            if (input == null)
                return null;

            Map<K, V> out = new HashMap<>();

            for (Map.Entry<K, V> entry : input.entrySet()) {
                if (predicate.test(entry)) {
                    out.put(entry.getKey(), entry.getValue());
                }
            }

            return out;
        });
    }

    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    static <I, O, R extends Observable<O>>
    R chain(@NonNull R out,
            @NonNull LiveData<I> source,
            @NonNull ChainedEmitter<O, I> emitter) {

        out.addSource(source, input -> {
            emitter.onChanged(input, new Emitter<O>() {
                @Override
                public void onNext(O value) {
                    out.setValue(value);
                }

                @Override
                public O value() {
                    return out.getValue();
                }
            });
        });

        return out;
    }

    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    static <I, O, R extends Observable<O>>
    R chainAsync(@NonNull R out,
                 @NonNull LiveData<I> source,
                 @NonNull ChainedEmitterAsync<O, I> emitter) {

        out.addSource(source, input -> {
            Threads.executeAsync(out, () -> {
                emitter.onChanged(input, new EmitterAsync<O>() {
                    @Override
                    public void onNext(O value) {
                        out.postValue(value);
                    }

                    @Override
                    public O value() {
                        return out.getValue();
                    }
                });
            });
        });

        return out;
    }

    @MainThread
    @SuppressWarnings("unchecked")
    static <I, O, R extends Observable<O>>
    R map(@NonNull R out,
          @NonNull LiveData<I> source,
          @NonNull Function<I, O> func) {
        //noinspection unchecked
        out.addSource(source, x -> {
            O apply = func.apply(x);
            out.setValue(apply);
        });

        return out;
    }

    @MainThread
    static <T, R extends MediatorLiveData<T>>
    R repeat(@NonNull R source, long millis) {
        new RepeatHelper<>(millis, source);
        return source;
    }

    /**
     * Use a function to convert a data type into another in background.
     *
     * @param <I> the input type
     * @param <O> the output type
     * @param <R> the result type
     * @param out the output
     * @param source the input source
     * @param func the conversion function
     * @return a new LiveData instance
     */
    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    static <I, O, R extends Observable<O>>
    R mapAsync(@NonNull R out,
               @NonNull LiveData<I> source,
               @NonNull FunctionAsync<I, O> func) {
        //noinspection unchecked
        out.addSource(source, x -> {
            Threads.executeLastAsync(out, () -> {
                O apply = func.apply(x);
                out.postValue(apply);
            });
        });

        return out;
    }

    @MainThread
    @SuppressWarnings("unchecked")
    static <I, O, R extends Observable<O>>
    R switchMap(@NonNull R out,
                @NonNull LiveData<I> source,
                @NonNull Function<I, LiveData<O>> func) {

        out.addSource(source, new Observer<I>() {
            LiveData<O> mSource;

            @Override
            public void onChanged(@Nullable I input) {
                LiveData<O> newLiveData = func.apply(input);

                if (mSource == newLiveData) {
                    return;
                }
                if (mSource != null) {
                    out.removeSource(mSource);
                }
                mSource = newLiveData;
                if (mSource != null) {
                    out.addSource(mSource, out::setValue);
                } else {
                    out.setValue(null);
                }
            }
        });

        return out;
    }

    @MainThread
    @CheckResult
    public static <I, C extends Collection<? extends I>, O>
    ObservableList<O> switchMapEach(@NonNull Observable<C> source,
                                    @NonNull Function<I, ? extends Observable<? extends O>> func) {
        ObservableList<O> out = new Observable.ObservableListImpl<>();

        out.addSource(source, new Observer<C>() {
            private final Observer<? super O> observer = new Observer<O>() {
                @Override
                public void onChanged(O o) {
                    if (canPublish) {
                        publish();
                    }
                }
            };
            private List<LiveData<? extends O>> attachedList;
            private boolean canPublish = true;

            @Override
            public void onChanged(@Nullable C input) {
                List<LiveData<? extends O>> toDetach = attachedList;

                if (toDetach != null) {
                    for (LiveData<? extends O> observable : toDetach) {
                        out.removeSource(observable);
                    }
                }

                if (input == null) {
                    attachedList = null;
                    return;
                }

                attachedList = new ArrayList<>(input.size());
                canPublish = false;

                for (I element : input) {
                    Observable<? extends O> toObserve = func.apply(element);
                    attachedList.add(toObserve);
                    out.addSource(toObserve, observer);
                }

                canPublish = true;
                publish();
            }

            void publish() {
                final List<LiveData<? extends O>> attachedList = this.attachedList;
                Threads.executeLastAsync(out, () -> {
                    List<O> result = new ArrayList<>(attachedList.size());

                    for (LiveData<? extends O> observable : attachedList) {
                        result.add(observable.getValue());
                    }

                    out.postValue(result);
                });
            }
        });

        return out;
    }

    @MainThread
    @CheckResult
    public static <I, C extends Collection<? extends I>, O>
    ObservableList<Holder2<I, O>> switchMapEachChained(@NonNull Observable<C> source,
                                                    @NonNull Function<I, ? extends Observable<O>> func) {
        ObservableList<Holder2<I, O>> out = new Observable.ObservableListImpl<>();

        out.addSource(source, new Observer<C>() {
            private final Observer<? super O> observer = new Observer<O>() {
                @Override
                public void onChanged(O o) {
                    if (canPublish) {
                        publish();
                    }
                }
            };
            private List<Holder2<I, LiveData<O>>> attachedList;
            private boolean canPublish = true;

            @Override
            public void onChanged(@Nullable C input) {
                List<Holder2<I, LiveData<O>>> toDetach = attachedList;

                if (toDetach != null) {
                    for (Holder2<I, LiveData<O>> l : toDetach) {
                        //noinspection ConstantConditions
                        out.removeSource(l.value2);
                    }
                }

                if (input == null) {
                    attachedList = null;
                    return;
                }

                attachedList = new ArrayList<>(input.size());
                canPublish = false;

                for (I element : input) {
                    Observable<O> toObserve = func.apply(element);
                    attachedList.add(Holder2.create(element, toObserve));
                    out.addSource(toObserve, observer);
                }

                canPublish = true;
                publish();
            }

            void publish() {
                final List<Holder2<I, LiveData<O>>> attachedList = this.attachedList;
                Threads.executeLastAsync(out, () -> {
                    List<Holder2<I, O>> result = new ArrayList<>(attachedList.size());

                    for (Holder2<I, LiveData<O>> l : attachedList) {
                        //noinspection ConstantConditions
                        result.add(Holder2.create(l.value1, l.value2.getValue()));
                    }

                    out.postValue(result);
                });
            }
        });

        return out;
    }

    @MainThread
    @CheckResult
    public static <I, O, C extends Collection<? extends I>>
    ObservableList<O> mapEach(@NonNull Observable<C> source,
                              @NonNull FunctionAsync<I, O> func) {

        return mapAsync(new Observable.ObservableListImpl<>(), source, (input -> {
            if (input == null) {
                return null;
            }

            List<O> result = new ArrayList<>(input.size());
            for (I e : input) {
                result.add(func.apply(e));
            }

            return result;
        }));
    }

    @MainThread
    @CheckResult
    public static <I, O, C extends Collection<? extends I>>
    ObservableList<O> mapEach(@NonNull Observable<C> source,
                              @NonNull FunctionListAsync<I, O> func) {

        return mapAsync(new Observable.ObservableListImpl<>(), source, (input -> {
            if (input == null) {
                return null;
            }

            List<O> result = new ArrayList<>(input.size());
            int index = 0;
            for (I e : input) {
                result.add(func.apply(index++, e));
            }

            return result;
        }));
    }

    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    static <T, R extends Observable<T>>
    R avoidNulls(@NonNull R out,
                 @NonNull LiveData<? extends T> source) {

        out.addSource(source, input -> {
            if (input != null) {
                out.setValue(input);
            }
        });

        return out;
    }

    @MainThread
    @CheckResult
    public static <I, C extends Collection<? extends I>>
    ObservableMass<C, I> each(@NonNull LiveData<C> source,
                              @NonNull InterceptorAsync<? super I> interceptor) {

        return mapAsync(new Observable.ObservableMassImpl<>(), source, (input -> {
            if (input == null) {
                return null;
            }

            for (I e : input) {
                interceptor.onIntercept(e);
            }

            return input;
        }));
    }

    @MainThread
    @CheckResult
    public static <I>
    ObservableList<I> each(@NonNull ObservableList<I> source,
                           @NonNull InterceptorAsync<? super I> interceptor) {

        return mapAsync(new Observable.ObservableListImpl<>(), source, (input -> {
            if (input == null) {
                return null;
            }

            for (I e : input) {
                interceptor.onIntercept(e);
            }

            return input;
        }));
    }

    @MainThread
    @CheckResult
    public static <K, V>
    ObservableMap<K, V> each(@NonNull ObservableMap<K, V> source,
                             @NonNull InterceptorAsync<? super V> interceptor) {

        return mapAsync(new Observable.ObservableMapImpl<>(), source, (input -> {
            if (input == null) {
                return null;
            }

            for (V e : input.values()) {
                interceptor.onIntercept(e);
            }

            return input;
        }));
    }

    @MainThread
    @CheckResult
    private static <O, R extends Observable<O>>
    R combinerIfAny(@NonNull R out,
                    @NonNull Function<Object[], O> func,
                    boolean async,
                    @NonNull LiveData<?>[] sources) {

        Object[] buffer = new Object[sources.length];

        TaskDelegate publisher = (async)
                ? new TaskDelegate(() -> Threads.executeLastAsync(out, () -> out.postValue(func.apply(buffer))))
                : new TaskDelegate(() -> out.postValue(func.apply(buffer)));

        for (int i = 0; i < sources.length; i++) {
            final int index = i;
            out.addSource(sources[i], (s) -> {
                buffer[index] = s;
                publisher.launch();
            });
        }

        publisher.start();

        return out;
    }

    @MainThread
    @CheckResult
    private static <O, R extends Observable<O>>
    R combinerIfAll(@NonNull R out,
                    @NonNull Function<Object[], O> func,
                    boolean async,
                    @NonNull LiveData<?>[] sources) {

        Object[] buffer = new Object[sources.length];
        AtomicInteger count = new AtomicInteger(sources.length);

        TaskDelegate publisher = (async)
                ? new TaskDelegate(() -> Threads.executeLastAsync(out, () -> out.postValue(func.apply(buffer))))
                : new TaskDelegate(() -> out.postValue(func.apply(buffer)));

        for (int i = 0; i < sources.length; i++) {
            buffer[i] = NO_DATA;
        }

        for (int i = 0; i < sources.length; i++) {
            final int index = i;

            out.addSource(sources[i], (s) -> {
                int left = (buffer[index] == NO_DATA) ? count.decrementAndGet() : count.get();
                buffer[index] = s;

                if (left == 0) {
                    publisher.launch();
                }
            });
        }

        publisher.start();

        return out;
    }

    @MainThread
    @CheckResult
    public static <O>
    Observable<O> combine(boolean allowSkipping,
                          boolean async,
                          @NonNull Function<Object[], O> func,
                          @NonNull LiveData<?>... sources) {

        return (allowSkipping)
                ? combinerIfAny(new Observable.ObservableImpl<>(), func, async, sources)
                : combinerIfAll(new Observable.ObservableImpl<>(), func, async, sources);
    }

    @MainThread
    @CheckResult
    static <O, R extends Observable<O>>
    R combine(@NonNull R out,
              boolean allowSkipping,
              boolean async,
              @NonNull Function<Object[], O> func,
              @NonNull LiveData<?>... sources) {

        return (allowSkipping)
                ? combinerIfAny(out, func, async, sources)
                : combinerIfAll(out, func, async, sources);
    }

    @MainThread
    @CheckResult
    static <O, R extends MediatorLiveData<O>>
    R reduce(@NonNull R out,
             @NonNull LiveData<? extends O> source,
             @NonNull Combine2Function<O, O, O> func) {

        out.addSource(source, input -> {
            if (input == null)
                return;

            O oldValue = out.getValue();
            O result = (oldValue == null) ? input : func.apply(oldValue, input);

            if (oldValue != result) {
                out.setValue(result);
            }
        });

        return out;
    }

    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    static <O, R extends Observable<O>>
    R reduceAsync(@NonNull R out,
                  @NonNull LiveData<? extends O> source,
                  @NonNull Combine2Function<O, O, O> func) {

        final LiveData<O> liveData = (LiveData<O>) source;
        final AtomicReference<O> oldValue = new AtomicReference<>();

        out.addSource(liveData, input -> {
            if (input == null)
                return;

            Threads.executeAsync(out, () -> {
                O value = oldValue.get();
                O result = (value == null) ? input : func.apply(value, input);

                if (value != result) {
                    oldValue.set(result);
                    out.postValue(result);
                }
            });
        });

        return out;
    }

    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    public static <O, C1 extends Collection<? extends O>, C2 extends Collection<? extends O>>
    ObservableList<O> concat(@NonNull LiveData<C1> source1,
                             @NonNull LiveData<C2> source2) {
        final ObservableList<O> out = new Observable.ObservableListImpl<>();

        final Observer observer = o -> {
            C1 c1 = source1.getValue();
            C2 c2 = source2.getValue();

            Threads.executeLastAsync(out, () -> {
                if (c1 == null && c2 == null) {
                    out.postValue(null);
                    return;
                }

                if (c1 == null) {
                    out.postValue(new ArrayList<>(c2));
                    return;
                }

                if (c2 == null) {
                    out.postValue(new ArrayList<>(c1));
                    return;
                }

                ArrayList<O> result = new ArrayList<>(c1.size() + c2.size());
                result.addAll(c1);
                result.addAll(c2);
                out.postValue(result);
            });
        };

        //noinspection unchecked
        out.addSource(source1, observer);
        out.addSource(source2, observer);

        return out;
    }

    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    static <V, K>
    ObservableMap<K, V> concat(@NonNull ObservableMap<? extends K, ? extends V> source1,
                               @NonNull LiveData<? extends Map<? extends K, ? extends V>> source2) {

        final ObservableMap<K, V> out = new Observable.ObservableMapImpl<>();

        final Observer observer = o -> {
            Threads.executeAsync(out, () -> {
                Map c1 = source1.getValue();
                Map c2 = source2.getValue();

                if (c1 == null && c2 == null) {
                    out.postValue(null);
                    return;
                }

                if (c1 == null) {
                    out.postValue(new HashMap<>(c2));
                    return;
                }

                if (c2 == null) {
                    out.postValue(new HashMap<>(c1));
                    return;
                }

                Map result = new HashMap<>(c1.size() + c2.size());
                result.putAll(c1);
                result.putAll(c2);
                out.postValue(result);
            });
        };

        //noinspection unchecked
        out.addSource(source1, observer);
        out.addSource(source2, observer);

        return out;
    }

    @MainThread
    @CheckResult
    static <O, R extends Observable<O>>
    R merge(@NonNull R out,
            @NonNull LiveData<? extends O> source1,
            @NonNull LiveData<? extends O> source2) {

        final Observer<O> observer = out::setValue;

        out.addSource(source1, observer);
        out.addSource(source2, observer);

        return out;
    }

    @MainThread
    @CheckResult
    static <O, R extends Observable<O>>
    R merge(@NonNull R out,
            @NonNull LiveData<? extends O> source1,
            @NonNull LiveData<? extends O> source2,
            @NonNull Combine2Function<O, O, O> func) {

        final Observer<O> observer = input -> {
            O v1 = source1.getValue();
            O v2 = source2.getValue();
            O result = func.apply(v1, v2);
            out.setValue(result);
        };

        out.addSource(source1, observer);
        out.addSource(source2, observer);

        return out;
    }

    @MainThread
    @CheckResult
    static <O, R extends Observable<O>>
    R condition(@NonNull R out,
                @NonNull LiveData<O> source,
                @NonNull Equator<? super O> equator) {

        return Observables.chain(out, source, (value, emitter) -> {
            if (value == null) {
                return;
            }

            if (emitter.value() == null || equator.test(emitter.value(), value)) {
                emitter.onNext(value);
            }
        });
    }

    @MainThread
    @CheckResult
    static <O, R extends Observable<O>>
    R conditionAllowingNulls(@NonNull R out,
                             @NonNull LiveData<O> source,
                             @NonNull Equator<? super O> equator) {

        return Observables.chain(out, source, (value, emitter) -> {
            if (emitter.value() == null) {
                emitter.onNext(value);
            } else {
                if (value == null || equator.test(emitter.value(), value)) {
                    emitter.onNext(value);
                }
            }
        });
    }

    @MainThread
    @CheckResult
    static <O, R extends Observable<O>>
    R distinct(@NonNull R out,
               @NonNull LiveData<O> source) {

        return condition(out, source, (l, r) -> {
            return (l != r) && (l == null || !l.equals(r));
        });
    }

    @MainThread
    @CheckResult
    public static <O> Observable<O> merge(@NonNull LiveData<? extends O> source1,
                                          @NonNull LiveData<? extends O> source2) {

        Observable<O> out = new Observable.ObservableImpl<>();
        Observer<O> observer = out::setValue;

        out.addSource(source1, observer);
        out.addSource(source2, observer);

        return out;
    }

    @MainThread
    @CheckResult
    public static <K, V, CT extends Collection<? extends V>>
    ObservableMap<K, V> toDictionary(@NonNull LiveData<CT> source,
                                     @NonNull Function<V, K> func) {

        return mapAsync(new Observable.ObservableMapImpl<>(), source, input -> {
            if (input == null)
                return null;

            Map<K, V> out = new HashMap<>(input.size());

            for (V e : input) {
                out.put(func.apply(e), e);
            }

            return out;
        });
    }

    @MainThread
    @CheckResult
    public static <T, C extends Collection<? extends T>>
    Observable<T> find(@NonNull LiveData<C> source,
                       @NonNull PredicateAsync<T> predicate) {
        return mapAsync(new Observable.ObservableImpl<>(), source, input -> {
            if (input == null)
                return null;

            for (T e : input) {
                if (predicate.test(e))
                    return e;
            }

            return null;
        });
    }

    @MainThread
    @CheckResult
    static <K, V>
    Observable<V> find(@NonNull ObservableMap<K, V> source,
                       @NonNull PredicateAsync<Map.Entry<K, V>> predicate) {
        return mapAsync(new Observable.ObservableImpl<>(), source, input -> {
            if (input == null)
                return null;

            for (Map.Entry<K, V> e : input.entrySet()) {
                if (predicate.test(e))
                    return e.getValue();
            }

            return null;
        });
    }

    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    public static <T, C extends Collection<? extends T>>
    ObservableList<T> add(@NonNull LiveData<C> source,
                          @NonNull T whatToAdd) {

        return mapAsync(new Observable.ObservableListImpl<>(), source, input -> {
            if (input == null) {
                return null;
            }

            List<T> result = new ArrayList<>(input.size() + 1);
            result.addAll(input);
            result.add(whatToAdd);
            return result;
        });
    }

    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    public static <T, C extends Collection<? extends T>>
    ObservableList<T> replace(@NonNull LiveData<C> source,
                              @NonNull T whatToAdd,
                              @NonNull Predicate<T> predicate) {

        return mapAsync(new Observable.ObservableListImpl<>(), source, input -> {
            if (input == null) {
                return null;
            }

            List<T> result = new ArrayList<>(input);

            for (int i = 0; i < result.size(); i++) {
                if (predicate.test(result.get(i))) {
                    result.set(i, whatToAdd);
                    break;
                }
            }

            return result;
        });
    }

    @MainThread
    @CheckResult
    @SuppressWarnings("unchecked")
    public static <T, C extends Collection<? extends T>>
    ObservableList<T> replaceOrAdd(@NonNull LiveData<C> source,
                                   @NonNull T whatToAdd,
                                   @NonNull Predicate<T> predicate) {
        return mapAsync(new Observable.ObservableListImpl<>(), source, input -> {
            if (input == null) {
                return null;
            }

            List<T> result = new ArrayList<>(input.size() + 1);
            result.addAll(input);

            for (int i = 0; i < result.size(); i++) {
                if (predicate.test(result.get(i))) {
                    result.set(i, whatToAdd);
                    return result;
                }
            }

            result.add(whatToAdd);
            return result;
        });
    }

    private static class TaskDelegate {
        private final Runnable task;
        /**
         * 0: Stopped
         * 1: Launch when started
         * 2: Launch immediately
         */
        private int status;

        TaskDelegate(Runnable task) {
            this.task = task;
        }

        static TaskDelegate mainThread(Runnable task) {
            return new TaskDelegate(task);
        }

        static TaskDelegate async(Runnable task) {
            return new TaskDelegate(() -> {
                Threads.executeAsync(task);
            });
        }

        @MainThread
        void launch() {
            if (status == 2) {
                task.run();
            } else if (status == 0) {
                status = 1;
            }
        }

        @WorkerThread
        void launchDelayed() {
            Threads.executeOnMainThread(this::launch);
        }

        @MainThread
        void start() {
            if (status == 0) {
                status = 2;
            } else if (status == 1) {
                status = 2;
                task.run();
            }
        }
    }

    private static class RepeatHelper<T> extends Observable<T> {
        private final long millis;
        private final MediatorLiveData<T> source;
        private long lastUpdateTime;

        RepeatHelper(long millis, MediatorLiveData<T> source) {
            this.millis = millis;
            this.source = source;

            source.addSource(this, observer);
        }

        private final Runnable action = new Runnable() {
            @Override
            public void run() {
                if (!running) {
                    return;
                }

                long t = SystemClock.elapsedRealtime();
                long left = millis + lastUpdateTime - t;

                if (left <= 0L) {
                    source.setValue(source.getValue());
                    lastUpdateTime = t;
                    left = millis;
                }

                handler.postDelayed(this, left);
            }
        };

        private final Handler handler = new Handler(Looper.getMainLooper());
        private final Observer<T> observer = value -> {
            update();
        };
        private boolean running = false;

        @Override
        protected void onActive() {
            running = true;

            super.onActive();

            update();
        }

        @Override
        protected void onInactive() {
            running = false;

            super.onInactive();

            handler.removeCallbacks(action);
        }

        private void update() {
            handler.removeCallbacks(action);
            handler.post(action);
        }
    }
}
