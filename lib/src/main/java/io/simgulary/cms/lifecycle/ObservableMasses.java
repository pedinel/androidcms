/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 28/05/19 04:27 PM
 */

package io.simgulary.cms.lifecycle;

import java.util.Collection;
import java.util.concurrent.Callable;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import io.simgulary.cms.threads.Threads;

public final class ObservableMasses {
    private ObservableMasses() {
        //no instance
    }

    @MainThread
    @CheckResult
    public static <T, CT extends Collection<? extends T>>
    ObservableMass<CT, T> create(@NonNull CreatorAsync<CT> emitter) {
        return new ObservableMass<CT, T>() {
            @Override
            protected void onActive() {
                super.onActive();

                Threads.executeLastAsync(this, () -> {
                    emitter.onEmit(new Emitter<CT>() {
                        @Override
                        public void onNext(CT value) {
                            postValue(value);
                        }

                        @Override
                        public CT value() {
                            return getValue();
                        }
                    });
                });
            }
        };
    }

    @MainThread
    @CheckResult
    public static <T, CT extends Collection<? extends T>>
    ObservableMass<CT, T> from(@NonNull Callable<CT> initializer) {
        return create(emitter -> {
            try {
                emitter.onNext(initializer.call());
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @MainThread
    @CheckResult
    public static <T, CT extends Collection<? extends T>> ObservableMass<CT, T> wrap(@NonNull LiveData<CT> liveData) {
        ObservableMass<CT, T> out = new Observable.ObservableMassImpl<>();

        out.addSource(liveData, out::setValue);

        return out;
    }

    @MainThread
    @CheckResult
    public static <T, CT extends Collection<? extends T>> ObservableMass<CT, T> of(CT value) {
        ObservableMass<CT, T> out = new Observable.ObservableMassImpl<>();
        out.setValue(value);
        return out;
    }
}
