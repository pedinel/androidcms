/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 14/06/19 03:49 PM
 */

package io.simgulary.cms.lifecycle;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.Observer;

abstract class ObservableBase<T, Self extends ObservableBase<T, Self>> extends MediatorLiveData<T> {
    abstract Self createSameInstance();

    protected final Self self() {
        //noinspection unchecked
        return (Self) this;
    }

    static <T> Observable<T> createInstance() {
        return new Observable<>();
    }

    static <CT extends Collection<? extends T>, T> ObservableMass<CT, T> createMassInstance() {
        return new ObservableMass<>();
    }

    static <T> ObservableList<T> createListInstance() {
        return new ObservableList<>();
    }

    static <K, V> ObservableMap<K, V> createMapInstance() {
        return new ObservableMap<>();
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @Nullable Predicate<? super T> condition,
                                  @NonNull Observer<? super T> observer) {

        observe(owner, new Observer<T>() {
            @Override
            public void onChanged(T s) {
                if (condition == null || condition.test(s)) {
                    removeObserver(this);
                    observer.onChanged(s);
                }
            }
        });
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @NonNull Observer<? super T> observer) {

        observeOnce(owner, null, observer);
    }

    @MainThread
    public final void observeOnceIfNotNull(@NonNull LifecycleOwner owner, @NonNull Observer<? super T> observer) {
        observeOnce(owner, Conditions.NOT_NULL, observer);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> map(@NonNull Function<T, O> func) {
        return Observables.map(createInstance(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> mapAsync(@NonNull FunctionAsync<T, O> func) {
        return Observables.mapAsync(createInstance(), this, func);
    }

    @MainThread
    @CheckResult
    public final Self repeat(long millis) {
        return Observables.repeat(self(), millis);
    }

    @MainThread
    @CheckResult
    public final <O, CT extends Collection<? extends O>> ObservableMass<CT, O> toMass(@NonNull Function<T, CT> func) {
        return Observables.map(createMassInstance(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> ObservableList<O> toList(@NonNull Function<T, List<O>> func) {
        return Observables.map(createListInstance(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> ObservableList<O> toListAsync(@NonNull FunctionAsync<T, List<O>> func) {
        return Observables.mapAsync(createListInstance(), this, func);
    }

    @MainThread
    @CheckResult
    public final <K, V> ObservableMap<K, V> toMap(@NonNull Function<T, Map<K, V>> func) {
        return Observables.map(createMapInstance(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> switchMap(@NonNull Function<T, LiveData<O>> func) {
        return Observables.switchMap(createInstance(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O, CT extends Collection<? extends O>>
    ObservableMass<CT, O> switchMapToMass(@NonNull Function<T, LiveData<CT>> func) {
        return Observables.switchMap(createMassInstance(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O>
    ObservableList<O> switchMapToList(@NonNull Function<T, LiveData<List<O>>> func) {
        return Observables.switchMap(createListInstance(), this, func);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> chain(@NonNull ChainedEmitter<O, T> emitter) {
        return Observables.chain(createInstance(), this, emitter);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> chainAsync(@NonNull ChainedEmitterAsync<O, T> emitter) {
        return Observables.chainAsync(createInstance(), this, emitter);
    }

    @MainThread
    @CheckResult
    public final <O, CT extends Collection<? extends O>>
    ObservableMass<CT, O> chainToMass(@NonNull ChainedEmitter<CT, T> emitter) {
        return Observables.chain(createMassInstance(), this, emitter);
    }

    @MainThread
    @CheckResult
    public final <O, CT extends Collection<? extends O>>
    ObservableMass<CT, O> chainAsyncToMass(@NonNull ChainedEmitterAsync<CT, T> emitter) {
        return Observables.chainAsync(createMassInstance(), this, emitter);
    }

    @MainThread
    @CheckResult
    public Observable<T> avoidNulls() {
        return Observables.avoidNulls(createInstance(), this);
    }

    @MainThread
    @CheckResult
    public final <I>
    Observable2<T, I> combineWith(@NonNull LiveData<I> source) {
        return Observables.combine(this, source);
    }

    @MainThread
    @CheckResult
    public final <I, O>
    Observable<O> combineWith(@NonNull LiveData<I> source,
                                  @NonNull Combine2Function<T, I, O> func) {

        return Observables.combine(this, source, func);
    }

    @MainThread
    @CheckResult
    public final <T1, T2, O>
    Observable<O> combineWith(@NonNull LiveData<T1> source1,
                                  @NonNull LiveData<T2> source2,
                                  @NonNull Combine3Function<T, T1, T2, O> func) {

        return Observables.combine(this, source1, source2, func);
    }

    @MainThread
    @CheckResult
    public final <T1, T2, T3, O>
    Observable<O> combineWith(@NonNull LiveData<T1> source1,
                                  @NonNull LiveData<T2> source2,
                                  @NonNull LiveData<T3> source3,
                                  @NonNull Combine4Function<T, T1, T2, T3, O> func) {

        return Observables.combine(this, source1, source2, source3, func);
    }

    @MainThread
    @CheckResult
    public final <I, O>
    Observable<O> combineWithAsync(@NonNull LiveData<I> source,
                                       @NonNull Combine2Function<T, I, O> func) {

        return Observables.combineAsync(this, source, func);
    }

    @MainThread
    @CheckResult
    public final <T1, T2, O>
    Observable<O> combineWithAsync(@NonNull LiveData<T1> source1,
                                       @NonNull LiveData<T2> source2,
                                       @NonNull Combine3Function<T, T1, T2, O> func) {

        return Observables.combineAsync(this, source1, source2, func);
    }

    @MainThread
    @CheckResult
    public final <T1, T2, T3, O>
    Observable<O> combineWithAsync(@NonNull LiveData<T1> source1,
                                       @NonNull LiveData<T2> source2,
                                       @NonNull LiveData<T3> source3,
                                       @NonNull Combine4Function<T, T1, T2, T3, O> func) {

        return Observables.combineAsync(this, source1, source2, source3, func);
    }

    @MainThread
    @CheckResult
    public Self reduce(@NonNull Combine2Function<T, T, T> func) {
        return Observables.reduce(createSameInstance(), this, func);
    }

//    @MainThread
//    @CheckResult
//    public ObservableBase<T> reduceAsync(@NonNull Combine2Function<T, T, T> func) {
//        return Observables.reduceAsync(createInstance(), this, func);
//    }
//
//    @MainThread
//    @CheckResult
//    public ObservableBase<T> merge(@NonNull LiveData<? extends T> source) {
//        return Observables.merge(createInstance(), this, source);
//    }
//
//    @MainThread
//    @CheckResult
//    public ObservableBase<T> merge(@NonNull LiveData<? extends T> source, @NonNull Combine2Function<T, T, T> func) {
//        return Observables.merge(createInstance(), this, source, func);
//    }
//
//    @MainThread
//    @CheckResult
//    public ObservableBase<T> intercept(@NonNull Interceptor<T> interceptor) {
//        return Observables.chain(createInstance(), this, (value, emitter) -> {
//            interceptor.onIntercept(value);
//            emitter.onNext(value);
//        });
//    }
//
//    @MainThread
//    @CheckResult
//    public ObservableBase<T> condition(@NonNull Equator<? super T> equator) {
//        return Observables.condition(createInstance(), this, equator);
//    }
//
//    @MainThread
//    @CheckResult
//    public ObservableBase<T> conditionAllowingNulls(@NonNull Equator<? super T> equator) {
//        return Observables.conditionAllowingNulls(createInstance(), this, equator);
//    }
//
//    @MainThread
//    @CheckResult
//    public ObservableBase<T> distinct() {
//        return Observables.distinct(createInstance(), this);
//    }
//
//    @MainThread
//    @CheckResult
//    public <I> ObservableBase<T> mapOnNull(@NonNull ObservableBase<I> source, @NonNull Function<I, T> func) {
//        return Observables.mapOnNull(createInstance(), this, source, func);
//    }
//
//    @MainThread
//    @CheckResult
//    public <I> ObservableBase<T> switchMapOnNull(@NonNull ObservableBase<I> alternative, @NonNull Function<I, LiveData<T>> func) {
//        return Observables.switchMapOnNull(createInstance(), this, alternative, func);
//    }
//
//    @MainThread
//    @CheckResult
//    public <I extends T> ObservableBase<T> onNull(@NonNull Callable<? extends LiveData<I>> source) {
//        return Observables.mapOnNull(createInstance(), this, source, input -> input);
//    }
//
//    @MainThread
//    @CheckResult
//    public ObservableBase<T> onNull(@NonNull T valueIfNull) {
//        return Observables.map(createInstance(), this, input -> input != null ? input : valueIfNull);
//    }
//
//    @MainThread
//    @CheckResult
//    public ObservableBase<T> setDefault(T initialValue) {
//        addSource(Observables.of(initialValue), value -> {
//            if (getValue() == null) {
//                setValue(value);
//            }
//        });
//        return this;
//    }
//
//    @MainThread
//    @CheckResult
//    public ObservableBase<T> withDelay(int delayInMillis) {
//        if (delayInMillis <= 0) {
//            return this;
//        } else {
//            ObservableImpl<T> out = createInstance();
//            out.addSource(this, value -> {
//                Threads.executeAfterDelay(delayInMillis, () -> {
//                    if (out.getValue() != getValue()) {
//                        out.setValue(getValue());
//                    }
//                });
//            });
//            return out;
//        }
//    }
//
//    @MainThread
//    @CheckResult
//    public final <O> O getValue(Function<T, O> func) {
//        return func.apply(super.getValue());
//    }
//
//    @MainThread
//    @CheckResult
//    public final T getValue(T valueIfNull) {
//        T value = getValue();
//        return value == null ? valueIfNull : value;
//    }
//
//    @MainThread
//    @CheckResult
//    public final <O> O getValue(O valueIfNull, Function<T, O> func) {
//        T value = getValue();
//        return value == null ? valueIfNull : func.apply(value);
//    }
//
//    @NonNull
//    @Override
//    public String toString() {
//        return String.valueOf(getValue());
//    }
//
//    static class ObservableImpl<T> extends ObservableBase<T> {}
//
//    static class ObservableMassImpl<CT extends Collection<? extends T>, T> extends ObservableMass<CT, T> {}
//
//    static class ObservableListImpl<T> extends ObservableList<T> {}
//
//    static class ObservableMapImpl<K, V> extends ObservableMap<K, V> {}
}
