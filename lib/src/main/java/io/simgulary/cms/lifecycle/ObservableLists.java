/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 28/05/19 04:27 PM
 */

package io.simgulary.cms.lifecycle;

import java.util.List;
import java.util.concurrent.Callable;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import io.simgulary.cms.threads.Threads;

public final class ObservableLists {
    private ObservableLists() {
        //no instance
    }

    @MainThread
    @CheckResult
    public static <T>
    ObservableList<T> create(@NonNull CreatorAsync<List<T>> emitter) {
        return new ObservableList<T>() {
            @Override
            protected void onActive() {
                super.onActive();

                Threads.executeLastAsync(this, () -> {
                    emitter.onEmit(new Emitter<List<T>>() {
                        @Override
                        public void onNext(List<T> value) {
                            postValue(value);
                        }

                        @Override
                        public List<T> value() {
                            return getValue();
                        }
                    });
                });
            }
        };
    }

    @MainThread
    @CheckResult
    public static <T>
    ObservableList<T> from(@NonNull Callable<List<T>> initializer) {
        return create(emitter -> {
            try {
                emitter.onNext(initializer.call());
            }
            catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    @MainThread
    @CheckResult
    public static <T> ObservableList<T> wrap(@NonNull LiveData<? extends List<T>> liveData) {
        ObservableList<T> out = new Observable.ObservableListImpl<>();

        out.addSource(liveData, out::setValue);

        return out;
    }

    @MainThread
    @CheckResult
    public static <T> ObservableList<T> of(List<T> value) {
        ObservableList<T> out = new Observable.ObservableListImpl<>();
        out.setValue(value);
        return out;
    }
}
