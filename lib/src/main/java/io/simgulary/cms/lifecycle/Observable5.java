/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 20/11/20 11:43 AM.
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;

public class Observable5<T1, T2, T3, T4, T5> extends Observable<Holder5<T1, T2, T3, T4, T5>> {
    @MainThread
    public void observe(@NonNull LifecycleOwner owner, @NonNull Observer5<? super T1, ? super T2, ? super T3, ? super T4, ? super T5> observer) {
        super.observe(owner, observer);
    }

    @MainThread
    public void observeForever(@NonNull Observer5<? super T1, ? super T2, ? super T3, ? super T4, ? super T5> observer) {
        super.observeForever(observer);
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @Nullable Predicate5<? super T1, ? super T2, ? super T3, ? super T4, ? super T5> condition,
                                  @NonNull Observer5<? super T1, ? super T2, ? super T3, ? super T4, ? super T5> observer) {

        super.observeOnce(owner, condition, observer);
    }

    @MainThread
    public final void observeOnce(@NonNull LifecycleOwner owner,
                                  @NonNull Observer5<? super T1, ? super T2, ? super T3, ? super T4, ? super T5> observer) {

        super.observeOnce(owner, observer);
    }

    @MainThread
    public final void observeOnceIfNotNull(@NonNull LifecycleOwner owner,
                                           @NonNull Observer5<? super T1, ? super T2, ? super T3, ? super T4, ? super T5> observer) {

        super.observeOnceIfNotNull(owner, observer);
    }

    @Override
    public Observable5<T1, T2, T3, T4, T5> repeat(long millis) {
        return Observables.repeat(this, millis);
    }

    @Override
    public Observable5<T1, T2, T3, T4, T5> avoidNulls() {
        return Observables.avoidNulls(new Observable5<>(), this);
    }

    @Override
    public Observable5<T1, T2, T3, T4, T5> setDefault(Holder5<T1, T2, T3, T4, T5> initialValue) {
        return (Observable5<T1, T2, T3, T4, T5>) super.setDefault(initialValue);
    }

    @MainThread
    @CheckResult
    public final <O> Observable<O> map(@NonNull Function5<T1, T2, T3, T4, T5, O> func) {
        return Observables.map(new ObservableImpl<>(), this, func);
    }

    // TODO: 20/11/2020 continue implementations
}
