/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 17/04/19 12:27 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.WorkerThread;

public interface InterceptorAsync<T> {
    @WorkerThread
    void onIntercept(T value);
}
