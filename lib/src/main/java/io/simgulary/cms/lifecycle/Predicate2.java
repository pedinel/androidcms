/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 08/07/19 02:33 PM
 */

package io.simgulary.cms.lifecycle;

import androidx.annotation.MainThread;

public interface Predicate2<A, B> extends Predicate<Holder2<? extends A, ? extends B>> {
    @MainThread
    boolean test(A a, B b);

    @Override
    default boolean test(Holder2<? extends A, ? extends B> item) {
        return test(item.value1, item.value2);
    }
}
