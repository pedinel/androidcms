/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */

package io.simgulary.cms.anim;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.View;

import java.util.WeakHashMap;

import androidx.annotation.CheckResult;
import androidx.annotation.IntRange;

@SuppressWarnings("unused")
public final class AnimationHandler {
    private static final Executor<?> sDefaultExecutor = new DefaultExecutor(new Handler(Looper.getMainLooper()));

    private AnimationHandler() {
        //no instance
    }

    @CheckResult
    public static <T> Animation<T> animate(@IntRange(from = 0) long durationMs, T ref) {
        return new Animation<>(ref, durationMs);
    }

    public static final class Animation<T> {
        private final T ref;
        private final long durationInMilliseconds;

        Animation(T ref, long durationInMilliseconds) {
            this.ref = ref;
            this.durationInMilliseconds = durationInMilliseconds;
        }

        @CheckResult
        public <X> CompoundAnimation<T, X> using(X from, X to) {
            return new CompoundAnimation<>(ref, durationInMilliseconds, from, to);
        }

        public void start(AnimationListener<T> l) {
            startDelayed(0L, l);
        }

        public void startDelayed(long delayMilliseconds, AnimationListener<T> l) {
            launch(ref, durationInMilliseconds, delayMilliseconds, l);
        }
    }

    public static final class CompoundAnimation<T, X>  {
        private final T ref;
        private final long durationInMilliseconds;
        private final X from;
        private final X to;

        CompoundAnimation(T ref, long durationInMilliseconds, X from, X to) {
            this.ref = ref;
            this.durationInMilliseconds = durationInMilliseconds;
            this.from = from;
            this.to = to;
        }

        public void start(BezierListener<X> l1, CompoundAnimationListener<T, X> l2) {
            startDelayed(0L, l1, l2);
        }

        public void startDelayed(long delayMilliseconds, BezierListener<X> l1, CompoundAnimationListener<T, X> l2) {
            launch(ref, durationInMilliseconds, delayMilliseconds, (ref, f) -> l2.onAnimate(ref, l1.onUpdate(from, to, f)));
        }
    }

    private static <T> void launch(T ref, long durationInMilliseconds, long delayMilliseconds, AnimationListener<T> l) {
        final Executor<?> handler;

        if (ref instanceof View) {
            handler = new ViewExecutor((View) ref);
        } else {
            handler = sDefaultExecutor;
        }

        final Runnable action = new AnimationAction<>(ref, durationInMilliseconds, l, handler);

        if (delayMilliseconds <= 0) {
            handler.post(action);
        } else {
            handler.postDelayed(action, delayMilliseconds);
        }
    }

    public interface AnimationListener<T> {
        void onAnimate(T ref, double f);
    }

    public interface BezierListener<T> {
        T onUpdate(T l, T r, double f);
    }

    public interface CompoundAnimationListener<T, X> {
        void onAnimate(T ref, X current);
    }

    static final class AnimationAction<T> implements Runnable {
        private static int REF_COUNT;
        private static final WeakHashMap<Object, Integer> countMap = new WeakHashMap<>();
        private final T ref;
        private final double durationInMilliseconds;
        private final AnimationListener<T> l;
        private final Executor<?> handler;
        private final int refCount;
        long startTime;

        AnimationAction(T ref, long durationInMilliseconds, AnimationListener<T> l, Executor<?> handler) {
            this.ref = ref;
            this.durationInMilliseconds = (double) Math.max(durationInMilliseconds, 1);
            this.l = l;
            this.handler = handler;
            refCount = ++REF_COUNT;
            countMap.put(ref, refCount);
        }

        @Override
        public void run() {
            if (startTime == 0L) {
                startTime = SystemClock.elapsedRealtime();
                l.onAnimate(ref, 0.0);
                handler.postDelayed(this, 10);
                return;
            }

            Integer refCount = countMap.get(ref);
            if (refCount != null && refCount != this.refCount)
                return;

            final long elapsedTime = SystemClock.elapsedRealtime() - startTime;
            final double f = Math.max(elapsedTime / durationInMilliseconds, 0.0);

            if (f < 1.0) {
                l.onAnimate(ref, f);
                handler.postDelayed(this, 10);
            } else {
                l.onAnimate(ref, 1.0);
            }
        }
    }

    static abstract class Executor<T> {
        final T delegated;

        Executor(T delegated) {
            this.delegated = delegated;
        }

        abstract void post(Runnable action);

        abstract void postDelayed(Runnable action, long delay);

        abstract void removeCallbacks(Runnable action);
    }

    static final class ViewExecutor extends Executor<View> {
        ViewExecutor(View delegated) {
            super(delegated);
        }

        @Override
        void post(Runnable action) {
            delegated.post(action);
        }

        @Override
        void postDelayed(Runnable action, long delay) {
            delegated.postDelayed(action, delay);
        }

        @Override
        void removeCallbacks(Runnable action) {
            delegated.removeCallbacks(action);
        }
    }

    static final class DefaultExecutor extends Executor<Handler> {
        DefaultExecutor(Handler delegated) {
            super(delegated);
        }

        @Override
        void post(Runnable action) {
            delegated.post(action);
        }

        @Override
        void postDelayed(Runnable action, long delay) {
            delegated.postDelayed(action, delay);
        }

        @Override
        void removeCallbacks(Runnable action) {
            delegated.removeCallbacks(action);
        }
    }
}
