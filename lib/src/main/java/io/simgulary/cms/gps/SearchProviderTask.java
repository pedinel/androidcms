/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 22/05/19 10:50 AM
 */

package io.simgulary.cms.gps;

import androidx.annotation.WorkerThread;
import io.simgulary.cms.threads.Threads;


final class SearchProviderTask implements Runnable {
	private final GpsHandler gpsHandler;
	private volatile boolean searchPending = false;
	volatile int intervalMs;

	SearchProviderTask(GpsHandler gpsHandler, int intervalMs) {
		this.gpsHandler = gpsHandler;
		this.intervalMs = intervalMs;
	}

	void execute() {
		synchronized (this) {
			searchPending = true;
			Threads.executeLastAsync(this, this);
		}
	}

	@Override
	@WorkerThread
	public void run() {
		boolean run;
		synchronized (this) {
			run = searchPending;
		}

		while (run) {
			synchronized (this) {
				searchPending = false;
			}

			if (!gpsHandler.locationEnabled) {
				Threads.executeOnMainThread(gpsHandler.enableProvider);
			}

			synchronized (this) {
				try {
					this.wait(intervalMs);
				}
				catch (InterruptedException ignored) {
				}

				run = searchPending;
			}
		}
	}
}
