/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 22/05/19 10:50 AM
 */

package io.simgulary.cms.gps;


import androidx.annotation.MainThread;
import io.simgulary.cms.threads.Threads;

@SuppressWarnings({"WeakerAccess", "unused"})
final class EnableProviderRunnable implements Runnable {
	private final GpsHandler gpsHandler;

	EnableProviderRunnable(GpsHandler gpsHandler) {
		this.gpsHandler = gpsHandler;
	}

	@Override
	@MainThread
	public void run() {
		gpsHandler.enableBestAvailableProvider();
	}

	public void execute() {
		Threads.executeOnMainThread(this);
	}
}
