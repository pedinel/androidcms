/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 22/05/19 11:48 AM
 */

package io.simgulary.cms.gps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import java.util.LinkedHashSet;
import java.util.List;

import androidx.annotation.AnyThread;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import io.simgulary.cms.utils.PermissionHandler;

/**
 * Handles GPS to always receive updates
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class GpsHandler<T> implements LocationListener {
	private static final long DEFAULT_TIME = 1000;
	private static final float DEFAULT_DISTANCE = 1f;
	private static final int DEFAULT_SEARCH_INTERVAL = 10000;
	private static final String[] PERMISSIONS = {
			Manifest.permission.ACCESS_FINE_LOCATION,
			Manifest.permission.ACCESS_COARSE_LOCATION
	};
	private static volatile GpsHandler<Location> instance;
	final EnableProviderRunnable enableProvider;
	private final LinkedHashSet<GpsListener<T>> locationListeners = new LinkedHashSet<>();
	private final LinkedHashSet<FullGpsListener<T>> stateListeners = new LinkedHashSet<>();
	private final LocationManager locationManager;
	private final SearchProviderTask searchProviderTask;
	private final Object enableLock = new Object();
	boolean locationEnabled = false;
	boolean statusEnabled = true;
	private long minTime;
	private float minDistance;
	@Nullable
	private T lastLocation = null;
	private boolean locationAvailable = false;
	private boolean notifyUnavailability = true;
	private boolean providerEnabled = false;

	@RequiresPermission(allOf = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
	public GpsHandler(Context context) {
		//noinspection MissingPermission
		this(context, DEFAULT_TIME, DEFAULT_DISTANCE);
	}

	/**
	 * Constructor
	 *
	 * @param context                Context
	 * @param minTimeMs              minimal waiting time to receive next GPS update
	 * @param minDistanceMeters      minimal displacement in meters to receive next update
	 */
	@RequiresPermission(allOf = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
	public GpsHandler(Context context, long minTimeMs, float minDistanceMeters) {
		minTime = minTimeMs;
		minDistance = minDistanceMeters;

		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if (context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
					context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				Log.e("GpsHandler", "GPSHandler: No Permissions");
			}
		}

		enableProvider = new EnableProviderRunnable(this);

		searchProviderTask = new SearchProviderTask(this, DEFAULT_SEARCH_INTERVAL);
	}

	public void enable() {
		if (!statusEnabled) {
			statusEnabled = true;

			if (!locationListeners.isEmpty()) {
				enableLocation();
			}
		}

	}

	public void disable() {
		if (statusEnabled) {
			statusEnabled = false;

			if (!locationListeners.isEmpty()) {
				disableLocation();
			}
		}
	}

	/**
	 * Enables GPS updates
	 */
	@AnyThread
	private synchronized void enableLocation() {
		if (statusEnabled && !locationEnabled) {
			enableProvider.execute();
		}
	}

	/**
	 * Adds a GPS listener
	 *
	 * @param l can be GPSListener or FullGPSListener
	 */
	public void addLocationListener(GpsListener<T> l) {
		synchronized (locationListeners) {
			locationListeners.add(l);

			if (l instanceof GpsHandler.FullGpsListener) {
				synchronized (stateListeners) {
					stateListeners.add((FullGpsListener<T>) l);
				}
			}
		}

		if (locationEnabled && lastLocation != null) {
			l.onLocationChanged(lastLocation);
		}

		if (locationListeners.size() == 1) {
			enableLocation();
		}
	}

	/**
	 * Removes a GPS listener
	 *
	 * @param l can be GPSListener or FullGPSListener
	 */
	public void removeLocationListener(GpsListener l) {
		synchronized (locationListeners) {
			locationListeners.remove(l);

			if (l instanceof GpsHandler.FullGpsListener) {
				synchronized (stateListeners) {
					stateListeners.remove(l);
				}
			}
		}

		if (locationListeners.isEmpty()) {
			disableLocation();
		}
	}

	/**
	 * Disables GPS Updates and Searches for the best available provider.
	 * When a provider is found, Enables GPS Updates for the fount provider
	 */
	@AnyThread
	public synchronized void resetLocation() {
		enableProvider.execute();
	}

	/**
	 * Removes and disables GPS updates
	 */
	@SuppressLint("MissingPermission")
	@MainThread
	public synchronized void disableLocation() {
		boolean disabled = false;

		synchronized (enableLock) {
			if (locationEnabled) {
				providerEnabled = false;
				locationEnabled = false;
				disabled = true;
				this.locationManager.removeUpdates(this);
			}
		}

		if (disabled) {
			onLocationDisabled();
		}
	}

	/**
	 * +
	 * Changes minimal update time
	 *
	 * @param minTime the requested minimal update time (in milliseconds)
	 * @see #resetLocation() to apply changes
	 */
	public void setMinTime(long minTime) {
		if (minTime < 1) {
			minTime = 1;
		}

		this.minTime = minTime;
	}

	/**
	 * +
	 * Changes minimal update distance
	 *
	 * @param minDistance the requested minimal update distance (in meters)
	 * @see #resetLocation() to apply changes
	 */
	public void setMinDistance(float minDistance) {
		if (minDistance < 0) {
			minDistance = 0;
		}

		this.minDistance = minDistance;
	}

	/**
	 * Retrieves the delay (in milliseconds) to search for providers again when no providers
	 * were found
	 *
	 * @return the interval to wait (in milliseconds)
	 */
	public int getSearchInterval() {
		return searchProviderTask.intervalMs;
	}

	/**
	 * Changes the interval (in milliseconds) to search for providers again when no providers
	 * were found
	 */
	public void setSearchInterval(int intervalMs) {
		if (intervalMs < 10) {
			intervalMs = 10;
		}
		searchProviderTask.intervalMs = intervalMs;
	}

	/**
	 * Checks if a provider is available
	 *
	 * @return {@code true} if a provider is available
	 */
	public boolean isProviderEnabled() {
		return providerEnabled;
	}

	/**
	 * Checks if App has permissions to retrieve GPS updates
	 *
	 * @param activity The caller Activity
	 * @param l        The callback listener
	 */
	public void checkOrAskForPermissions(Activity activity, PermissionHandler.PermissionListener l) {
		PermissionHandler.getInstance().checkPermission(activity, l, PERMISSIONS);
	}

	/**
	 * Requests GPS updates from the best provider
	 *
	 * @return true if a Provider is registered successful
	 */
	@SuppressLint("MissingPermission")
	@MainThread
	synchronized boolean enableBestAvailableProvider() {
		disableLocation();

		synchronized (enableLock) {
			String bestProvider = getBestAvailableProvider();

			if (bestProvider != null) {
				locationManager.requestLocationUpdates(bestProvider, minTime, minDistance, this);
				lastLocation = getLastKnownLocation();

				locationEnabled = true;
				changeAvailability(true);

				if (!notifyUnavailability) {
					notifyUnavailability = true;
				}
				providerEnabled = true;

				onLocationEnabled();
			}
			else {
				changeAvailability(false);
				searchProviderTask.execute();

				if (notifyUnavailability) {
					notifyUnavailability = false;
					onNoAvailableProvider();
				}
				providerEnabled = false;
			}

			return locationEnabled;
		}
	}

	/**
	 * Creates the current Location object for the updated location
	 *
	 * @param lastLocation The previous created location. null if first location update
	 * @param location The current location update from GPS
	 *
	 */
	@NonNull
	protected abstract T onCreateLocation(@Nullable T lastLocation, @NonNull Location location);

	/**
	 * This is called when a provider status is updated
	 * If the status has changed its availability, all listeners are notified
	 *
	 * @param locationAvailable Current provider availability
	 */
	@MainThread
	private void changeAvailability(boolean locationAvailable) {
		if (this.locationAvailable != locationAvailable) {
			this.locationAvailable = locationAvailable;

			if (locationAvailable) {
				onLocationAvailable();
			}
			else {
				onLocationUnavailable();
			}
		}
	}

	/**
	 * Notifies all listeners that GPS provider is available
	 */
	@MainThread
	private void onLocationAvailable() {
		synchronized (stateListeners) {
			for (FullGpsListener l : stateListeners) {
				l.onLocationAvailable();
			}
		}
	}

	/**
	 * Notifies all listeners that GPS provider is unavailable
	 */
	@MainThread
	private void onLocationUnavailable() {
		synchronized (stateListeners) {
			for (FullGpsListener l : stateListeners) {
				l.onLocationUnavailable();
			}
		}
	}

	/**
	 * Retrieves the name of the best available Provider
	 *
	 * @return the best Provider's name
	 */
	@AnyThread
	private String getBestAvailableProvider() {
		List<String> providers = locationManager.getProviders(true);

		if (providers.contains(LocationManager.GPS_PROVIDER)) {
			return LocationManager.GPS_PROVIDER;
		}

		if (providers.contains(LocationManager.NETWORK_PROVIDER)) {
			return LocationManager.NETWORK_PROVIDER;
		}

		return null;
	}

	/**
	 * Retrieves the last known location available
	 *
	 * @return {@link Location} if there is a Last Location; else {@code null}
	 */
	@AnyThread
	@Nullable
	private T getLastKnownLocation() {
		Location location = null;
		float bestAccuracy = Float.MAX_VALUE;

		for (String provider : locationManager.getProviders(true)) {
			@SuppressLint("MissingPermission")
			Location l = locationManager.getLastKnownLocation(provider);
			if (l == null) {
				continue;
			}

			float accuracy = l.getAccuracy();
			if (accuracy <= 0) {
				accuracy = Float.MAX_VALUE;
			}

			if (location == null || accuracy <= bestAccuracy) {
				location = l;
				bestAccuracy = accuracy;
			}
		}
		if (location != null) {
			return onCreateLocation(lastLocation, location);
		}

		return lastLocation;
	}

	/**
	 * Notifies all listeners that GPS Updates are now enabled
	 */
	@MainThread
	private void onLocationEnabled() {
		synchronized (stateListeners) {
			for (FullGpsListener<T> l : stateListeners) {
				l.onLocationEnabled(lastLocation);
			}
		}
	}

	/**
	 * Notifies all listeners that GPS Provider is unavailable
	 */
	@MainThread
	private void onNoAvailableProvider() {
		synchronized (stateListeners) {
			for (FullGpsListener l : stateListeners) {
				l.onNoAvailableProvider();
			}
		}
	}

	/**
	 * Notifies all listeners that GPS Updates are now disabled
	 */
	@MainThread
	private void onLocationDisabled() {
		synchronized (stateListeners) {
			for (FullGpsListener l : stateListeners) {
				l.onLocationDisabled();
			}
		}
	}

	/**
	 * This is triggered when a GPS Update is received.
	 *
	 * @param location received from GPS
	 */
	@Override
	@MainThread
	public void onLocationChanged(Location location) {
		synchronized (this) {
			lastLocation = onCreateLocation(lastLocation, location);

			synchronized (locationListeners) {
				for (GpsListener<T> l : locationListeners) {
					l.onLocationChanged(lastLocation);
				}
			}
		}
	}

	/**
	 * This is triggered when a GPS Status change is received and calls changeAvailability
	 *
	 * @param provider The provider that changed
	 * @param status   The current status
	 * @param extras   The Bundle data
	 */
	@Override
	@MainThread
	public void onStatusChanged(String provider, int status, Bundle extras) {
		final boolean b = (status == LocationProvider.AVAILABLE && extras.getInt("satellites") > 0);

		changeAvailability(b);
	}

	/**
	 * This is triggered when the provider is available
	 *
	 * @param provider The provider's name
	 */
	@Override
	@AnyThread
	public void onProviderEnabled(String provider) {
		resetLocation();
	}

	/**
	 * This is triggered when the provider is unavailable
	 *
	 * @param provider The provider's name
	 */
	@Override
	@AnyThread
	public void onProviderDisabled(String provider) {
		resetLocation();
	}

	/**
	 * Listener for GPS Updates
	 */
	public interface GpsListener<T> {
		/**
		 * This is called when a GPS update is retrieved
		 *
		 * @param location The {@link T} object that contains the retrieved location
		 */
		void onLocationChanged(@NonNull T location);
	}

	/**
	 * Listener for GPS Updates and GPS Status changes
	 */
	public interface FullGpsListener<T> extends GpsListener<T> {
		/**
		 * This is called when GPS updates are requested
		 * @param lastKnownLocation the last known location
		 */
		void onLocationEnabled(@Nullable T lastKnownLocation);

		/**
		 * This is called when GPS updates becomes disabled
		 */
		void onLocationDisabled();

		/**
		 * This is called when GPS updates are available
		 */
		void onLocationAvailable();

		/**
		 * This is called when GPS updates are unavailable. This can be called when
		 * no GPS signal is available
		 */
		void onLocationUnavailable();

		/**
		 * This is called when no GPS Providers are available.
		 */
		void onNoAvailableProvider();
	}

	public static class Legacy extends GpsHandler<Location> {
		@SuppressLint("MissingPermission")
		public Legacy(Context context) {
			super(context);
		}

		@SuppressLint("MissingPermission")
		public Legacy(Context context, long minTimeMs, float minDistanceMeters) {
			super(context, minTimeMs, minDistanceMeters);
		}

		@NonNull
		@Override
		protected Location onCreateLocation(@Nullable Location lastLocation, @NonNull Location location) {
			return location;
		}
	}
}
