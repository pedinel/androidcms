/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified by Pedro Parra 19/03/19 12:16 PM
 */

package io.simgulary.cms.threads;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import io.simgulary.cms.app.AppApplication;

/**
 * Threads Handler
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public final class Threads {
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private static final SerialDelegatedHandler delegatedHandler = new SerialDelegatedHandler();
    private static long lastExecution;
    private static final Executor THREAD_POOL_EXECUTOR = AsyncTask.THREAD_POOL_EXECUTOR;

    /**
     * Executes a Task in the Main Thread
     *
     * @param runnable the Task
     */
    public static void executeOnMainThread(Runnable runnable) {
        handler.post(runnable);
    }

    /**
     * Executes a Task in a Worker Thread
     *
     * @param runnable the Task
     */
    public static void executeAsync(Runnable runnable) {
        THREAD_POOL_EXECUTOR.execute(runnable);
    }

    /**
     * Executes a Task in a Worker Thread. The task that shares the same delegated
     * are executed one before the another
     *
     * @param delegated the delegated
     * @param runnable the Task
     */
    public static void executeAsync(Object delegated, Runnable runnable) {
        delegatedHandler.execute(delegated, runnable);
    }

    /**
     * Executes a Task in a Worker Thread. The task that shares the same delegated
     * are executed one before the another
     *
     * @param delegated the delegated
     * @param runnable the Task
     */
    public static void executeLastAsync(Object delegated, Runnable runnable) {
        delegatedHandler.executeLast(delegated, runnable);
    }

    /**
     * Executes a Task calling an {@link AsyncTask} using the {@link #THREAD_POOL_EXECUTOR}
     *
     * @param runnable the Task to execute in {@link AsyncTask#doInBackground(Object[])}
     * @return the created {@link AsyncTask} already executed
     */
    public static AsyncTask executeTask(Runnable runnable) {
        return new Task().executeOnExecutor(THREAD_POOL_EXECUTOR, runnable);
    }

    /**
     * Executes a Task in Main Thread after the given delay (in milliseconds)
     *
     * @param milliseconds the delay
     * @param task the Task
     *
     * @return an {@link DelayedTask} object that can be cancelled or updated
     */
    public static DelayedTask executeDelayed(long milliseconds, Runnable task) {
        DelayedTask out = new DelayedTask(task, false);
        out.updateDelay(milliseconds);
        return out;
    }

    /**
     * Executes a Task in Worker Thread after the given delay (in milliseconds)
     *
     * @param milliseconds the delay
     * @param task the Task
     *
     * @return an {@link DelayedTask} object that can be cancelled or updated
     */
    public static DelayedTask executeAsyncDelayed(long milliseconds, Runnable task) {
        DelayedTask out = new DelayedTask(task, true);
        out.updateDelay(milliseconds);
        return out;
    }

    /**
     * Executes a Task in Main Thread after the given delay (in milliseconds)
     * If the task is already on queue, this methods does nothing (the first call has priority)
     *
     * @param milliseconds the delay
     * @param runnable the Task
     */
    public static void executeAfterDelay(long milliseconds, Runnable runnable) {
        synchronized (handler) {
            long t = SystemClock.elapsedRealtime();
            long et = lastExecution + milliseconds;
            if (et > t) {
                milliseconds += et - t;
            }
            lastExecution = t + milliseconds;
        }

        handler.postDelayed(runnable, milliseconds);
    }

    @Deprecated
    public static void executeAfterAnim(Runnable runnable) {
        executeAfterDelay(AppApplication.get().getAnimDuration() / 2, runnable);
    }

    @CheckResult
    public static <T> ChainHandler<T> runAsync(WorkerThreadTask<T> task) {
        return new ChainHandler<>(task);
    }

    @CheckResult
    public static <T> ChainHandler<T> runOnMainThread(MainThreadTask<T> task) {
        return new ChainHandler<>(task);
    }

    private static final class Task extends AsyncTask<Runnable, Void, Void> {
        @Override
        protected Void doInBackground(Runnable[] runnables) {
            if (runnables != null) {
                for (Runnable runnable : runnables) {
                    runnable.run();
                }
            }

            return null;
        }
    }

    /**
     * A Task Delayer Manager that executes a Task when the specified delay elapses.
     * The task is executed only once
     */
    @SuppressWarnings("WeakerAccess")
    public static final class DelayedTask {
        private final Runnable taskWrapper;
        private volatile Runnable task;
        private volatile boolean executed = false;
        private volatile boolean cancelled = false;

        DelayedTask(Runnable runnable, boolean async) {
            task = runnable;

            taskWrapper = new Runnable() {
                @Override
                public void run() {
                    synchronized (taskWrapper) {
                        if (executed || cancelled)
                            return;

                        executed = true;
                    }

                    Runnable delayedTask = task;

                    if (delayedTask != null) {
                        task = null;

                        if (async) {
                            THREAD_POOL_EXECUTOR.execute(delayedTask);
                        } else {
                            delayedTask.run();
                        }
                    }
                }
            };
        }

        /**
         * Replaces the delay with the given one. This starts over the delay if not executed yet.
         *
         * @param milliseconds the new delay
         */
        public final void updateDelay(long milliseconds) {
            synchronized (taskWrapper) {
                if (executed || cancelled)
                    return;

                handler.removeCallbacks(taskWrapper);
                handler.postDelayed(taskWrapper, Math.max(0, milliseconds));
            }
        }

        /**
         * Cancels (aborts) the task. This task will never be executed after this.
         */
        public final void cancel() {
            synchronized (taskWrapper) {
                if (executed || cancelled)
                    return;

                cancelled = true;
                handler.removeCallbacks(taskWrapper);
            }
        }

        /**
         * Checks if the task was executed. If cancelled, this will return false.
         *
         * @return {@code true} only if the task was successfully executed.
         */
        public boolean isDone() {
            return executed;
        }

        /**
         * Checks if the task was cancelled. If executed, this will return false.
         *
         * @return {@code true} only if the task was cancelled before execution.
         */
        public boolean isCancelled() {
            return cancelled;
        }

        /**
         * Checks if the task has not been executed or cancelled.
         *
         * @return {@code true} only if the task has not been executed and has not been cancelled.
         */
        public boolean isPending() {
            return !executed && !cancelled;
        }
    }

    static final class SerialDelegatedHandler {
        private final Map<Object, ChainedExecutor> map = new ConcurrentHashMap<>();

        void execute(Object delegated, Runnable task) {
            synchronized (map) {
                ChainedExecutor executor = map.get(delegated);

                if (executor != null) {
                    executor.chain(task);
                    return;
                }

                executor = new ChainedExecutor(delegated, task);
                map.put(delegated, executor);
                executor.execute();
            }
        }

        void executeLast(Object delegated, Runnable task) {
            synchronized (map) {
                ChainedExecutor executor = map.get(delegated);

                if (executor != null) {
                    executor.setNext(task);
                    return;
                }

                executor = new ChainedExecutor(delegated, task);
                map.put(delegated, executor);
                executor.execute();
            }
        }

        final class ChainedExecutor implements Runnable {
            private final Object delegated;
            private final Runnable task;
            private ChainedExecutor next;

            ChainedExecutor(Object delegated, Runnable task) {
                this.delegated = delegated;
                this.task = task;
            }

            void execute() {
                handler.post(() -> THREAD_POOL_EXECUTOR.execute(this));
            }

            void chain(Runnable task) {
                if (next == null) {
                    next = new ChainedExecutor(delegated, task);
                } else {
                    next.chain(task);
                }
            }

            void setNext(Runnable task) {
                next = new ChainedExecutor(delegated, task);
            }

            @Override
            public void run() {
                task.run();

                synchronized (map) {
                    if (next == null) {
                        map.remove(delegated);
                        return;
                    }

                    map.put(delegated, next);
                    next.execute();
                }
            }
        }
    }

    public static final class ChainHandler<T> {
        @NonNull private final ChainedExecutor<T> executor;

        ChainHandler(@NonNull MainThreadTask<T> task) {
            executor = next -> {
                T value = task.start();
                next.doNext(value);
            };
        }

        ChainHandler(@NonNull WorkerThreadTask<T> task) {
            executor = next -> {
                executeAsync(() -> {
                    final T value = task.start();

                    executeOnMainThread(() -> {
                        next.doNext(value);
                    });
                });
            };
        }

        <I> ChainHandler(@NonNull final ChainedExecutor<I> prev, @NonNull ChainedMainThreadTask<I, T> task) {
            executor = next -> {
                prev.execute(input -> {
                    T value = task.apply(input);
                    next.doNext(value);
                });
            };
        }

        <I> ChainHandler(@NonNull final ChainedExecutor<I> prev, @NonNull ChainedWorkerThreadTask<I, T> task) {
            executor = next -> {
                prev.execute(input -> {
                    executeAsync(() -> {
                        final T value = task.apply(input);

                        executeOnMainThread(() -> {
                            next.doNext(value);
                        });
                    });
                });
            };
        }

        @CheckResult
        public <O> ChainHandler<O> runOnMainThread(@NonNull ChainedMainThreadTask<T, O> next) {
            return new ChainHandler<>(executor, next);
        }

        @CheckResult
        public <O> ChainHandler<O> runAsync(@NonNull ChainedWorkerThreadTask<T, O> next) {
            return new ChainHandler<>(executor, next);
        }

        public void onCompleted(@NonNull ChainResult<T> callback) {
            Threads.executeOnMainThread(() -> {
                executor.execute(callback::onResult);
            });
        }
    }

    private interface ChainedExecutor<T> {
        void execute(OnNext<T> next);
    }

    private interface OnNext<I> {
        void doNext(I input);
    }

    private interface FirstTask<T> {
        T start();
    }

    public interface MainThreadTask<T> extends FirstTask<T> {
        @Override @MainThread
        T start();
    }

    public interface WorkerThreadTask<T> extends FirstTask<T> {
        @Override @WorkerThread
        T start();
    }

    private interface ChainedTask<I, O> {
        O apply(I input);
    }

    public interface ChainedMainThreadTask<I, O> extends ChainedTask<I, O> {
        @Override @MainThread
        O apply(I input);
    }

    public interface ChainedWorkerThreadTask<I, O> extends ChainedTask<I, O> {
        @Override @WorkerThread
        O apply(I input);
    }

    public interface ChainResult<T> {
        @MainThread
        void onResult(T result);
    }
}
