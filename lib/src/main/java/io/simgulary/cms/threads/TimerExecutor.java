/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 01/07/19 02:49 PM
 */

package io.simgulary.cms.threads;

import android.os.Handler;
import android.os.Looper;

public final class TimerExecutor {
    private static final Handler handler = new Handler(Looper.getMainLooper());
    private final Runnable task;
    private final Runnable delegated;
    private final long delayInMillis;

    public TimerExecutor(Runnable task, long delayInMillis) {
        this.task = task;
        this.delayInMillis = delayInMillis;
        this.delegated = () -> {
            task.run();

            executeDelayedAndRepeat();
        };
    }

    public void executeAndRepeat() {
        abort();
        handler.post(delegated);
    }

    public void executeDelayedAndRepeat() {
        abort();
        handler.postDelayed(delegated, delayInMillis);
    }

    public void abort() {
        handler.removeCallbacks(delegated);
    }
}
