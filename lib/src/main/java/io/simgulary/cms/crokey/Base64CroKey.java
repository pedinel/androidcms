/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 13/03/19 01:48 PM
 */

package io.simgulary.cms.crokey;

/**
 * Encrypts from BASE_64 to BASE_64
 */
@SuppressWarnings("unused")
public class Base64CroKey extends StringCroKey {
    public Base64CroKey(String privateKey) throws CroKeyException {
        super(privateKey);
    }

    public Base64CroKey(String privateKey, String mode, String hashAlgorithm, int blockDepth) throws CroKeyException {
        super(privateKey, mode, hashAlgorithm, blockDepth);
    }

    @Override
    protected byte[] onRawDecode(String data) {
        byte[] dataBytes = BASE_64.decode(data);

        return fillBlock(dataBytes);
    }

    @Override
    protected String onRawEncode(byte[] data) {
        byte[] fixedBlock = unBlock(data);
        return BASE_64.encode(fixedBlock);
    }
}
