/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 13/03/19 01:48 PM
 */

package io.simgulary.cms.crokey;

import java.util.Locale;
import java.util.UUID;

/**
 * Created by pedro on 28/12/16.
 * Generates random strings
 */

@SuppressWarnings({"unused", "WeakerAccess"})
public final class Random {
	/**
	 * Te charset
	 */
	private static final String CHARSET = "UTF-8";

	private Random() {
	}

	/**
	 * @return the random hexadecimal block of 32-characters
	 */
	private static String hexRand() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}

	/**
	 * @param targetSize the resulting length
	 * @return the random hexadecimal block of the specified length
	 */
	private static String hexRand(int targetSize) {
		if (targetSize < 1) {
			throw new IllegalArgumentException("invalid length");
		}

		if (targetSize > 32) {
			final StringBuilder sb = new StringBuilder();
			int left = targetSize - sb.length();
			while (left > 0) {
				if (left > 32) {
					sb.append(hexRand());
				}
				else {
					sb.append(hexRand().substring(0, left));
				}

				left = targetSize - sb.length();
			}

			return sb.toString();
		}
		else {
			return hexRand().substring(0, targetSize);
		}
	}

	/**
	 * @return the random hexadecimal string
	 */
	public static String generateHex(int length) {
		return hexRand(length);
	}

	/**
	 * @return the random {@code byte[]}
	 */
	public static byte[] generateBytes(int length) {
		final byte[] result = Encoder.hexStringToBytes(hexRand(length * 2));

		if (result.length != length) {
			throw new IllegalStateException(String.format(Locale.ROOT, "Error generating bytes: %d/%d", result.length, length));
		}

		return result;
	}

	/**
	 * @return the random {@code byte[]}
	 */
	public static char[] generateChars(int length) {
		final char[] result = Encoder.bytesToChars(generateBytes(length));

		if (result.length != length) {
			throw new IllegalStateException(String.format(Locale.ROOT, "Error generating chars: %d/%d", result.length, length));
		}

		return result;
	}

	public static String generateBase64(int bytesLength) {
		return Encoder.bytesToBase64(generateBytes(bytesLength));
	}
}
