/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 13/03/19 01:48 PM
 */

package io.simgulary.cms.crokey;

import java.security.GeneralSecurityException;

/**
 * Exceptions thrown by CroKey
 */
@SuppressWarnings("WeakerAccess")
public class CroKeyException extends GeneralSecurityException {
    CroKeyException(String message, Throwable cause) {
        super(message, cause);
    }
}
