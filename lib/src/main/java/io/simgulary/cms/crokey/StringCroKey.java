/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 13/03/19 01:48 PM
 */

package io.simgulary.cms.crokey;

/**
 * Encrypts from String
 */
@SuppressWarnings("WeakerAccess")
public class StringCroKey extends CroKey<String, String> {
    public StringCroKey(String privateKey) throws CroKeyException {
        super(privateKey, AES_CBC_NOPADDING, "SHA-512", 4);
    }

    public StringCroKey(String privateKey, String mode, String hashAlgorithm, int blockDepth) throws CroKeyException {
        super(privateKey, mode, hashAlgorithm, blockDepth);
    }

    @Override
    protected byte[] onDecodePrivateKey(String key) {
        return keyFix(hasher.digest(key.getBytes(CHARSET_UTF_8)));
    }

    @Override
    protected byte[] onDecodePublicKey(String key) {
        return keyFix(hasher.digest(key.getBytes(CHARSET_UTF_8)));
    }

    @Override
    protected byte[] onRawDecode(String data) {
        byte[] dataBytes = data.getBytes(CHARSET_UTF_8);

        return fillBlock(dataBytes);
    }

    @Override
    protected byte[] onCryptDecode(String data) {
        return BASE_64.decode(data);
    }

    @Override
    protected String onRawEncode(byte[] data) {
        byte[] fixedBlock = unBlock(data);
        return new String(fixedBlock, CHARSET_UTF_8);
    }

    @Override
    protected String onCryptEncode(byte[] data) {
        return BASE_64.encode(data);
    }
}
