/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 13/03/19 01:48 PM
 */

package io.simgulary.cms.crokey;

/**
 * Secure Implementation of Data encrypt and decrypt
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public interface CroKeyEncoder<DecryptedType, EncryptedType> {
    /**
     * Encrypts data
     *
     * @param decryptedData the decrypted data
     * @return the encrypted data
     */
    EncryptedType encrypt(DecryptedType decryptedData) throws CroKeyException;

    /**
     * Decrypts data
     *
     * @param encryptedData the encrypted data
     * @return the decrypted data
     */
    DecryptedType decrypt(EncryptedType encryptedData) throws CroKeyException;

    /**
     * Changes the Key to encrypt/decrypt
     *
     * @param publicKey the key
     */
    CroKeyEncoder<DecryptedType, EncryptedType> setKey(String publicKey) throws CroKeyException;
}
