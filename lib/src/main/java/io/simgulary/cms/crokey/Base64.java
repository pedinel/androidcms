/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 13/03/19 01:48 PM
 */

package io.simgulary.cms.crokey;

/**
 * Handles BASE_64 data conversion
 */

@SuppressWarnings("WeakerAccess")
public interface Base64 {
	/**
	 * Converts into BASE_64
	 *
	 * @param data the raw data
	 * @return the BASE_64 data
	 */
	String encode(byte[] data);

	/**
	 * Converts from BASE_64
	 *
	 * @param data the BASE_64 data
	 * @return the raw data
	 */
	byte[] decode(String data);
}
