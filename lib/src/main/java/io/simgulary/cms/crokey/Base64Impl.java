/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 13/03/19 01:48 PM
 */

package io.simgulary.cms.crokey;

/**
 * Base 64 Conversion Handler
 */

final class Base64Impl implements Base64 {
	private static final int ENC_MODE = android.util.Base64.NO_PADDING | android.util.Base64.NO_WRAP;
	private static final int DEC_MODE = android.util.Base64.DEFAULT;

	@Override
	public String encode(byte[] data) {
		return android.util.Base64.encodeToString(data, ENC_MODE);
	}

	@Override
	public byte[] decode(String data) {
		return android.util.Base64.decode(data, DEC_MODE);
	}
}
