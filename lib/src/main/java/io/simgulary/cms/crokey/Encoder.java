/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 13/03/19 01:48 PM
 */

package io.simgulary.cms.crokey;

import android.util.Base64;

/**
 * Encodes/Decodes numeric values into another type representation
 */

@SuppressWarnings({"unused", "WeakerAccess"})
public final class Encoder {
	/**
	 * The byte bits
	 */
	private static final int BYTE_BLOCK = 0xFF;

	/**
	 * The byte-bit offsets
	 */
	private static final int BYTE_SLICE[] = {0, 8, 16, 24};

	/**
	 * The char code
	 */
	private static final char[] digits = {
			'0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'a', 'b', 'c', 'd', 'e', 'f',
			'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
			'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
			'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D',
			'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
			'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
			'U', 'V', 'W', 'X', 'Y', 'Z', '.', '_'
	};

	private Encoder() {
		//prevents instance
	}

	/**
	 * Transforms the input into a {@code byte[]} array
	 *
	 * @param data the data to transform
	 * @return the transformed data
	 *
	 * @see #bytesToChars(byte...)
	 */
	public static byte[] charsToBytes(char... data) {
		final byte[] response = new byte[data.length];
		for (int i = 0; i < data.length; i++) {
			response[i] = (byte) data[i];
		}

		return response;
	}

	/**
	 * Transforms the input into the specified data type
	 *
	 * @param data the input byte data
	 * @return the converted type
	 *
	 * @see #charsToBytes(char...)
	 */
	public static char[] bytesToChars(byte... data) {
		final char[] response = new char[data.length];
		for (int i = 0; i < data.length; i++) {
			response[i] = (char) data[i];
		}

		return response;

	}

	/**
	 * Transforms the input into a {@code byte[]} array
	 *
	 * @param data the data to transform
	 * @return the transformed data
	 */
	public static byte[] integersToBytes(int... data) {
		final char[] buffer = new char[4 * data.length];

		for (int i = 0; i < data.length; i++) {
			final int bufferIndex = i * 4;
			for (int c = 0; c < BYTE_SLICE.length; c++) {
				buffer[bufferIndex + c] = (char) ((data[i] >> BYTE_SLICE[c]) & BYTE_BLOCK);
			}
		}

		return charsToBytes(buffer);
	}

	/**
	 * Transforms the input into the specified data type
	 *
	 * @param data the input byte data
	 * @return the converted type
	 */
	public static int[] bytesToIntegers(byte... data) {
		if (data.length % 4 != 0) {
			throw new IllegalArgumentException("data length is not multiple of 4. Invalid Integer conversion");
		}

		final char[] buffer = bytesToChars(data);
		final int[] out = new int[buffer.length / 4];

		for (int i = 0; i < out.length; i++) {
			final int bufferIndex = i * 4;
			for (int c = 0; c < BYTE_SLICE.length; c++) {
				out[i] |= (buffer[bufferIndex + c] & BYTE_BLOCK) << BYTE_SLICE[c];
			}
		}

		return out;
	}

	/**
	 * Converts a {@code byte[]} array into its BASE_64 representation
	 *
	 * @param data the data to encode
	 * @return the encoded BASE_64 String
	 *
	 * @see #base64ToBytes(String)
	 */
	public static String bytesToBase64(byte... data) {
		return Base64.encodeToString(data, Base64.NO_WRAP | Base64.NO_PADDING);
	}

	/**
	 * Decodes a BASE_64 String into its {@code byte[]} array data value
	 *
	 * @param base64 the data to decode
	 * @return the {@code byte[]} data result
	 */
	public static byte[] base64ToBytes(String base64) {
		return Base64.decode(base64, Base64.NO_WRAP | Base64.NO_PADDING);
	}

	/**
	 * Converts a {@code int[]} array into its BASE_64 representation
	 *
	 * @param data the data to encode
	 * @return the encoded BASE_64 String
	 *
	 * @see #base64ToBytes(String)
	 */
	public static String integersToBase64(int... data) {
		return bytesToBase64(integersToBytes(data));
	}

	/**
	 * Decodes a BASE_64 String into its {@code int[]} array data value
	 *
	 * @param base64 the data to decode
	 * @return the {@code int[]} data result
	 */
	public static int[] base64ToIntegers(String base64) {
		return bytesToIntegers(base64ToBytes(base64));
	}

	/**
	 * converts a {@code byte[]} array into its hexadecimal representation {@code String}
	 * the resulting {@link String#length()} is twice the {@param data} length
	 *
	 * @param data the {@code byte[]} array
	 * @return the hexadecimal representation {@code String}
	 *
	 * @see #hexCharsToBytes(char[])
	 */
	public static char[] bytesToHexChars(byte... data) {
		final int len = data.length;
		final char[] buffer = new char[len * 2];
		int index = 0;

		for (final byte b : data) {
			final int byteData0 = (b & 0xF0) >> 4;
			final int byteData1 = b & 0x0F;

			buffer[index++] = digits[byteData0];
			buffer[index++] = digits[byteData1];
		}

		return buffer;
	}

	public static char[] bytesToDigits(int bits, byte... data) {
		if (bits < 1 || bits > 6) {
			throw new IllegalArgumentException("Bits out of range [1,6]: " + bits);
		}
		int t = data.length * 8;
		final int len = (t / bits) + ((t % bits) != 0 ? 1 : 0);
		final char[] out = new char[len];
		int index = 0;
		int mask = (1 << bits) - 1;
		int buffer = 0;
		int offset = 0;

		for (final byte b : data) {
			buffer |= ((b & 0xFF) << offset);
			offset += 8;

			while (offset >= bits) {
				out[index++] = digits[buffer & mask];
				buffer >>>= bits;
				offset -= bits;
			}
		}

		return out;
	}

	/**
	 * converts a hexadecimal representation {@code char[]} into a {@code byte[]} array
	 * the resulting {@code byte[]} length is half of the {@param str} length
	 *
	 * @param str the hexadecimal representation {@code String}
	 * @return the {@code byte[]} array
	 *
	 * @see #bytesToHexChars(byte[])
	 */
	public static byte[] hexCharsToBytes(char... str) {
		if (str == null) {
			return null;
		}
		else if (str.length % 2 != 0) {
			throw new IllegalArgumentException("str length is not multiple of 2");
		}
		else {
			final int len = str.length / 2;
			final byte[] buffer = new byte[len];
			for (int i = 0; i < len; i++) {
				buffer[i] = (byte) Integer.parseInt(String.valueOf(str, i * 2, 2), 16);
			}
			return buffer;
		}
	}

	/**
	 * Converts a string into its hexadecimal representation for each character
	 *
	 * @param data the input String
	 * @return the hexadecimal String representation
	 *
	 * @see #hexStringToBytes(String)
	 */
	public static String bytesToHexString(byte... data) {
		return new String(bytesToHexChars(data));
	}

	/**
	 * Converts a hexadecimal string into its String value
	 *
	 * @param hexString the hexadecimal input String
	 * @return the String value that represents the input data
	 *
	 * @see #bytesToHexString(byte...)
	 */
	public static byte[] hexStringToBytes(String hexString) {
		return hexCharsToBytes(hexString.toCharArray());
	}
}
