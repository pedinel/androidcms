/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 13/03/19 01:48 PM
 */

package io.simgulary.cms.crokey;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Generic Base AES Crypt algorithm
 */

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class CroKey<DecryptedType, EncryptedType> implements CroKeyEncoder<DecryptedType, EncryptedType> {
	public static final String AES_CBC_NOPADDING = "AES/CBC/NoPadding";//128
	public static final String AES_CBC_PKCS5PADDING = "AES/CBC/PKCS5Padding";//128
	public static final String AES_ECB_NOPADDING = "AES/ECB/NoPadding";//128
	public static final String AES_ECB_PKCS5PADDING = "AES/ECB/PKCS5Padding";//128
	public static final String DES_CBC_NOPADDING = "DES/CBC/NoPadding";//56
	public static final String DES_CBC_PKCS5PADDING = "DES/CBC/PKCS5Padding";//56
	public static final String DES_ECB_NOPADDING = "DES/ECB/NoPadding";//56
	public static final String DES_ECB_PKCS5PADDING = "DES/ECB/PKCS5Padding";//56
	public static final String DESEDE_CBC_NOPADDING = "DESede/CBC/NoPadding";//168
	public static final String DESEDE_CBC_PKCS5PADDING = "DESede/CBC/PKCS5Padding";//168
	public static final String DESEDE_ECB_NOPADDING = "DESede/ECB/NoPadding";//168
	public static final String DESEDE_ECB_PKCS5PADDING = "DESede/ECB/PKCS5Padding";//168
	public static final String RSA_ECB_PKCS1PADDING = "RSA/ECB/PKCS1Padding";//1024-2048
	public static final String RSA_ECB_OAEPWITHSHA1 = "RSA/ECB/OAEPWithSHA-1AndMGF1Padding";//1024-2048
	public static final String RSA_ECB_OAEPWITHSHA256 = "RSA/ECB/OAEPWithSHA-256AndMGF1Padding";//1024-2048
	protected final static byte[] EMPTY_BLOCK = "                                ".getBytes();
	protected final static byte EMPTY_BYTE = EMPTY_BLOCK[0];
	protected static final Charset CHARSET_UTF_8 = Charset.forName("UTF-8");
	protected static final Base64 BASE_64 = new Base64Impl();
	private static MessageDigest md5;
	private static MessageDigest sha1;
	private static MessageDigest sha512;
	protected final MessageDigest hasher;
	protected final int blockSize;
	protected final int blockBits;
	private final Cipher cipher;
	private final IvParameterSpec ivSpec;
	private SecretKeySpec keySpec;
	private boolean encryptMode = false;
	private boolean initialize = true;

	public CroKey(String privateKey, String mode, String hashAlgorithm, int blockDepth) throws CroKeyException {
		try {
			blockSize = (1 << blockDepth);
			blockBits = blockSize - 1;
			cipher = Cipher.getInstance(mode);
			hasher = MessageDigest.getInstance(hashAlgorithm);
			ivSpec = new IvParameterSpec(onDecodePrivateKey(privateKey));
		}
		catch (Exception e) {
			throw new CroKeyException("Unable to initialize CroKey", e);
		}
	}

	/**
	 * Converts a String password for private key into bytes
	 *
	 * @param key the String password
	 * @return the bytes block
	 */
	protected abstract byte[] onDecodePrivateKey(String key);

	/**
	 * Converts a String password for public key into bytes
	 *
	 * @param key the String password
	 * @return the bytes block
	 */
	protected abstract byte[] onDecodePublicKey(String key);

	/**
	 * Converts the data to encrypt into a byte-block
	 *
	 * @param data the data to convert
	 * @return block-fixed bytes
	 */
	protected abstract byte[] onRawDecode(DecryptedType data);

	protected abstract byte[] onCryptDecode(EncryptedType data);

	/**
	 * Converts the bytes into the decrypted data type
	 *
	 * @param data the data to convert. The data is block-fixed and should be decoded first
	 * @return unblocked data
	 */
	protected abstract DecryptedType onRawEncode(byte[] data);

	protected abstract EncryptedType onCryptEncode(byte[] data);

	protected final byte[] fillBlock(byte[] dataBytes) {
		if ((dataBytes.length & blockBits) != 0) {
			int left = blockSize - (dataBytes.length & blockBits);
			byte[] tmp = new byte[dataBytes.length + left];
			System.arraycopy(dataBytes, 0, tmp, 0, dataBytes.length);
			System.arraycopy(EMPTY_BLOCK, 0, tmp, dataBytes.length, left);
			dataBytes = tmp;
		}

		return dataBytes;
	}

	protected final byte[] unBlock(byte[] dataBytes) {
		int k = dataBytes.length - 1;

		while (k >= 0 && dataBytes[k] == EMPTY_BYTE) {
			--k;
		}

		int size = k + 1;
		byte[] fixedBlock = new byte[size];
		System.arraycopy(dataBytes, 0, fixedBlock, 0, size);

		return fixedBlock;
	}

	protected byte[] keyFix(byte[] src) {
		if (src.length > blockSize) {
			byte[] out = new byte[blockSize];
			System.arraycopy(src, src.length >> 1, out, 0, blockSize);
			src = out;
		}

		return src;
	}

	private void init(boolean forEncrypt) throws CroKeyException {
		if (initialize || encryptMode != forEncrypt) {
			try {
				cipher.init(forEncrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, ivSpec);
				initialize = false;
				encryptMode = forEncrypt;
			}
			catch (Exception e) {
				e.printStackTrace();
				throw new CroKeyException("Unable to initialize cipher", e);
			}
		}
	}

	@Override
	public final EncryptedType encrypt(DecryptedType decryptedData) throws CroKeyException {
		if (decryptedData == null)
			return null;

		init(true);

		byte[] decryptedBytes = onRawDecode(decryptedData);

		try {
			byte[] encryptedBytes = cipher.doFinal(decryptedBytes);
			return onCryptEncode(encryptedBytes);
		}
		catch (Exception e) {
			throw new CroKeyException("Encrypt error", e);
		}
	}

	@Override
	public final DecryptedType decrypt(EncryptedType encryptedData) throws CroKeyException {
		if (encryptedData == null)
			return null;

		init(false);

		byte[] encryptedBytes = onCryptDecode(encryptedData);

		try {
			byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
			return onRawEncode(decryptedBytes);
		}
		catch (Exception e) {
			throw new CroKeyException("Decrypt error", e);
		}
	}

	@Override
	public final CroKeyEncoder<DecryptedType, EncryptedType> setKey(String publicKey) throws CroKeyException {
		try {
			keySpec = new AESSecretKeySpec(onDecodePublicKey(publicKey));
			initialize = true;
		}
		catch (Exception e) {
			throw new CroKeyException("Unable to set public key", e);
		}

		return this;
	}

	public static String md5(byte[] in) {
		if (md5 == null) {
			try {
				md5 = MessageDigest.getInstance("MD5");
			}
			catch (NoSuchAlgorithmException ignored) {
			}
		}

		return BASE_64.encode(md5.digest(in));
	}

	public static String md5(String in) {
		return md5(in.getBytes(CHARSET_UTF_8));
	}

	public static String sha1(byte[] in) {
		if (sha1 == null) {
			try {
				sha1 = MessageDigest.getInstance("SHA-1");
			}
			catch (NoSuchAlgorithmException ignored) {
			}
		}

		return BASE_64.encode(sha1.digest(in));
	}

	public static String sha1(String in) {
		return sha1(in.getBytes(CHARSET_UTF_8));
	}

	public static String sha512(byte[] in) {
		if (sha512 == null) {
			try {
				sha512 = MessageDigest.getInstance("SHA-512");
			}
			catch (NoSuchAlgorithmException ignored) {
			}
		}

		return BASE_64.encode(sha512.digest(in));
	}

	public static String sha512(String in) {
		return sha512(in.getBytes(CHARSET_UTF_8));
	}
}
