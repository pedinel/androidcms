/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 31/05/19 11:39 AM
 */

package io.simgulary.cms.http;

import android.os.SystemClock;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.simgulary.cms.http.requests.ErrorResponse;
import io.simgulary.cms.threads.Threads;
import io.simgulary.cms.utils.Logger;
import retrofit2.Response;

final class ApiResponseWrapper<T> implements ApiResponse<T> {
    private static final Logger log = Logger.getLogger(ApiResponse.class);

    private final AtomicBoolean isLoading = new AtomicBoolean(false);
    private final AtomicReference<T> data = new AtomicReference<>();
    private final AtomicReference<Response<T>> response = new AtomicReference<>();
    private final AtomicReference<Throwable> error = new AtomicReference<>();
    private final AtomicReference<String> errorBody = new AtomicReference<>();
    private volatile long dataUpTime;
    private volatile long updateTime;
    private volatile Object cachedError;

    ApiResponseWrapper() {
        dataUpTime = 0L;
        updateTime = SystemClock.elapsedRealtime();
    }

    ApiResponseWrapper(long dataUpTime) {
        this.dataUpTime = dataUpTime;
        this.updateTime = SystemClock.elapsedRealtime();
    }

    boolean isLoading() {
        return isLoading.get();
    }

    @Override
    public boolean isSuccessful() {
        return !hasFailed() && response.get() != null && response.get().isSuccessful();
    }

    @Override
    public boolean hasFailed() {
        return error.get() != null;
    }

    @Override
    public int code() {
        return error.get() == null && response.get() != null ? response.get().code() : -1;
    }

    @Override
    public boolean isCacheResponse() {
        return isSuccessful() && response.get().raw().cacheResponse() != null;
    }

    @Override
    public boolean hasError() {
        return !hasFailed() && response.get() != null && !response.get().isSuccessful();
    }

    @Override
    public long getUpdateTime() {
        return updateTime;
    }

    ApiResponseWrapper<T> setLoading() {
        isLoading.set(true);
        return this;
    }

    ApiResponseWrapper<T> setNotLoading() {
        isLoading.set(false);
        return this;
    }

    ApiResponseWrapper<T> setResponse(Response<T> r,
                                      @NonNull ApiRequest.DataInterceptor<T> interceptor,
                                      @Nullable ApiRequest.ResponseListener<T> l) {
        isLoading.set(false);
        response.set(r);
        error.set(null);

        if (r.isSuccessful()) {
            T result = r.body();

            try {
                result = interceptor.onIntercept(result);
            }
            catch (Throwable e) {
                log.error(e, "Error intercepting request: %s", this);
            }

            data.set(result);
            updateTime = dataUpTime = SystemClock.elapsedRealtime();
            if (l != null) {
                final T emit = result;
                Threads.executeOnMainThread(() -> {
                    l.onResponse(emit);
                });
            }
        } else {
            updateTime = dataUpTime;
            try {
                if (r.errorBody() == null) {
                    log.warn("Request failed %d: unknown", r.code());
                } else {
                    errorBody.set(r.errorBody().string());
                    log.warn("Request failed %d: %s", r.code(), errorBody.get());
                }
            }
            catch (IOException ignored) {
            }
        }

        return this;
    }

    ApiResponseWrapper<T> setData(T result,
                                  @NonNull ApiRequest.DataInterceptor<T> interceptor,
                                  @Nullable ApiRequest.ResponseListener<T> l) {
        result = interceptor.onIntercept(result);
        data.set(result);
        if (l != null) {
            final T emit = result;
            Threads.executeOnMainThread(() -> {
                l.onResponse(emit);
            });
        }
        return this;
    }

    ApiResponseWrapper<T> setError(Throwable t) {
        isLoading.set(false);
        error.set(t);
        updateTime = dataUpTime;

        return this;
    }

    @Override
    public T data() {
        return data.get();
    }

    @Override
    public <R extends ErrorResponse> R error(Class<R> clazz) throws JsonSyntaxException, JsonIOException {
        if (clazz.isInstance(cachedError)) {
            return clazz.cast(cachedError);
        }

        R out;

        try {
            out = clazz.newInstance();
        }
        catch (Throwable ignored) {
            return null;
        }

        if (errorBody.get() != null) {
            try {
                out = BaseHttpClient.baseInstance.gson.fromJson(errorBody.get(), clazz);
                cachedError = out;
            }
            catch (Throwable e) {
                log.error(e, "Unable to read error response: %s", errorBody.get());
            }
        }

        if (out == null) {
            try {
                out = clazz.newInstance();
            }
            catch (Throwable ignored) {
            }
        }

        if (out != null) {
            out.setData(this);
        }

        return out;
    }

    @Override
    public Throwable exception() {
        return error.get();
    }

    ApiResponseWrapper<T> copy() {
        ApiResponseWrapper<T> copy = new ApiResponseWrapper<>(dataUpTime);
        copy.data.set(data.get());
        return copy;
    }

    private Object content() {
        if (isLoading()) {
            return "Loading: true";
        } else if (isSuccessful()) {
            return data();
        } else if (hasError()) {
            return response.get().errorBody();
        } else {
            Throwable exception = exception();
            return exception != null ? exception.getMessage() : null;
        }
    }

    @Override
    public String toString() {
        return String.format("code: %s, data: %s", code(), content());
    }
}
