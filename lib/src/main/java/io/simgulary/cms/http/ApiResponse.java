/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 29/04/19 02:18 PM
 */

package io.simgulary.cms.http;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import androidx.annotation.CheckResult;
import io.simgulary.cms.http.requests.ErrorResponse;

public interface ApiResponse<T> {
    /**
     * @return the latest successful response. This value is kept if further response are
     * not successful
     */
    @CheckResult
    T data();

    /**
     * Converts the response into another type. This only works if {@link #hasError()}
     * returns {@code true}. Otherwise, this returns {@code null}
     *
     * @param clazz the response Class
     * @param <R> the Response Parameter type
     * @return the Parsed Response
     * @throws JsonIOException if there was a problem reading from the Reader
     * @throws JsonSyntaxException if json is not a valid representation for an object of type
     */
    @CheckResult
    <R extends ErrorResponse> R error(Class<R> clazz) throws JsonSyntaxException, JsonIOException;

    /**
     * @return the latest error. This only works if the request could not be completed and throed
     * an exception. Otherwise, this returns {@code null}
     */
    @CheckResult
    Throwable exception();

    /**
     * Checks if the latest request was completed successfully. This means that the request was
     * completed with an status code of 200-299. Also, the {@link #data()} was updated with the
     * latest result.
     *
     * @return {@code true} if the last request was completed successfully
     */
    boolean isSuccessful();

    /**
     * Checks if the latest request was completed but the status code is not 200-299. Any previous
     * {@link #data()} is kept unchanged, but {@link #error(Class)} will retrieve a valid response.
     *
     * @return {@code true} if the last request was completed but the status code is not 200-299
     */
    boolean hasError();

    /**
     * Checks it the latest request could not be completed. If {@code true}, {@link #exception()}
     * will retrieve the exception with the detailed error.
     *
     * @return {@code true} if the latest request could not be completed.
     */
    boolean hasFailed();

    /**
     * @return the Status code, or -1 if failed
     */
    int code();

    /**
     * @return {@code true} if the response were retrieved from cache.
     */
    boolean isCacheResponse();

    /**
     * @return the relative update time for the data
     */
    long getUpdateTime();
}
