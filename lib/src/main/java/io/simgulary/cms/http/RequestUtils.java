/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 03/12/19 06:43 PM
 */

package io.simgulary.cms.http;

import java.util.Collection;
import java.util.Iterator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import io.simgulary.cms.threads.Threads;
import io.simgulary.cms.utils.Identifiable;

public final class RequestUtils {
    private RequestUtils() {
        //no instance
    }

    public static boolean launchIfElapsedMillis(ApiRequest<?> request, long millis) {
        if (request.getMillisSinceLastUpdate() >= millis) {
            return request.launch();
        } else {
            return false;
        }
    }

    public static <T, C extends Collection<T>>
    void updateMatch(ApiRequest<C> requestToUpdate,
                     ApiRequest<T> updater,
                     Finder<T, T> matchCheck,
                     Finder<T, T> updateCheck,
                     Finder<T, T> deleteCheck) {

        updater
                .getResponse()
                .observeForever(new Observer<ApiResponse<T>>() {
                    @Override
                    public void onChanged(ApiResponse<T> response) {
                        if (response == null)
                            return;

                        if (response.isSuccessful()) {
                            updater.getResponse().removeObserver(this);

                            T updatedValue = response.data();
                            update(requestToUpdate, updatedValue, matchCheck, updateCheck, deleteCheck);
                        }
                    }
                });
    }

    public static <T extends Identifiable, C extends Collection<T>>
    void updateMatch(ApiRequest<C> requestToUpdate,
                     ApiRequest<T> updater) {
        updateMatch(requestToUpdate,
                updater,
                (v, k) -> v.getId() == k.getId(),
                (v, k) -> v.getUpdatedAtMillis() < k.getUpdatedAtMillis(),
                (v, k) -> v.getDeletedAtMillis() <= k.getDeletedAtMillis());
    }

    public static <T, C extends Collection<T>, D extends Collection<T>>
    void updateMatches(ApiRequest<C> requestToUpdate,
                       ApiRequest<D> updater,
                       Finder<T, T> matchCheck,
                       Finder<T, T> updateCheck,
                       Finder<T, T> deleteCheck) {

        updater.getResponse()
                .observeForever(new Observer<ApiResponse<D>>() {
                    @Override
                    public void onChanged(ApiResponse<D> response) {
                        if (response == null)
                            return;

                        if (response.isSuccessful()) {
                            updater.getResponse().removeObserver(this);

                            D updatedData = response.data();
                            updateList(requestToUpdate, updatedData, matchCheck, updateCheck, deleteCheck);
                        }
                    }
                });
    }

    public static <T extends Identifiable, C extends Collection<T>, D extends Collection<T>>
    void updateMatches(ApiRequest<C> requestToUpdate,
                       ApiRequest<D> updater) {
        updateMatches(requestToUpdate,
                updater,
                (v, k) -> v.getId() == k.getId(),
                (v, k) -> v.getUpdatedAtMillis() < k.getUpdatedAtMillis(),
                (v, k) -> v.getDeletedAtMillis() < k.getDeletedAtMillis());
    }

    public static <T, C extends Collection<T>>
    void update(@NonNull ApiRequest<C> requestToUpdate,
                @NonNull T updatedValue,
                @NonNull Finder<T, T> matchCheck,
                @Nullable Finder<T, T> updateCheck,
                @Nullable Finder<T, T> deleteCheck) {

        C responseData = requestToUpdate.getResponseData();

        if (responseData == null)
            return;

        if (updateCheck == null && deleteCheck == null)
            throw new IllegalArgumentException("updatedCheck and deleteCheck are both null");

        Finder<T, T> update = (updateCheck != null) ? updateCheck : deleteCheck;

        C out;

        try {
            //noinspection unchecked
            out = (C) responseData.getClass().newInstance();
        }
        catch (Throwable e) {
            throw new RuntimeException(e);
        }

        Threads.runAsync(() -> {
            Iterator<T> it = responseData.iterator();
            boolean valueNotFound = true;

            while (it.hasNext()) {
                T value = it.next();

                if (matchCheck.test(value, updatedValue)) {
                    valueNotFound = false;

                    if (update.test(value, updatedValue)) {
                        if (deleteCheck == null || !deleteCheck.test(value, updatedValue)) {
                            out.add(updatedValue);
                        }
                    } else {
                        out.add(value);
                    }
                    break;
                }

                out.add(value);
            }

            while (it.hasNext()) {
                out.add(it.next());
            }

            if (valueNotFound && deleteCheck == null) {
                out.add(updatedValue);
            }

            return out;
        }).onCompleted(result -> {
            if (requestToUpdate.getResponseData() != responseData)
                //content was updated
                return;

            requestToUpdate.replace(result);
        });

    }

    public static <T, C extends Collection<T>, D extends Collection<T>>
    void updateList(ApiRequest<C> requestToUpdate,
                D updatedData,
                Finder<T, T> matchCheck,
                Finder<T, T> updateCheck,
                Finder<T, T> deleteCheck) {

        C responseData = requestToUpdate.getResponseData();

        if (responseData == null)
            return;

        if (updateCheck == null && deleteCheck == null)
            throw new IllegalArgumentException("updatedCheck and deleteCheck are both null");

        Finder<T, T> update = (updateCheck != null) ? updateCheck : deleteCheck;
        Finder<T, T> delete = (deleteCheck != null) ? deleteCheck : (v, k) -> false;

        C out;

        try {
            //noinspection unchecked
            out = (C) responseData.getClass().newInstance();
        }
        catch (Throwable e) {
            throw new RuntimeException(e);
        }

        Threads.runAsync(() -> {
            for (T value : responseData) {
                boolean valueNotFound = true;

                for (T updatedValue : updatedData) {
                    if (matchCheck.test(value, updatedValue)) {
                        valueNotFound = false;

                        if (update.test(value, updatedValue)) {
                            if (!delete.test(value, updatedValue)) {
                                out.add(updatedValue);
                            }
                        } else {
                            out.add(value);
                        }
                        break;
                    }
                }

                if (valueNotFound) {
                    out.add(value);
                }
            }

            return out;
        }).onCompleted(result -> {
            if (requestToUpdate.getResponseData() != responseData)
                //content was updated
                return;

            requestToUpdate.replace(result);
        });

    }
}
