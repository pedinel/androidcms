/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 23/04/19 01:21 PM
 */

package io.simgulary.cms.http;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

import androidx.lifecycle.LiveData;
import io.simgulary.cms.lifecycle.Observable;
import io.simgulary.cms.lifecycle.ObservableList;
import io.simgulary.cms.lifecycle.ObservableLists;
import io.simgulary.cms.lifecycle.ObservableMap;
import io.simgulary.cms.lifecycle.ObservableMaps;
import io.simgulary.cms.lifecycle.ObservableMass;
import io.simgulary.cms.lifecycle.ObservableMasses;
import io.simgulary.cms.lifecycle.Observables;

abstract class LiveDataAdapter<K extends LiveData<?>> implements JsonSerializer<K>, JsonDeserializer<K> {
    private static final Type GENERIC_TYPE = new TypeToken<java.util.Map<String, String>>(){}.getType();
    @Override
    public K deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (typeOfT instanceof ParameterizedType) {
            Type[] arguments = ((ParameterizedType) typeOfT).getActualTypeArguments();

            if (arguments.length > 0) {
                Type typeOfArgument = arguments[0];
                return make(context.deserialize(json, typeOfArgument));
            }
        }

        return make(context.deserialize(json, GENERIC_TYPE));
    }

    @Override
    public JsonElement serialize(K src, Type typeOfSrc, JsonSerializationContext context) {
        if (src == null)
            return JsonNull.INSTANCE;

        Object value = src.getValue();

        if (value == null)
            return JsonNull.INSTANCE;

        return context.serialize(value);
    }

    protected abstract K make(Object value);

    public static final class Single extends LiveDataAdapter<Observable<?>> {
        @Override
        protected Observable<?> make(Object value) {
            return Observables.of(value);
        }
    }

    public static final class Mass extends LiveDataAdapter<ObservableMass<?, ?>> {
        @Override
        protected ObservableMass<?, ?> make(Object value) {
            return ObservableMasses.of((Collection<?>) value);
        }
    }

    public static final class List extends LiveDataAdapter<ObservableList<?>> {
        @Override
        protected ObservableList<?> make(Object value) {
            return ObservableLists.of((java.util.List<?>) value);
        }
    }

    public static final class Map extends LiveDataAdapter<ObservableMap<?, ?>> {
        @Override
        protected ObservableMap<?, ?> make(Object value) {
            return ObservableMaps.of((java.util.Map<?, ?>) value);
        }
    }
}
