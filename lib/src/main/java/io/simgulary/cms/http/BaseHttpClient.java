/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 03/12/19 08:49 PM
 */

package io.simgulary.cms.http;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import io.simgulary.cms.BuildConfig;
import io.simgulary.cms.lifecycle.Observable;
import io.simgulary.cms.lifecycle.ObservableList;
import io.simgulary.cms.lifecycle.ObservableMass;
import io.simgulary.cms.utils.Logger;
import okhttp3.Cache;
import okhttp3.Dispatcher;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * The App Http Client to connect to API
 */
@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class BaseHttpClient {
    private static final Logger log = Logger.getLogger(BaseHttpClient.class);
    protected final Gson gson;
    protected static volatile BaseHttpClient baseInstance;
    protected boolean useOfflineCache = true;
    private CacheInterceptor mCacheInterceptor;

    private final Retrofit retrofit;

    // Caches the built services. They are lazy built when required
    private final Map<Class<?>, SoftReference<?>> cache = new HashMap<>();

    protected BaseHttpClient(Context context, String baseUrl) {
        baseInstance = this;

        LiveDataAdapter.Single singleLiveData = new LiveDataAdapter.Single();
        LiveDataAdapter.List multiLiveData = new LiveDataAdapter.List();

        GsonBuilder b = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .registerTypeAdapter(LiveData.class, singleLiveData)
                .registerTypeAdapter(MutableLiveData.class, singleLiveData)
                .registerTypeAdapter(MediatorLiveData.class, singleLiveData)
                .registerTypeAdapter(Observable.class, singleLiveData)
                .registerTypeAdapter(ObservableMass.class, new LiveDataAdapter.Mass())
                .registerTypeAdapter(ObservableList.class, new LiveDataAdapter.List())
                .registerTypeAdapter(LiveDataAdapter.Map.class, new LiveDataAdapter.Map());

        gson = onBuildGson(b);

        this.retrofit = buildRetrofit(context.getApplicationContext(), baseUrl);
    }

    public final void setOfflineMode(boolean enabled) {
        if (mCacheInterceptor != null) {
            mCacheInterceptor.setAvoidNetwork(enabled);
        }
    }

    protected Gson onBuildGson(GsonBuilder gsonBuilder) {
        gsonBuilder.registerTypeAdapter(Map.class, new MapAdapter());

        return gsonBuilder.create();
    }

    @NonNull
    protected Retrofit buildRetrofit(Context context, String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(buildClient(context))
                .callbackExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                .addConverterFactory(buildGsonConverter())
                .addCallAdapterFactory(buildResponseCallFactory())
                .build();
    }

    private static CallAdapter.Factory buildResponseCallFactory() {
        return new ApiResponseFactory();
    }

    @NonNull
    private OkHttpClient buildClient(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .dispatcher(onBuildDispatcher())
                .cache(onBuildCache(context))
                .addInterceptor(onBuildHeaderInterceptor(context));

        if (useOfflineCache) {
            mCacheInterceptor = new CacheInterceptor(context);
            builder.addInterceptor(mCacheInterceptor);
        }

        return onBuildHttpClient(builder);
    }

    protected Dispatcher onBuildDispatcher() {
        return new Dispatcher(onBuildThreadExecutor());
    }

    protected ExecutorService onBuildThreadExecutor() {
        return new ThreadPoolExecutor(1, 1,
                60L, TimeUnit.SECONDS,
                new LinkedBlockingQueue<>());
    }

    @NonNull
    protected Interceptor onBuildHeaderInterceptor(Context context) {
        return new HeaderInterceptor();
    }

    @NonNull
    protected Cache onBuildCache(Context context) {
        File httpCacheDirectory = new File(context.getFilesDir(), "http");
        int cacheSize = 48 * 1024 * 1024; // 48 MiB
        return new Cache(httpCacheDirectory, cacheSize);
    }

    @NonNull
    protected OkHttpClient onBuildHttpClient(OkHttpClient.Builder builder) {
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor.Logger logger = new HttpLoggingInterceptor.Logger() {
                @Override
                public void log(String message) {
                    try {
                        if (message.length() <= 4000) {
                            log.debug(message);
                            return;
                        }
                        else {
                            String v = message.substring(0, 4000);
                            log.debug(v);
                        }

                        message = message.substring(4000);
                        log(message);
                    }
                    catch (OutOfMemoryError e) {
                        log.debug("Out of memory. Can not display Json");
                    }
                }
            };

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(logger);
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder.addInterceptor(interceptor);
        }


        return builder.build();
    }

    @NonNull
    private GsonConverterFactory buildGsonConverter() {
        return GsonConverterFactory.create(gson);
    }

    /**
     * Retrieves the Specified Service Implementation
     *
     * @param clazz the Service Class Name
     * @param <T> the Class Type
     * @return the Service Instance
     */
    @NonNull
    protected <T> T getService(Class<T> clazz) {
        SoftReference<?> holder = cache.get(clazz);

        Object cached = (holder != null) ? holder.get() : null;

        if (cached != null) {
            return clazz.cast(cached);
        }

        T ref = retrofit.create(clazz);
        cache.put(clazz, new SoftReference<>(ref));
        log.debug("New Service: %s", clazz.getSimpleName());

        return ref;
    }
}
