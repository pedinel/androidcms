/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 02/07/19 05:44 PM
 */

package io.simgulary.cms.http;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.simgulary.cms.lifecycle.Mutable;
import io.simgulary.cms.utils.Logger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

final class ApiResponseHandler<T> implements Callback<T> {
    private static final Logger log = Logger.getLogger(ApiResponseHandler.class);
    private static final int MAX_ATTEMPTS = 1;
    private final Mutable<ApiResponse<T>> delegated;
    @NonNull private final ApiRequest.DataInterceptor<T> interceptor;
    private final AtomicReference<ApiRequest.ResponseListener<T>> listener;
    private final AtomicReference<ApiRequest.HeadersInterceptor> headersListener;
    @Nullable private volatile ApiResponseWrapper<T> wrapper;
    private final AtomicInteger attemptsCount = new AtomicInteger(0);

    ApiResponseHandler(Mutable<ApiResponse<T>> delegated,
                       @NonNull ApiRequest.DataInterceptor<T> interceptor,
                       AtomicReference<ApiRequest.ResponseListener<T>> listener,
                       AtomicReference<ApiRequest.HeadersInterceptor> headersListener) {
        this.delegated = delegated;
        this.interceptor = interceptor;
        this.listener = listener;
        this.headersListener = headersListener;
    }

    void execute(Call<T> call, boolean async) {
        if (async) {
            attemptsCount.set(0);
            synchronized (this) {
                delegated.postValue(newResponse().setLoading());
            }
            call.enqueue(this);
        } else {
            try {
                Response<T> response = call.execute();
                onResponse(call, response);
            }
            catch (IOException e) {
                onFailure(call, e);
            }
        }
    }

    @Override
    public void onResponse(@NonNull Call<T> call, @NonNull Response<T> response) {

        onResponse(response);
        synchronized (this) {
            delegated.postValue(newResponse().setResponse(response, interceptor, listener.get()));
        }
    }

    @Override
    public void onFailure(@NonNull Call<T> call, @NonNull Throwable t) {
        log.error(t, "Request Failed");
        if (attemptsCount.incrementAndGet() < MAX_ATTEMPTS) {
            call.clone().enqueue(this);
        } else {
            synchronized (this) {
                delegated.postValue(newResponse().setError(t));
            }
        }
    }

    void replaceData(@NonNull T newData) {
        synchronized (this) {
            ApiResponseWrapper<T> w = this.wrapper;
            if (w != null && w.isSuccessful()) {
                delegated.postValue(w.setData(newData, interceptor, listener.get()));
            }
        }
    }

    @Nullable
    ApiResponse<T> response() {
        return wrapper;
    }

    private ApiResponseWrapper<T> newResponse() {
        final ApiResponseWrapper<T> out;

        ApiResponseWrapper<T> wrapper = this.wrapper;
        if (wrapper == null) {
            out = new ApiResponseWrapper<>();
        } else {
            out = wrapper.copy();
        }

        this.wrapper = out;
        return out;
    }

    private void onResponse(Response<T> response) {
        ApiRequest.HeadersInterceptor interceptor = headersListener.get();

        if (interceptor != null) {
            interceptor.onIntercept(response.headers());
        }
    }

    @MainThread
    void finishLoad() {
        synchronized (this) {
            ApiResponseWrapper<T> wrapper = this.wrapper;
            if (wrapper != null && wrapper.isLoading()) {
                delegated.setValue(newResponse().setNotLoading());
            }
        }
    }
}
