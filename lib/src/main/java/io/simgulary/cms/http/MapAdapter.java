/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 27/06/19 05:45 PM
 */

package io.simgulary.cms.http;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;

final class MapAdapter implements JsonDeserializer<Map> {
    @Override
    public Map deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (!json.isJsonObject()) {
            if (json.isJsonArray() && json.getAsJsonArray().size() == 0) {
                return new LinkedHashMap<>();
            } else {
                return null;
            }
        }

        Type valueType;

        if (typeOfT instanceof ParameterizedType) {
            Type[] actualTypeArguments = ((ParameterizedType) typeOfT).getActualTypeArguments();
            valueType = actualTypeArguments[1];
        } else {
            valueType = Object.class;
        }

        JsonObject object = json.getAsJsonObject();
        Map<String, ?> out = new LinkedHashMap<>(object.size());

        for (Map.Entry<String, JsonElement> e : object.entrySet()) {
            out.put(e.getKey(), context.deserialize(e.getValue(), valueType));
        }

        return out;
    }
}
