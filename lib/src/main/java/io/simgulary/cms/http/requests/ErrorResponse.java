/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */

package io.simgulary.cms.http.requests;

import android.content.Context;

import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import io.simgulary.cms.app.AppApplication;
import io.simgulary.cms.http.ApiResponse;

@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class ErrorResponse {
    private volatile String localizedMessage;

    private int responseCode;
    protected boolean sessionInvalid;

    @Nullable
    public abstract String getMessage();

    @Nullable
    public abstract Map<String, List<String>> getErrors();

    @NonNull
    public final String getLocalizedMessage() {
        if (localizedMessage == null) {
            AppApplication context = AppApplication.get();

            String message = getMessage();

            if (message != null) {
                localizedMessage = convertLocalizedMessage(context, message, responseCode, getErrors());
            }

            if (localizedMessage == null) {
                localizedMessage = getGenericMessage(context);
            }
        }

        return localizedMessage;
    }

    public final void setData(ApiResponse response) {
        this.responseCode = response.code();
    }

    @Nullable
    protected abstract String convertLocalizedMessage(Context context, String rawMessage, int responseCode, @Nullable Map<String, List<String>> errors);

    @NonNull
    protected abstract String getGenericMessage(Context context);

    public final boolean isSessionInvalid() {
        return sessionInvalid;
    }
}
