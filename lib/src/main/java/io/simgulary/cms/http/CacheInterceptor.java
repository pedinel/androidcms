/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified by Pedro Parra 01/04/19 12:35 PM
 */

package io.simgulary.cms.http;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import io.simgulary.cms.utils.Logger;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

@SuppressWarnings("unused")
final class CacheInterceptor implements Interceptor {
    private static final Logger log = Logger.getLogger(CacheInterceptor.class);
    private final Context context;
    private boolean avoidNetwork = false;

    CacheInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();

        CacheControl cacheControl;


        if (hasNetwork()) {
            String cacheControlHeader = request.header("Cache-Control");
            if (cacheControlHeader != null) {
                cacheControl = new CacheControl.Builder()
                        .maxAge(5, TimeUnit.SECONDS)
                        .build();

                log.debug("useNetwork: %s", request);
            } else {
                cacheControl = null;
                log.debug("useNetworkOnly: %s", request);
            }
        } else {
            cacheControl = new CacheControl.Builder()
                    .maxStale(14, TimeUnit.DAYS)
                    .build();

            request = request.newBuilder()
                    .cacheControl(cacheControl)
                    .build();

            log.debug("useOfflineCache: %s", request);
        }

        Response response = chain.proceed(request);

        if (cacheControl != null && response.isSuccessful() && response.networkResponse() != null) {
            log.debug("Storing response: %s", request);
            return response.newBuilder()
                    .header("Cache-Control", cacheControl.toString())
                    .build();
        }

        return response;
    }

    void setAvoidNetwork(boolean avoidNetwork) {
        this.avoidNetwork = avoidNetwork;
    }

    @SuppressLint("MissingPermission")
    private boolean hasNetwork() {
        if (avoidNetwork) {
            return false;
        }

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork;

        try {
            activeNetwork = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
            return activeNetwork != null && activeNetwork.isConnected();
        }
        catch (Throwable e) {
            log.error(e, "Unable to get network state");
            return true;
        }
    }
}
