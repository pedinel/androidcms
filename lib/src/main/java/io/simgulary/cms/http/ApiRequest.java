/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 17/12/20 08:21 PM.
 */

package io.simgulary.cms.http;

import android.os.SystemClock;

import java.io.Closeable;
import java.util.concurrent.atomic.AtomicReference;

import androidx.annotation.CallSuper;
import androidx.annotation.CheckResult;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import io.simgulary.cms.lifecycle.Mutable;
import io.simgulary.cms.lifecycle.Observable;
import io.simgulary.cms.utils.Logger;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Response;

@SuppressWarnings({"unused", "WeakerAccess"})
public class ApiRequest<Type> implements Closeable {
    private static final Logger log = Logger.getLogger(ApiRequest.class);
    private static final ActiveListener SILENT_LISTENER;

    private final Mutable<ApiResponse<Type>> internalResponse;
    private final Observable<ApiResponse<Type>> internalDelegatedResponse;
    private final AtomicReference<DataInterceptor<Type>> interceptor = new AtomicReference<>();
    private final AtomicReference<ResponseListener<Type>> responseListener = new AtomicReference<>();
    private final AtomicReference<HeadersInterceptor> headersListener = new AtomicReference<>();
    private final ApiResponseHandler<Type> callback;
    private final ApiRequestFlag statusFlag = new ApiRequestFlag();

    @Nullable private volatile Observable<ApiResponse<Type>> response;
    @Nullable private volatile Observable<Boolean> loading;
    @Nullable private volatile Observable<Type> data;
    @Nullable private volatile Call<Type> call;
    @Nullable private volatile ActiveListener activeListener;

    static {
        SILENT_LISTENER = ApiRequest::launchIfNull;
    }

    protected ApiRequest(@NonNull Call<Type> delegatedCall) {
        this.call = delegatedCall;

        internalResponse = Mutable.doOnActive(this::onActiveObservers);

        internalDelegatedResponse = internalResponse.chain((apiResponse, emitter) -> {
            if (apiResponse.isSuccessful()) {
                statusFlag.setReplied();
            }

            resetCall();
            emitter.onNext(apiResponse);

            if (apiResponse.isSuccessful() && statusFlag.isOutdated()) {
                launch();
            }
        });

        callback = new ApiResponseHandler<>(internalResponse, response -> {
            log.debug("response retrieved " + (response != null));
            response = onDataResponse(response);

            DataInterceptor<Type> delegated = interceptor.get();

            if (delegated != null) {
                log.debug("calling interceptor " + delegated);
                response = delegated.onIntercept(response);
            }

            return response;
        }, responseListener, headersListener);
    }

    /**
     * Applies a custom interceptor to make data conversions.
     *
     * @param interceptor the Interceptor
     */
    public void setDataInterceptor(@Nullable DataInterceptor<Type> interceptor) {
        this.interceptor.set(interceptor);
    }

    public ApiRequest<Type> setDataUpdatedListener(ResponseListener<Type> listener) {
        this.responseListener.set(listener);
        return this;
    }

    public void setHeadersInterceptor(HeadersInterceptor listener) {
        this.headersListener.set(listener);
    }

    /**
     * This makes the Request to execute automatically if needed
     *
     * @return self
     */
    @CheckResult
    public ApiRequest<Type> withAutoLaunch() {
        if (activeListener != SILENT_LISTENER) {
            activeListener = SILENT_LISTENER;
        }
        return this;
    }

    @CheckResult
    public ApiRequest<Type> withoutAutoLaunch() {
        if (activeListener != null) {
            activeListener = null;
        }
        return this;
    }

    /**
     * This makes the Request to execute automatically if needed
     *
     * @return self
     */
    @CheckResult
    public ApiRequest<Type> withAutoLaunch(long expireTimeMillis) {
        ActiveListener l = this.activeListener;
        if (l instanceof ExpiringActiveListener) {
            if (((ExpiringActiveListener) l).expireTimeMillis == expireTimeMillis)
                return this;
        }

        this.activeListener = new ExpiringActiveListener(expireTimeMillis);
        return this;
    }

    /**
     * Checks if the request is prepared to be launched safety
     */
    public boolean isPrepared() {
        final Call<Type> call;

        synchronized (callback) {
            call = this.call;
        }

        //noinspection SimplifiableIfStatement
        if (call == null) {
            return false;
        }

        return !call.isExecuted() && !call.isCanceled();
    }

    /**
     * Retrieves the Last Update Time using {@link SystemClock#elapsedRealtime()}
     *
     * @return the Last Update Time
     */
    public final long getLastUpdateTime() {
        ApiResponse<Type> response = callback.response();

        if (response == null)
            return 0L;

        return response.getUpdateTime();
    }

    public final long getMillisSinceLastUpdate() {
        return SystemClock.elapsedRealtime() - getLastUpdateTime();
    }

    /**
     * Launches the Request if not launched already. If any launched request is already completed,
     * this will work again
     */
    @SuppressWarnings("unused")
    public final boolean launch() {
        return launchCall();
    }

    /**
     * Launches the Request if data is null.
     */
    @SuppressWarnings("unused")
    public final boolean launchIfNull() {
        if (!hasData()) {
            return launch();
        } else {
            return false;
        }
    }

    /**
     * Launches the Request if data is null.
     */
    @SuppressWarnings("unused")
    public final boolean launchIfFailed() {
        ApiResponse<Type> response = callback.response();

        if (response == null)
            return false;

        return response.isSuccessful() || launch();
    }

    public final boolean launchIfElapsedMillis(long millis) {
        if (getMillisSinceLastUpdate() >= millis) {
            return launch();
        } else {
            return false;
        }
    }

    public final ApiResponse<Type> launchAndWait() {
        return launchCallSync();
    }

    @CheckResult @Deprecated
    public final boolean hasEmptyData() {
        ApiResponse<Type> response = callback.response();
        return response == null || response.data() == null;
    }

    @CheckResult
    public final boolean hasData() {
        ApiResponse<Type> response = callback.response();
        return response != null && response.data() != null;
    }

    @CheckResult
    public final Type getResponseData() {
        ApiResponse<Type> response = callback.response();
        return response != null ? response.data() : null;
    }

    /**
     * Cancels the Request
     */
    @MainThread
    public final void cancel() {
        final Call<Type> call;

        synchronized (callback) {
            call = this.call;
        }

        if (call == null)
            return;

        if (!call.isCanceled()) {
            call.cancel();
            callback.finishLoad();
        }
    }

    /**
     * Forces data replacement without consuming the request. This only works if the last request is successful
     * @param data the new data to replace
     */
    public void replace(Type data) {
        callback.replaceData(data);
    }

    public void setDummy(Type data) {
        //noinspection ConstantConditions
        callback.onResponse(call, Response.success(data));
    }

    public final Observable<Boolean> getLoading() {
        if (loading == null) {
            return makeLoading();
        }

        return loading;
    }

    public final Observable<Type> getData() {
        if (data == null) {
            return makeData();
        }

        return data;
    }

    public final Observable<ApiResponse<Type>> getResponse() {
        if (response == null) {
            return makeResponse();
        }

        return response;
    }

    public final boolean isNotLaunched() {
        return internalResponse.getValue() == null;
    }

    public final void observe(LifecycleOwner owner, Observer<ApiResponse<Type>> observer) {
        getResponse().observe(owner, observer);
    }

    @MainThread
    public final void removeObservers(LifecycleOwner owner) {
        Observable<ApiResponse<Type>> response = this.response;
        if (response != null)
            response.removeObservers(owner);

        Observable<Type> data = this.data;
        if (data != null)
            data.removeObservers(owner);

        Observable<Boolean> loading = this.loading;
        if (loading != null)
            loading.removeObservers(owner);
    }

    public void reset() {
        synchronized (callback) {
            final Call<Type> call = this.call;

            if (call != null && !call.isCanceled() && call.isExecuted()) {
                this.call = call.clone();
                call.cancel();
                callback.finishLoad();
            }
        }
    }

    /**
     * Marks the request as outdated. The request will be updated the next time is needed
     */
    public void setOutdated() {
        if (statusFlag.setOutdated()) {
            launch();
        }
    }

    /**
     * Marks the request as outdated. The request will be updated the next time is needed
     */
    public void setOutdatedIfElapsedMillis(long millis) {
        if (getMillisSinceLastUpdate() >= millis) {
            setOutdated();
        }
    }

    /**
     * Allow data conversions or transformations for the request. If null, no response were retrieved.
     * This is only triggered in a successful response.
     *
     * @param data the original response
     * @return the output. This can be passed to a custom interceptor. see {@link #setDataInterceptor(DataInterceptor)}
     */
    protected Type onDataResponse(@Nullable Type data) {
        return data;
    }

    /**
     * The Request has observers
     */
    @MainThread @CallSuper
    protected void onActiveObservers() {
        if (statusFlag.isOutdated()) {
            launch();
        }

        ActiveListener l = activeListener;

        if (l != null) {
            l.onActive(this);
        }
    }

    private void resetCall() {
        final Call<Type> call;

        synchronized (callback) {
            call = this.call;
        }

        if (call == null)
            return;


        if (!call.isCanceled() && call.isExecuted()) {
            Call<Type> copy = call.clone();
            synchronized (callback) {
                if (this.call != null) {
                    this.call = copy;
                }
            }
        }
    }

    private synchronized void prepareNewLaunch() {
        final Call<Type> call;

        synchronized (callback) {
            call = this.call;
        }

        if (call == null)
            return;

        if (call.isCanceled() || call.isExecuted()) {
            //noinspection UnnecessaryLocalVariable
            Call<Type> copy = call.clone();
            synchronized (callback) {
                if (this.call != null) {
                    this.call = copy;
                }
            }
        }
    }

    private synchronized boolean launchCall() {
        if (isPrepared()) {
            statusFlag.setLaunched();
            callback.execute(call, true);
            return true;
        }

        return false;
    }

    private synchronized ApiResponse<Type> launchCallSync() {
        prepareNewLaunch();
        callback.execute(call, false);

        return callback.response();
    }

    private synchronized Observable<Boolean> makeLoading() {
        if (loading == null) {
            loading = internalDelegatedResponse.chain((response, emitter) -> {
                if (response == null) {
                    emitter.onNext(false);
                    return;
                }

                Boolean value = emitter.value();
                boolean loading = ((ApiResponseWrapper) response).isLoading();

                if (value == null || value != loading) {
                    emitter.onNext(loading);
                }
            });
        }

        return loading;
    }

    private synchronized Observable<Type> makeData() {
        if (data == null) {
            data = internalDelegatedResponse.chain((response, emitter) -> {
                if (response == null)
                    return;

                Type oldValue = emitter.value();
                Type newValue = response.data();

                if (oldValue != newValue) {
                    emitter.onNext(newValue);
                }
            });
        }

        return data;
    }

    private synchronized Observable<ApiResponse<Type>> makeResponse() {
        if (response == null) {
            response = internalDelegatedResponse.chain((apiResponse, emitter) -> {
                if (apiResponse == null || ((ApiResponseWrapper) apiResponse).isLoading())
                    return;

                emitter.onNext(apiResponse);
            });
        }

        return response;
    }

    @Override @MainThread
    public final void close() {
        final Call<Type> call;

        synchronized (callback) {
            call = this.call;

            if (call == null) {
                return;
            }

            this.call = null;
        }

        call.cancel();
        callback.finishLoad();
    }

    @MainThread
    public final void close(LifecycleOwner owner) {
        close();
        removeObservers(owner);
    }

    @CheckResult
    public final RequestRefreshBuilder<Type> refreshHandler(LifecycleOwner owner) {
        return new RequestRefreshBuilder<>(owner, this);
    }

    @NonNull
    @Override
    public String toString() {
        Call<Type> call = this.call;
        if (call == null)
            return super.toString();

        return String.format("ApiRequest %s", call.request().url());
    }

    public interface HeadersInterceptor {
        void onIntercept(Headers headers);
    }

    public interface DataInterceptor<T> {
        /**
         * Intercepts the response to allow conversions
         *
         * @param response the original response
         * @see ApiRequest#onDataResponse(T)
         * @return the converted response
         */
        T onIntercept(@Nullable T response);
    }

    public interface ResponseListener<T> {
        void onResponse(T response);
    }

    private interface ActiveListener {
        void onActive(ApiRequest<?> request);
    }

    private static class ExpiringActiveListener implements ActiveListener {
        final long expireTimeMillis;

        private ExpiringActiveListener(long expireTimeMillis) {
            this.expireTimeMillis = expireTimeMillis;
        }

        @Override
        public void onActive(ApiRequest<?> request) {
            if (request.hasData()) {
                RequestUtils.launchIfElapsedMillis(request, expireTimeMillis);
            } else {
                request.launch();
            }
        }
    }
}
