/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */
package io.simgulary.cms.http.requests;

import java.io.Serializable;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import io.simgulary.cms.BR;

/**
 * Request Parameter
 */
@SuppressWarnings("unused")
public class Parameter<T> extends BaseObservable implements Serializable {
    T value;

    @Bindable
    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        if (this.value != value) {
            this.value = value;
            notifyPropertyChanged(BR.value);
        }
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    public static class Numeric extends Parameter<Long> {
        @Override
        public void setValue(Long value) {
            if (this.value == null || !this.value.equals(value)) {
                super.setValue(value);
            }
        }

        @Override
        public String toString() {
            return (value == null) ? null : value.toString();
        }
    }

    public static class Decimate extends Parameter<Double> {
        @Override
        public void setValue(Double value) {
            if (this.value == null || !this.value.equals(value)) {
                super.setValue(value);
            }
        }

        @Override
        public String toString() {
            return (value == null) ? null : value.toString();
        }
    }

    public static class Text extends Parameter<String> {
        @Override
        public String toString() {
            return value;
        }
    }
}