/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 23/11/20 04:14 PM.
 */

package io.simgulary.cms.http;

import android.os.SystemClock;

import java.util.concurrent.atomic.AtomicLong;

import androidx.annotation.CheckResult;

final class ApiRequestFlag {
    private final AtomicLong launchDate = new AtomicLong(0L);
    private final AtomicLong responseDate = new AtomicLong(0L);
    private final AtomicLong requiredDate = new AtomicLong(0L);

    @CheckResult
    boolean setOutdated() {
        boolean launch = !isOutdated();
        requiredDate.set(SystemClock.elapsedRealtime());
        return launch;
    }

    boolean isOutdated() {
        return responseDate.get() < requiredDate.get();
    }

    void setLaunched() {
        launchDate.set(SystemClock.elapsedRealtime());
    }

    void setReplied() {
        responseDate.set(launchDate.get());
    }
}