/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */

package io.simgulary.cms.http;

import java.lang.reflect.Constructor;
import java.lang.reflect.Type;

import androidx.annotation.NonNull;
import retrofit2.Call;
import retrofit2.CallAdapter;

final class HttpResponseCallAdapter<T> implements CallAdapter<T, ApiRequest<T>> {
    private final Type responseType;
    private final Class<?> rawType;

    HttpResponseCallAdapter(Type responseType, Class<?> rawType) {
        this.responseType = responseType;
        this.rawType = rawType;
    }

    @Override
    public Type responseType() {
        return responseType;
    }

    @Override
    public ApiRequest<T> adapt(@NonNull Call<T> call) {
        try {
            //noinspection unchecked
            Class<? extends ApiRequest<T>> rawType = (Class<? extends ApiRequest<T>>) this.rawType;
            Constructor<?> constructor = rawType.getDeclaredConstructor(Call.class);
            //noinspection unchecked
            return (ApiRequest<T>) constructor.newInstance(call);
        }
        catch (Throwable e) {
            throw new RuntimeException("Unable to instantiate " + rawType, e);
        }
    }
}
