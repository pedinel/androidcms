/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 30/05/19 03:00 PM
 */

package io.simgulary.cms.http;

import java.util.List;

import io.simgulary.cms.utils.Identifiable;

public interface PaginatedResult<T extends Identifiable> {
    List<T> getCurrentItems();

    int getCurrentPage();

    int getTotalPages();

    int getTotalCount();

    int getPerPageCount();

    int getFromIndex();

    int getToIndex();

    default boolean isFirstPage() {
        return getCurrentPage() == 1;
    }

    default boolean isLastPage() {
        return getCurrentPage() == getTotalPages();
    }
}
