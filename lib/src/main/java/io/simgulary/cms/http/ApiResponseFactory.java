/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */

package io.simgulary.cms.http;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;

final class ApiResponseFactory extends CallAdapter.Factory {
    @Nullable
    @Override
    public CallAdapter<?, ?> get(@NonNull Type returnType, @NonNull Annotation[] annotations, @NonNull Retrofit retrofit) {
        Class<?> rawType = getRawType(returnType);

        if (ApiRequest.class.isAssignableFrom(rawType)) {
            Class<?> rootClass = rawType;

            while (!ApiRequest.class.equals(rootClass)) {
                returnType = rootClass.getGenericSuperclass();
                rootClass = getRawType(returnType);
            }

            if (returnType instanceof ParameterizedType) {
                Type dataType = getParameterUpperBound(0, ((ParameterizedType) returnType));
                return new HttpResponseCallAdapter<>(dataType, rawType);
            } else {
                return new HttpResponseCallAdapter<>(Object.class, rawType);
            }
        } else {
            return null;
        }
    }
}
