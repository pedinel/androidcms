package io.simgulary.cms.http;

import androidx.annotation.CheckResult;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import io.simgulary.cms.http.requests.Parameter;
import io.simgulary.cms.lifecycle.Observable;
import io.simgulary.cms.threads.Threads;

public final class RequestRefreshBuilder<T> {
    private final LifecycleOwner mOwner;
    private final ApiRequest<T> mRequest;
    private long mExpirationInMillis;
    private long mAutoRetryDelayInMillis;

    public RequestRefreshBuilder(LifecycleOwner owner, ApiRequest<T> request) {
        mOwner = owner;
        mRequest = request;
    }

    @CheckResult
    public RequestRefreshBuilder<T> expiration(long expirationInSeconds) {
        mExpirationInMillis = expirationInSeconds * 1000L;
        return this;
    }

    @CheckResult
    public RequestRefreshBuilder<T> autoRetryDelay(long autoRetryDelayInSeconds) {
        mAutoRetryDelayInMillis = autoRetryDelayInSeconds * 1000L;
        return this;
    }

    public ApiRequest<T> apply() {
        if (mExpirationInMillis > 0L) {
            makeExpirationInMillis(mOwner, mRequest, mExpirationInMillis);
        }

        if (mAutoRetryDelayInMillis > 0L) {
            makeAutoRetryDelay(mOwner, mRequest, mAutoRetryDelayInMillis);
        }

        return mRequest;
    }

    private static <T> void makeExpirationInMillis(LifecycleOwner owner, ApiRequest<T> request, long delayInMillis) {
        Observable<ApiResponse<T>> observable = request.getResponse();
        observable.observe(owner, new Observer<ApiResponse<?>>() {
            Threads.DelayedTask task;
            final Runnable action = () -> {
                if (observable.hasActiveObservers()) {
                    if (expired()) {
                        request.setOutdated();
                    } else {
                        post();
                    }
                }
            };
            @Override
            public void onChanged(ApiResponse<?> apiResponse) {
                if (!apiResponse.isSuccessful()) {
                    return;
                }

                post();
            }

            private void post() {
                if (task != null) {
                    task.cancel();
                }

                task = Threads.executeDelayed(delayInMillis - request.getMillisSinceLastUpdate(), action);
            }

            private boolean expired() {
                return request.getMillisSinceLastUpdate() >= delayInMillis;
            }
        });
    }

    private static <T> void makeAutoRetryDelay(LifecycleOwner owner, ApiRequest<T> request, long delayInMillis) {
        Observable<ApiResponse<T>> observable = request.getResponse();
        observable.observe(owner, new Observer<ApiResponse<?>>() {
            Threads.DelayedTask task;
            final Runnable action = () -> {
                if (observable.hasActiveObservers()) {
                    request.launchIfFailed();
                }
            };

            @Override
            public void onChanged(ApiResponse<?> apiResponse) {
                if (apiResponse.isSuccessful()) {
                    return;
                }

                post();
            }

            private void post() {
                if (task != null) {
                    task.cancel();
                }

                task = Threads.executeDelayed(delayInMillis, action);
            }
        });
    }
}
